<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\Utility\Text;
/**
 * News Entity.
 *
 * @property int $id
 * @property string $ar_title
 * @property string $ar_permalink
 * @property string $ar_description
 * @property string $ar_keywords
 * @property string $ar_meta_description
 * @property string $en_title
 * @property string $en_permalink
 * @property string $en_description
 * @property string $en_keywords
 * @property string $en_meta_description
 * @property string $tags
 * @property bool $active
 * @property int $category_id
 * @property \App\Model\Entity\Category $category
 * @property string $related_news
 * @property int $editor_id
 * @property bool $reviewed
 * @property int $reviewer_id
 * @property \App\Model\Entity\Reviewer $reviewer
 * @property int $publisher_id
 * @property bool $published
 * @property \Cake\I18n\Time $publish_date
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Image[] $images
 */
class News extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
    ];
    

}
