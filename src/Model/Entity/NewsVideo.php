<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NewsVideo Entity
 *
 * @property int $id
 * @property int $news_id
 * @property string $youtube_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \App\Model\Entity\News $news
 * @property \App\Model\Entity\Youtube $youtube
 */
class NewsVideo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
