<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PollQuestion Entity
 *
 * @property int $id
 * @property string $ar_title
 * @property string $en_title
 * @property int $type
 * @property int $poll_id
 * @property bool $required
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $updated
 *
 * @property \App\Model\Entity\Poll $poll
 * @property \App\Model\Entity\PollQuestionOption[] $poll_question_options
 */
class PollQuestion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true
    ];
}
