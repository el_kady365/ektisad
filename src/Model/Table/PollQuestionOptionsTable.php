<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PollQuestionOptions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PollQuestions
 *
 * @method \App\Model\Entity\PollQuestionOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\PollQuestionOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PollQuestionOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestionOption|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PollQuestionOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestionOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestionOption findOrCreate($search, callable $callback = null)
 */
class PollQuestionOptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('poll_question_options');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('PollQuestions', [
            'foreignKey' => 'poll_question_id',
            'joinType' => 'INNER'
        ]);
        
        $this->hasMany('PollAnswers', [
            'foreignKey' => 'poll_question_option_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('ar_title',__('Required'));

        $validator
            ->notEmpty('en_title',__('Required'));

        $validator
            ->integer('display_order')
            ->allowEmpty('display_order');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poll_question_id'], 'PollQuestions'));
        return $rules;
    }
}
