<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Images Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $News
 *
 * @method \App\Model\Entity\Image get($primaryKey, $options = [])
 * @method \App\Model\Entity\Image newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Image[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Image|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Image patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Image[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Image findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ImagesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->imageSettings = [
            'image' => [// The name of your upload field
                'folder' => 'img/uploads', // Customise the root upload folder here, or omit to use the default
                'required' => true,
                'width' => 0,
                'height' => 0,
                'versions' => [
                    'thumb1' => [
                        'width' => 75,
                        'height' => 55,
                        'crop' => 1
                    ]
                ],
            ]
        ];

        $this->addBehavior('Image', $this->imageSettings);

        $this->table('images');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('ar_title', __('Required'));

        $validator
                ->notEmpty('en_title', __('Required'));

        $validator
                ->allowEmpty('ar_description');

        $validator
                ->allowEmpty('en_description');

        $validator
                ->notEmpty('image', __('Required'), 'create');

        $validator
                ->allowEmpty('ar_tags');

        $validator
                ->allowEmpty('en_tags');

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        return $validator;
    }

}
