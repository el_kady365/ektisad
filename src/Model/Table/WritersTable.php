<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use ArrayObject;

/**
 * Writers Model
 *
 * @property \Cake\ORM\Association\HasMany $Articles
 *
 * @method \App\Model\Entity\Writer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Writer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Writer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Writer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Writer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Writer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Writer findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WritersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('writers');
        $this->displayField('id');
        $this->imageSettings = [
            'image' => [// The name of your upload field
                'folder' => 'img/uploads/writers', // Customise the root upload folder here, or omit to use the default
                'width' => 120,
                'height' => 120,
                'required' => true,
                'versions' => [
                    'thumb1' => [
                        'width' => 75,
                        'height' => 75,
                        'crop' => 1
                    ]]
            ]
        ];

        $this->addBehavior('Image', $this->imageSettings);

        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Articles', [
            'foreignKey' => 'writer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
//        $validator->provider('upload', \App\Model\Behavior\Validation\UploadValidation::class);
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('ar_title', __('Required'));

        $validator
                ->notEmpty('en_title', __('Required'));

        $validator
                ->allowEmpty('image');

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        return $validator;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if (!empty($entity->ar_about)) {
            $entity->ar_about = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->ar_about);
        }
        if (!empty($entity->en_about)) {
            $entity->en_about = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->en_about);
        }

        return true;
    }

}
