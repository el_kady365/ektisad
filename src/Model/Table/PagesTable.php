<?php

namespace App\Model\Table;

use App\Model\Entity\Page;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;
use Cake\Routing\Router;
use Cake\Datasource\EntityInterface;

/**
 * Pages Model
 *
 */
class PagesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public $folder;
    public $imageSettings;

    public function initialize(array $config) {
        parent::initialize($config);


        $this->imageSettings = [
            'image' => [// The name of your upload field
                'folder' => 'img/uploads', // Customise the root upload folder here, or omit to use the default
                'width' => 630,
                'height' => 0,
                'required' => true,
                'versions' => [],
            ]
        ];

        $this->addBehavior('Image', $this->imageSettings);
        $this->addBehavior('Tree');

        $this->belongsTo('Menus', ['forgienKey' => 'menu_id']);
        $this->table('pages');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create')
                ->allowEmpty('image')
                ->notEmpty('ar_title', __('Required', true))
                ->notEmpty('en_title', __('Required', true))
                ->notEmpty('ar_content', __('Required', true))
                ->notEmpty('en_content', __('Required', true))
                ->notEmpty('menu_id', __('Required', true));



        return $validator;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if (!empty($entity->ar_content)) {
            $entity->ar_content = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->ar_content);
        }
        if (!empty($entity->en_content)) {
            $entity->en_content = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->en_content);
        }

        return true;
    }

}
