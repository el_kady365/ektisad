<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PollAnswers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PollQuestions
 * @property \Cake\ORM\Association\BelongsTo $PollQuestionOptions
 *
 * @method \App\Model\Entity\PollAnswer get($primaryKey, $options = [])
 * @method \App\Model\Entity\PollAnswer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PollAnswer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PollAnswer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PollAnswer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PollAnswer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PollAnswer findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PollAnswersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('poll_answers');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PollQuestions', [
            'foreignKey' => 'poll_question_id'
        ]);
        $this->belongsTo('PollQuestionOptions', [
            'foreignKey' => 'poll_question_option_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poll_question_id'], 'PollQuestions'));
        $rules->add($rules->existsIn(['poll_question_option_id'], 'PollQuestionOptions'));

        return $rules;
    }
}
