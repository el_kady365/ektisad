<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Utility\Text;
use Cake\Datasource\EntityInterface;

/**
 * Articles Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Writers
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Editors
 * @property \Cake\ORM\Association\BelongsTo $Reviewers
 * @property \Cake\ORM\Association\BelongsTo $Publishers
 *
 * @method \App\Model\Entity\Article get($primaryKey, $options = [])
 * @method \App\Model\Entity\Article newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Article[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Article|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Article patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Article[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Article findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArticlesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('articles');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Writers', [
            'foreignKey' => 'writer_id'
        ]);
        $this->belongsTo('Editors', [
            'className' => 'Users',
            'foreignKey' => 'editor_id'
        ]);
        $this->belongsTo('Reviewers', [
            'className' => 'Users',
            'foreignKey' => 'reviewer_id'
        ]);
        $this->belongsTo('Publishers', [
            'className' => 'Users',
            'foreignKey' => 'publisher_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');


        $validator
                ->allowEmpty('ar_permalink');




        $validator
                ->allowEmpty('ar_meta_description');


        $validator
                ->allowEmpty('en_permalink');



        $validator
                ->allowEmpty('en_meta_description');

        $validator
                ->allowEmpty('ar_tags');

        $validator
                ->allowEmpty('en_tags');

        $validator
                ->notEmpty('writer_id', __('Required'));

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        $validator
                ->boolean('reviewed')
                ->allowEmpty('reviewed');

        $validator
                ->boolean('published')
                ->allowEmpty('published');

        $validator
                ->dateTime('publish_date')
                ->allowEmpty('publish_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['writer_id'], 'Writers'));
        $rules->add($rules->existsIn(['editor_id'], 'Editors'));
        $rules->add($rules->existsIn(['reviewer_id'], 'Reviewers'));
        $rules->add($rules->existsIn(['publisher_id'], 'Publishers'));
        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if (!empty($entity->ar_title)) {
            $entity->ar_permalink = Text::slug($entity->ar_title, ['transliteratorId' => false]);
        }
        if (!empty($entity->en_title)) {
            $entity->en_permalink = Text::slug($entity->en_title, ['transliteratorId' => false]);
        }

        if (!empty($entity->ar_description)) {
            $entity->ar_description = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->ar_description);
        }
        if (!empty($entity->en_description)) {
            $entity->en_description = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->en_description);
        }

        return true;
    }

}
