<?php

namespace App\Model\Table;

use App\Model\Entity\News;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\Utility\Text;
use Cake\Datasource\EntityInterface;

/**
 * News Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Categories
 * @property \Cake\ORM\Association\BelongsTo $Editors
 * @property \Cake\ORM\Association\BelongsTo $Reviewers
 * @property \Cake\ORM\Association\BelongsTo $Publishers
 * @property \Cake\ORM\Association\HasMany $NewsImages
 */
class NewsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);
        $this->imageSettings = [
            'ad_image' => [// The name of your upload field
                'folder' => 'img/uploads', // Customise the root upload folder here, or omit to use the default
                'width' => 670,
                'height' => 80,
                'required' => true,
                'versions' => [],
            ]
        ];

        $this->addBehavior('Image', $this->imageSettings);
        $this->table('news');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);
        $this->belongsTo('Editors', [
            'className' => 'Users',
            'foreignKey' => 'editor_id'
        ]);
        $this->belongsTo('Writers', [
            'className' => 'Writers',
            'foreignKey' => 'writer_id'
        ]);
        $this->belongsTo('Reviewers', [
            'className' => 'Users',
            'foreignKey' => 'reviewer_id'
        ]);
        $this->belongsTo('Publishers', [
            'className' => 'Users',
            'foreignKey' => 'publisher_id'
        ]);
        $this->hasMany('NewsImages', [
            'foreignKey' => 'news_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('NewsNotes', [
            'foreignKey' => 'news_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('NewsVideos', [
            'foreignKey' => 'news_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->belongsTo('Groups', [
            'className' => 'Groups',
            'foreignKey' => 'assigned_to'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('en_meta_description');

        $validator
                ->notEmpty('category_id', __('Required', true));

        $validator
                ->allowEmpty('en_description');
        $validator
                ->allowEmpty('en_description');

        $validator
                ->allowEmpty('en_meta_description');

        $validator
                ->allowEmpty('en_tags');

        $validator
                ->allowEmpty('en_tags');

        $validator
                ->boolean('active')
                ->allowEmpty('active');
        $validator
                ->allowEmpty('ad_image');


        $validator
                ->boolean('reviewed')
                ->allowEmpty('reviewed');

        $validator
                ->boolean('published')
                ->allowEmpty('published');

        $validator
                ->dateTime('publish_date')
                ->allowEmpty('publish_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['editor_id'], 'Editors'));
        $rules->add($rules->existsIn(['reviewer_id'], 'Reviewers'));
        $rules->add($rules->existsIn(['publisher_id'], 'Publishers'));
        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {

        $field = 'image';

        if (!empty($data['id'])) {
            $all_images = $this->NewsImages->find('list', ['conditions' => ['NewsImages.news_id' => $data['id']]]);
            $all_videos = $this->NewsVideos->find('list', ['conditions' => ['NewsVideos.news_id' => $data['id']]]);
            $all_notes = $this->NewsNotes->find('list', ['conditions' => ['NewsNotes.news_id' => $data['id']]]);
            if (!empty($data['news_images'])) {
                $news_images_ids = [];

                foreach ($all_images as $id => $fimage) {
                    $news_images_ids[] = $id;
                }

                $delete_news_array = [];
                $posted_ids = [];
                foreach ($data['news_images'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    } else {
                        if (isset($item[$field]['name']) && empty($item[$field]['name']) && $item[$field]['error'] === UPLOAD_ERR_NO_FILE) {
                            unset($data['news_images'][$i]);
                        }
                    }
                }

                $delete_news_array = array_diff($news_images_ids, $posted_ids);

                if ($delete_news_array) {
                    foreach ($delete_news_array as $delete_id) {
                        $im = $this->NewsImages->get($delete_id);
                        $this->NewsImages->delete($im);
                    }
                }
            } else {
                foreach ($all_images as $delete_id) {
                    $im = $this->NewsImages->get($delete_id);
                    $this->NewsImages->delete($im);
                }
            }

            if (!empty($data['news_videos'])) {
                $news_videos_ids = [];

                foreach ($all_videos as $id => $fimage) {
                    $news_videos_ids[] = $id;
                }

                $delete_videos_array = [];
                $posted_ids = [];
                foreach ($data['news_videos'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }

                $delete_videos_array = array_diff($news_videos_ids, $posted_ids);
                if (!empty($delete_videos_array))
                    $this->NewsVideos->deleteAll(['id IN ' => $delete_videos_array]);
            } else {
                $this->NewsVideos->deleteAll(['news_id' => $data['id']]);
            }

            if (!empty($data['news_notes'])) {
                $news_notes_ids = [];

                foreach ($all_notes as $id => $fimage) {
                    $news_notes_ids[] = $id;
                }

                $delete_notes_array = [];
                $posted_ids = [];
                foreach ($data['news_notes'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }

                $delete_notes_array = array_diff($news_notes_ids, $posted_ids);
                if (!empty($delete_notes_array))
                    $this->NewsNotes->deleteAll(['id IN ' => $delete_notes_array]);
            } else {
                $this->NewsNotes->deleteAll(['news_id' => $data['id']]);
            }
        } else {
            foreach ($data['news_images'] as $key => $image) {
                if (isset($image[$field]['name']) && empty($image[$field]['name']) && $image[$field]['error'] === UPLOAD_ERR_NO_FILE) {
                    unset($data['news_images'][$key]);
                }
            }
        }
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options) {
        if (!empty($entity->en_title)) {
            $entity->en_permalink = Text::slug($entity->en_title, ['transliteratorId' => false]);
        }
        if (!empty($entity->en_title)) {
            $entity->en_permalink = Text::slug($entity->en_title, ['transliteratorId' => false]);
        }

        /* /*        if (!empty($entity->ar_description)) {
          //            $entity->ar_description = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->ar_description);
          //        }
          //        if (!empty($entity->en_description)) {
          //            $entity->en_description = preg_replace_callback('#(<img\s(?>(?!src=)[^>])*?src=")data:image/(gif|png|jpeg);base64,([\w=+/]++)("[^>]*>)#', "data_to_img", $entity->en_description);
          //        }
         * 
         */

        return true;
    }

    public function getFacebookCommentsCount($id) {
        $process = curl_init('https://graph.facebook.com/v2.4/?fields=share&id=' . \Cake\Routing\Router::url(['action' => 'view', $id], true));
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        $return = json_decode(curl_exec($process));
        curl_close($process);
        if (isset($return->share)) {
            return $return->share->comment_count;
        }
        return 0;
    }

    function get_tags_conditions($lang, $tags) {
        $tags = explode(',', $tags);
        $conditions = [];
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $conditions[] = "CONCAT(',', {$lang}_tags, ',') LIKE '%,$tag,%'";
            }
        }
        return $conditions;
    }

}
