<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * PollQuestions Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Polls
 * @property \Cake\ORM\Association\HasMany $PollQuestionOptions
 *
 * @method \App\Model\Entity\PollQuestion get($primaryKey, $options = [])
 * @method \App\Model\Entity\PollQuestion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PollQuestion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PollQuestion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PollQuestion findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PollQuestionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public $types = [];

    public function initialize(array $config) {
        parent::initialize($config);
        $this->types = [
            1 => __('TextBox'),
            2 => __('Radio Single Choice'),
            3 => __('SelectBox Single Choice'),
            4 => __('Multi choice'),
            5 => __('Essay text'),
            6 => __('Label'),
        ];

        $this->table('poll_questions');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('PollQuestionOptions', [
            'foreignKey' => 'poll_question_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('ar_title', __('Required'));

        $validator
                ->notEmpty('en_title', __('Required'));

        $validator
                ->integer('type')
                ->allowEmpty('type');

        $validator
                ->boolean('required')
                ->allowEmpty('required');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
//      $rules->add($rules->existsIn(['poll_id'], 'Polls'));
        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {


        if (!empty($data['id'])) {
            $all_options = $this->PollQuestionOptions->find('list', ['conditions' => ['PollQuestionOptions.poll_question_id' => $data['id']]]);

            if (!empty($data['poll_question_options'])) {
                $poll_question_options_ids = [];

                foreach ($all_options as $id => $fimage) {
                    $poll_question_options_ids[] = $id;
                }

                $delete_options_array = [];
                $posted_ids = [];
                foreach ($data['poll_question_options'] as $i => $item) {
                    if (!empty($item['id'])) {
                        $posted_ids[] = $item['id'];
                    }
                }

                $delete_options_array = array_diff($poll_question_options_ids, $posted_ids);
                if (!empty($delete_videos_array))
                    $this->PollQuestionOptions->deleteAll(['id IN ' => $delete_options_array]);
            } else {
                $this->PollQuestionOptions->deleteAll(['poll_question_id' => $data['id']]);
            }
        }
    }

}
