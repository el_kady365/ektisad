<?php

namespace App\Model\Table;

use App\Model\Entity\Category;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;
use Cake\Utility\Text;

/**
 * Categories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentCategories
 * @property \Cake\ORM\Association\HasMany $ChildCategories
 * @property \Cake\ORM\Association\HasMany $News
 */
class CategoriesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('categories');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);

//        $this->hasMany('ChildCategories', [
//            'className' => 'Categories',
//            'foreignKey' => 'parent_id'
//        ]);
        $this->hasMany('News', [
            'foreignKey' => 'category_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');
        $validator
                ->integer('lft')
                ->allowEmpty('lft');

        $validator
                ->integer('rght')
                ->allowEmpty('rght');

        $validator
                ->integer('display_order')
                ->allowEmpty('display_order');

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        $validator->notEmpty('ar_title', __('Required', true))
                ->notEmpty('en_title', __('Required', true));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCategories'));
        return $rules;
    }

    public function beforeSave(Event $event, Entity $entity, ArrayObject $options) {
        if (!empty($entity->ar_title)) {
            $entity->ar_permalink = Text::slug($entity->ar_title, ['transliteratorId' => false]);
        }
        if (!empty($entity->en_title)) {
            $entity->en_permalink = Text::slug($entity->en_title, ['transliteratorId' => false]);
        }
        return true;
    }

    public function GetCategoriesList() {
        $all_categories = $this->find('threaded', ['conditions' => [ 'Categories.active' => 1]])->toArray();

        $categories = array();
        if (!empty($all_categories)) {
            foreach ($all_categories as $category) {
                if (empty($category->children)) {
                    $categories[$category->id] = $category->ar_title;
                } else {
                    $children = [];
                    foreach ($category->children as $child) {

                        if ($child->children) {
                            $childchilds = [];
                            foreach ($child->children as $childchild) {
                                $childchilds[$child->id] = $childchild->ar_title;
                            }
                            $children[$child->ar_title] = $childchilds;
                        } else {
                            $children[$child->id] = $child->ar_title;
                        }
                    }
                    $categories[$category->ar_title] = $children;
                }
            }
        }
//        debug($categories);
        return $categories;
    }

}
