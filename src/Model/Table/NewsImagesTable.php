<?php

namespace App\Model\Table;

use App\Model\Entity\NewsImage;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\Entity;
use ArrayObject;
use Cake\Routing\Router;

/**
 * NewsImages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $News
 */
class NewsImagesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('news_images');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->imageSettings = [
            'image' => [// The name of your upload field
                'folder' => 'img/uploads', // Customise the root upload folder here, or omit to use the default
                'required' => true,
                'width' => 0,
                'height' => 0,
                'versions' => [],
            ]
        ];

        $this->addBehavior('Image', $this->imageSettings);
        $this->belongsTo('News', [
            'foreignKey' => 'news_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('image', __('Required'), 'create');

        $validator
                ->notEmpty('video', __('Required'));

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['news_id'], 'News'));
        return $rules;
    }

}
