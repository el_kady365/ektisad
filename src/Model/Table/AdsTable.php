<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;

/**
 * Ads Model
 *
 * @method \App\Model\Entity\Ad get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ad newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Ad[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ad|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ad patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ad[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ad findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public $image_settings = [
        'header' => [
            'image' => [
                'folder' => 'img/uploads/ads/',
                'width' => 650,
                'height' => 80,
                'crop' => 1,
                'required' => true,
                'versions' => [],
            ]],
        'banner_left' => ['image' => [
                'folder' => 'img/uploads/ads/',
                'width' => 80,
                'height' => 0,
                'crop' => 1,
                'required' => true,
                'versions' => [],
            ]],
        'banner_right' => ['image' => [
                'folder' => 'img/uploads/ads/',
                'width' => 80,
                'height' => 0,
                'crop' => 1,
                'required' => true,
                'versions' => [],
            ]],
        'banner_bottom' => ['image' => [
                'folder' => 'img/uploads/ads/',
                'width' => 730,
                'height' => 90,
                'crop' => 1,
                'required' => true,
                'versions' => [],
            ]],
        'side_bar' => ['image' => [
                'folder' => 'img/uploads/ads/',
                'width' => 300,
                'height' => 250,
                'crop' => 1,
                'required' => true,
                'versions' => [],
            ]]
    ];
    public $locations = [];

    public function initialize(array $config) {
        parent::initialize($config);
        $this->locations = ['header' => __('Header'), 'banner_left' => __('Banner left'), 'banner_right' => __('Banner right'), 'banner_bottom' => __('Banner Bottom'), 'side_bar' => __('Side Bar')];
        $this->addBehavior('Image', [
            'image' => [// The name of your upload field
                'folder' => 'img/uploads/ads', // Customise the root upload folder here, or omit to use the default
                'width' => 460,
                'crop' => false,
                'required' => false,
                'versions' => [],
            ]
        ]);
        $this->table('ads');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->notEmpty('title', __('Required'));

        $validator
                ->notEmpty('url', __('Required'));
        
        $validator
                ->notEmpty('category_id', __('Required'));

        $validator->add('url', 'url', [
            'message' => __('Please enter valid url'),
            'rule' => ['url', true]
        ]);

        $validator
                ->notEmpty('location', __('Required'));

        $validator
                ->notEmpty('image', __('Required'), 'create');

        $validator
                ->integer('display_order')
                ->allowEmpty('display_order');

        $validator
                ->boolean('active')
                ->allowEmpty('active');

        return $validator;
    }

}
