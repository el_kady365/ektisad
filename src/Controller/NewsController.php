<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 */
class NewsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index($category_id = false) {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        if ($this->RequestHandler->isRss()) {
            $news = $this->News->find('all', [
                'contain' => ['Categories', 'NewsImages', 'Writers'],
                'conditions' => [
                    'News.published' => 1,
                    'News.publish_date <= ' => $today,
                ],
                'order' => ['News.publish_date desc'],
                'limit' => 30
            ]);
            $this->set('channel', [
                'title' => __("RSS news feed"),
                'link' => Router::url('/', true),
                'description' => __("RSS news feed."),
                'language' => 'en-us'
            ]);
            $this->set(compact('news'));
        } else {
            if (!$category_id) {
                throw new \Cake\Network\Exception\NotFoundException();
            }
            $this->paginate = [
                'contain' => ['Categories', 'NewsImages', 'Writers'],
                'conditions' => [
                    'News.category_id' => $category_id,
                    'News.published' => 1,
                    'News.publish_date <= ' => $today,
                ],
                'order' => ['News.publish_date desc'],
                'limit' => 10
            ];
            $featured_news = TableRegistry::get('News')->find('all', [
                'conditions' =>
                ['News.published' => 1,
                    'News.featured' => 1,
                    'News.category_id' => $category_id,
                    'News.publish_date <= ' => $today,
                ],
                'order' => ['News.publish_date desc'],
                'contain' => ['NewsImages'],
                'limit' => 6]
            );

            $category = TableRegistry::get('Categories')->get($category_id);
            $news = $this->paginate($this->News);
            $this->set(compact('news', 'category', 'featured_news'));
            $this->set('selectedCategory', $category_id);
            $this->set('_serialize', ['news']);
            $this->set('title_for_layout', $category->{$this->lang . '_title'});
            $adsObject = TableRegistry::get('Ads')->find();
            $ads = $adsObject->where(['Ads.active' => 1, 'Ads.category_id' => $category_id])
                            ->order(['Ads.display_order asc'])->all();
            if ($ads->count()) {
                $this->set('ads', $ads);
            }
        }
    }

    /**
     * View method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $news = $this->News->find('all', [
                    'contain' => ['Categories', 'NewsImages', 'NewsVideos', 'Editors', 'Writers'],
                    'conditions' => [
                        'News.id' => $id,
                        'News.published' => 1,
                        'News.publish_date <= ' => $today,
                    ]
                ])->firstOrFail();
        $tags = $news->{$this->lang . '_tags'};

        $tags_conditions = $this->News->get_tags_conditions($this->lang, $tags);
//        debug($tags_conditions);
        $related_news = $this->News->find('all', [
            'contain' => ['NewsImages'],
            'conditions' => [
                'OR' => $tags_conditions,
                'News.id !=' => $id,
                'News.published' => 1,
                'News.publish_date <= ' => $today,
            ],
            'order' => ['Rand()'],
            'limit' => 4,
        ]);
        $related_images = TableRegistry::get('Images')->find('all', [
            'conditions' => [
                'OR' => $tags_conditions,
                'Images.active' => 1
            ], 'order' =>
            'Rand()',
            'limit' => 4
        ]);
        $comments_count = $this->News->getFacebookCommentsCount($id);
        $views = $news->views;

        $session = $this->request->session();
        $news_ids = [];
        if (!$session->check('news_id') || !in_array($id, $session->read('news_id'))) {
            ++$views;
            $news_ids[] = $id;
            $session->write('news_id', $news_ids);
        }

        $newsObj = $this->News->get($id);
        $newsObj->views = $views;
        $newsObj->comments_count = $comments_count;
        $this->News->save($newsObj);
        $this->set('selectedCategory', $news->category->id);
        $this->set('og_description', $news->{$this->lang . '_description'});
        $this->set('meta_description', $news->{$this->lang . '_meta_description'});
        $this->set('og_title', $news->{$this->lang . '_title'});
        if (isset($news->news_images[0]['image'])) {
            $this->set('og_image', Router::url($news->news_images[0]['image_info']['path'], true));
        }
        $this->set('news', $news);
        $adsObject = TableRegistry::get('Ads')->find();
        $ads = $adsObject->where(['Ads.active' => 1, 'Ads.category_id' => $news->category->id])
                        ->order(['Ads.display_order asc'])->all();
        if ($ads->count()) {
            $this->set('ads', $ads);
        }
        $this->set('related_news', $related_news);
        $this->set('related_images', $related_images);
        $this->set('_serialize', ['news']);
        $this->set('title_for_layout', $news->category->{$this->lang . '_title'} . ' - ' . $news->{$this->lang . '_title'});
    }

    public
            function search() {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $conditions = [
            'News.published' => 1,
            'News.publish_date <= ' => $today,
        ];
        if ($this->request->query('s')) {
            $s = $this->request->query('s');
            $conditions[] = "match(`News`.`ar_title`,`News`.`ar_description`,`News`.`en_title`,`News`.`en_description`) AGAINST('$s' in boolean mode)";
        }
        $this->paginate = [
            'contain' => ['NewsImages', 'Writers'],
            'conditions' => $conditions,
            'order' => ['News.publish_date desc'],
            'limit' => 10
        ];
        $news = $this->paginate($this->News);
        $this->set(compact('news'));
        $this->set('_serialize', ['news']);
        $this->set('title_for_layout', __('Search results'));
    }

}
