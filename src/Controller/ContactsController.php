<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 */
class ContactsController extends AppController {

    public function contactUs($type) {
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->data);
            $contact->type = $type;
            if ($type == 1) {
                $subject = __('New Contact us message');
            } else {
                $subject = __('New Suggestions message');
            }
            if ($this->Contacts->save($contact)) {

                $email = new Email('default');
                $email->template('contact')
                        ->emailFormat('html')
                        ->to($this->config['email'])
                        ->from($contact->email)
                        ->subject($subject)
                        ->viewVars(['data' => $contact, 'lang' => $this->lang, 'config' => $this->config]);

                if ($email->send()) {
                    $this->Flash->success(__('Your message has been sent'));
                    return $this->redirect(['action' => 'contact-us',$type]);
                } else {
                    $this->Flash->error(__("can't send email", true), 'fail');
                }
            } else {
                $this->Flash->error(__("Your message could not be sent, please check for input errors and try again"));
            }
        }
        $this->set('type', $type);
        $this->set(compact('contact'));
        $this->set('_serialize', ['contact']);
        $this->set('title_for_layout', __('Contact us'));
    }

    public function advertiseWithUs() {
        $this->set('title_for_layout', __('Advertise with us'));
    }

}
