<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function home() {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');

        $featured_news = TableRegistry::get('News')->find('all', [
            'conditions' =>
            ['News.published' => 1,
                'News.featured' => 1,
                'News.publish_date <= ' => $today,
            ],
            'order' => 'News.publish_date desc',
            'contain' => ['NewsImages'],
            'limit' => 6]
        );


        $categoris_list = TableRegistry::get('Categories')->find('threaded')->where(["Categories.active" => true])->all()->toArray();

        foreach ($categoris_list as &$category) {
            $conditions = ['News.published' => 1,
                'News.show_on_home <= ' => 1,
                'News.publish_date <= ' => $today,
            ];
            if (!empty($category['children'])) {
                $cat_ids = \Cake\Utility\Hash::extract($categoris_list, '{n}.children.{n}.id');
                $conditions['News.category_id IN'] = $cat_ids;
            } else {
                $conditions['News.category_id IN'] = $category['id'];
            }
            $category['News'] = TableRegistry::get('News')->find('all', [
                'conditions' => $conditions,
                'order' => 'News.publish_date desc',
                'contain' => ['NewsImages'],
                'limit' => 6]
            );
        }

        $home_videos = TableRegistry::get('Videos')->find()->where(['Videos.active' => 1])->order(['Videos.display_order' => 'asc'])->limit(8);
//                debug($home_videos);
        $this->set('home_videos', $home_videos);
        $this->set('featured_news', $featured_news);
        $this->set('home_categories', $categoris_list);
//        debug($featured_news);exit;
        $this->set('title_for_layout', __('Home'));
    }

    function view($id) {
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);

        $this->set('page', $page);
        $this->set('_serialize', ['page']);
        $this->set('title_for_layout', $page->{$this->lang . '_title'});
    }

    public function answer($question_id) {
        $object = TableRegistry::get('PollAnswers');
        $pollAnswer = $object->newEntity();
        if ($this->request->is('post')) {
            $pollAnswer = $object->patchEntity($pollAnswer, $this->request->data);
            $pollAnswer->poll_question_id = $question_id;

//                    debug($pollAnswer);
//                    exit;
//                     $this->Cookie->configKey('poll_question_' . $question_id, [
//                            'expires' => '+7 days',
//                            'httpOnly' => true,
//                            'path' => '/'
//                        ]);
//                        $this->Cookie->write('poll_question_' . $question_id, 1);
            if ($object->save($pollAnswer)) {
                setcookie('poll_question', $question_id, time() + (60 * 60 * 24 * 7), '/');
                $this->Flash->success(__('The poll Answer has been saved.'), ['key' => 'poll']);
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('The poll question could not be saved. Please, try again.'), ['key' => 'poll']);
            }
        }
    }

}
