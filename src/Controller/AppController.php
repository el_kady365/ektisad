<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $config = [];
    public $lang = '';

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->load_config();
        $this->_setLanguage();
        $prefix = isset($this->request->params['prefix']) ? $this->request->params['prefix'] : '';
//        $session = $this->request->session();
//         $session->write('test', 1);
//        if (isset($_GET['test'])) {
//            $session->write('test', $_GET['test']);
//        }
//        if (!$session->check('test')) {
//            throw new \Cake\Network\Exception\NotFoundException();
//        }
        if (!$prefix) {
            $ads = TableRegistry::get('Ads')->find()->where(['Ads.active' => 1, "(Ads.category_id is Null OR Ads.category_id='')"])
                    ->all();
            $this->set(compact('ads'));
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        $prefix = isset($this->request->params['prefix']) ? $this->request->params['prefix'] : '';
        if (!$prefix) {
            $today = \Cake\I18n\Time::now()->format('Y-m-d H:i:s');

            $this->loadModel('Categories');
            $nav_categories = $this->Categories->find('threaded', ['conditions' => ['Categories.active' => 1], 'order' => ['Categories.display_order asc']]);
            $side_images = TableRegistry::get('Images')->find('all')->where(['Images.active' => 1])
                            ->order(['Images.created desc'])->limit(12);

            $side_articles = TableRegistry::get('Articles')->find()->where(['Articles.published' => 1, 'Articles.publish_date <=' => $today])
                            ->order(['Articles.publish_date desc'])->contain(['Writers'])->limit(4)->all();

            $most_commented = TableRegistry::get('News')->find('all')->where(['News.published' => 1, 'News.publish_date <=' => $today])
                            ->order(['News.comments_count desc', 'News.publish_date desc'])->contain(['NewsImages'])->limit(4);

            $most_viewed = TableRegistry::get('News')->find('all')->where(['News.published' => 1, 'News.publish_date <=' => $today])
                            ->order(['News.views desc', 'News.publish_date desc'])->contain(['NewsImages'])->limit(4);

            $latest_news = TableRegistry::get('News')->find()->where(['News.published' => 1, 'News.publish_date <=' => $today])
                            ->order(['News.publish_date desc'])->limit(10)->all();


            $polls = TableRegistry::get('PollQuestions')->find()->where(['PollQuestions.active' => 1])->contain(['PollQuestionOptions'])->first();
            $poll_id = $polls->id;
//            debug($poll_id);
            $poll_object = TableRegistry::get('PollQuestionOptions')->find();
            $poll_options = $poll_object->select(['PollQuestionOptions.ar_title', 'PollQuestionOptions.en_title', 'count' => $poll_object->func()->count('*')])
                    ->innerJoinWith('PollAnswers')->innerJoinWith('PollQuestions')
                    ->where(['PollQuestions.id' => $poll_id])
                    ->group(['PollQuestionOptions.ar_title', 'PollQuestionOptions.en_title'])
                    ->order(['PollQuestionOptions.display_order asc'])
                    ->toArray();
            $poll_answer = TableRegistry::get('PollQuestionOptions')->find();
            $total_results = $poll_answer->select(['count' => $poll_answer->func()->count('*')])
                            ->innerJoinWith('PollAnswers')->innerJoinWith('PollQuestions')
                            ->where(['PollQuestions.id' => $poll_id])->toArray();
            $this->set(compact('nav_categories', 'side_images', 'most_commented', 'most_viewed', 'ads', 'side_articles', 'latest_news', 'polls', 'poll_options', 'total_results'));
        }
    }

    public function load_config() {
        $this->loadModel('Settings');
        $configs = $this->Settings->find('all');
        $this->config = [];

        foreach ($configs as $config) {
            $vars = \Cake\Utility\Hash::extract($this->Settings->vars, '{s}.fields.' . $config->setting_key);
            if (isset($vars[0]['behavior']) && !empty($config->setting_value)) {
                $this->config[$config->setting_key] = Router::url('/' . $vars[0]['behavior']['settings']['folder'] . '/' . $config->setting_value);
            } else {
                $this->config[$config->setting_key] = $config->setting_value;
            }
        }
//        exit;
        $this->set('config', $this->config);
    }

    function _setLanguage($lang = false) {
        $session = $this->request->session();
        $params = $this->request->params;

        $locale_languages = ['en' => 'en_US', 'ar' => 'ar_AR'];

        if (!$lang) {
            if (isset($params['language'])) {
                $lang = $locale_languages[$params['language']];
            } elseif ($session->check('Config.language')) {
                $lang = $locale_languages[$session->read('Config.language')];
            } else {
                $lang = 'ar_AR';
            }
        }


        I18n::locale($lang);
        $this->set('locale', $lang);

        if ($lang != 'ar_AR') {
            $lang = 'en';
        } else {
            $lang = 'ar';
        }

        $session->write('Config.language', $lang);
        Configure::write('language', $lang);

        $this->lang = $lang;
        $this->set('lang', $lang);

        return $lang;
    }

    public function changeLang($lang) {
        $previous_url = $this->referer('/', true);
        if (preg_match('/\/(en|ar)\/?/', $previous_url)) {
            $langUrl = preg_replace('/\/(en|ar)\/?/', '/' . $lang . '/', $previous_url);
        } else {
            $langUrl = '/' . $lang . $previous_url;
        }
        $this->redirect($langUrl);
    }

}
