<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $conditions = [];
//        debug($this->loggedUser);
        $order = ['Articles.created desc'];
        if ($this->loggedUser['group_id'] != 1) {
            if ($this->loggedUser['group_id'] == 2) {
                $conditions['reviewed'] = true;
                $order[] = 'Articles.reviewed asc';
            } elseif ($this->loggedUser['group_id'] == 4) {
                $conditions['editor_id'] = $this->loggedUser['id'];
            } elseif ($this->loggedUser['group_id'] == 5) {
                $conditions['Articles.writer_id'] = $this->loggedUser['writer_id'];
            }
        }
        $this->paginate = [
            'contain' => ['Writers', 'Editors', 'Reviewers', 'Publishers'],
            'conditions' => $conditions,
            'order' => $order
        ];
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $article = $this->Articles->get($id, [
            'contain' => ['Writers', 'Editors', 'Reviewers', 'Publishers']
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            if (hasCapablity('articles', 'add', $this->loggedUser) && !$article->writer_id && $this->loggedUser['group_id'] == 5) {
                $article->writer_id = $this->loggedUser['writer_id'];
            }
            $article->editor_id = $this->loggedUser['id'];
            if ($article->reviewed) {
                $article->reviewer_id = $this->loggedUser['id'];
            }
            if ($article->published) {
                $article->reviewed = true;
                $article->publisher_id = $this->loggedUser['id'];
                if (empty($article->publish_date)) {
                    $article->publish_date = date('Y-m-d H:i:s');
                }
            }
            $article = $this->Articles->patchEntity($article, $this->request->data);

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $writers = $this->Articles->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id', 'limit' => 200, 'conditions' => ['Writers.active' => 1]]);

        $editors = $this->Articles->Editors->find('list', ['limit' => 200]);
        $reviewers = $this->Articles->Reviewers->find('list', ['limit' => 200]);
        $publishers = $this->Articles->Publishers->find('list', ['limit' => 200]);
        $this->set(compact('article', 'writers', 'editors', 'reviewers', 'publishers'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($article->getOriginal('reviewed') == false && $article->reviewed) {
                $article->reviewer_id = $this->loggedUser['id'];
            }
            if ($article->getOriginal('published') == false && $article->published) {
                $article->reviewed = true;
                $article->publisher_id = $this->loggedUser['id'];
                $article->publish_date = date('Y-m-d H:i:s');
            }
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $writers = $this->Articles->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id', 'limit' => 200, 'conditions' => ['Writers.active' => 1]]);

        $editors = $this->Articles->Editors->find('list', ['limit' => 200]);
        $reviewers = $this->Articles->Reviewers->find('list', ['limit' => 200]);
        $publishers = $this->Articles->Publishers->find('list', ['limit' => 200]);
        $this->set(compact('article', 'writers', 'editors', 'reviewers', 'publishers'));
        $this->set('_serialize', ['article']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    function doOperation() {
//        debug($this->request->data);
//        exit;
        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->Articles->deleteAll(['Articles.id IN' => $ids])) {
                $this->Flash->success(__('Articles deleted successfully'));
            } else {
                $this->Flash->error(__('Articles can not be deleted'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    public function review($id) {
        $article = $this->Articles->get($id);
        $article->reviewed = true;
        $article->reviewer_id = $this->loggedUser['id'];
        if ($this->Articles->save($article)) {
            $this->Flash->success(__('The article has been reviewed successfully.'));
        } else {
            $this->Flash->error(__('The article could not be reviewed. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function publish($id) {
        $article = $this->Articles->get($id);
        $article->published = true;
        $article->reviewed = true;
        $article->publisher_id = $this->loggedUser['id'];
        $article->publish_date = date('Y-m-d H:i:s');
        if ($this->Articles->save($article)) {
            $this->Flash->success(__('The article has been reviewed successfully.'));
        } else {
            $this->Flash->error(__('The article could not be reviewed. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
