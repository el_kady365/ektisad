<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\GroupsTable $Groups
 */
class UsersController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize() {
        parent::initialize();
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);

//        $this->Auth->allow('add');
    }

    public function login() {
        $this->viewBuilder()->layout('');

        if ($this->Auth->user()) {

            $this->redirect(array('controller' => 'pages', 'action' => 'dashboard'));
        }
        if ($this->request->is('post')) {

            $uuser = $this->Auth->identify();
//            debug($uuser);exit;
            if ($uuser) {

                $this->Auth->setUser($uuser);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Your username or password was incorrect.'));
        }

        $this->set('title_for_layout', __('login'));
    }

    public function logout() {
        $this->Flash->success(__('Good Bye'));
        $this->redirect($this->Auth->logout());
    }

    public function index() {
        $this->paginate = [
            'contain' => ['Groups']
        ];
        $uusers = $this->paginate($this->Users);
        $this->set(compact('uusers'));
        $this->set('_serialize', ['uusers']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $uuser = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $uuser);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $uuser = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $uuser = $this->Users->patchEntity($uuser, $this->request->data);
            if ($this->Users->save($uuser)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list');
        $writers = $this->Users->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id']);
        $this->set(compact('uuser', 'groups', 'writers'));
        $this->set('_serialize', ['uuser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $uuser = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $uuser = $this->Users->patchEntity($uuser, $this->request->data);
            if ($this->Users->save($uuser)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list');
        $writers = $this->Users->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id']);
        $uuser->unsetProperty('password');
        $this->set(compact('uuser', 'groups', 'writers'));
        $this->set('_serialize', ['uuser']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $uuser = $this->Users->get($id);
        if ($this->Users->delete($uuser)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    function doOperation() {
//        debug($this->request->data);
//        exit;
        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->Users->deleteAll(['Users.id IN' => $ids])) {
                $this->Flash->success(__('Users deleted successfully'));
            } else {
                $this->Flash->error(__('Users can not be deleted'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
