<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * Ads Controller
 *
 * @property \App\Model\Table\AdsTable $Ads
 */
class AdsController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($category = false) {
        if ($category) {
            $conditions = ["Ads.category_id != ''"];
        } else {
            $conditions = ["(Ads.category_id = '' OR Ads.category_id IS NULL)"];
        }
        $filters = ['category_id', 'location'];
        foreach ($filters as $filter) {
            if ($this->request->query($filter)) {
                $conditions['Ads.' . $filter] = $this->request->query($filter);
            }
        }
        $this->paginate = [
            'conditions' => $conditions
        ];


        $ads = $this->paginate($this->Ads);
        $categories = $this->Ads->Categories->GetCategoriesList();
        $this->set('locations', $this->Ads->locations);
        $this->set('category', $category);
        $this->set(compact('ads', 'categories'));
        $this->set('_serialize', ['ads']);
    }

    /**
     * View method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $ad = $this->Ads->get($id, [
            'contain' => []
        ]);

        $this->set('ad', $ad);
        $this->set('_serialize', ['ad']);
        $this->set('locations', $this->Ads->locations);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($location = false) {
        $ad = $this->Ads->newEntity();
        $params = $this->request->params;
        $category = false;
        if (isset($params['category']) && !isset($params['location']) && isset($this->Ads->image_settings[$params['category']])) {
            $ad->location = $params['category'];
            $this->Ads->behaviors()->get('Image')->Setup($this->Ads->image_settings[$ad->location]);
        } elseif (isset($params['location'])) {
            $ad->location = $params['location'];
            $this->Ads->behaviors()->get('Image')->Setup($this->Ads->image_settings[$ad->location]);
        } elseif ($location) {
            $ad->location = $location;
            $this->Ads->behaviors()->get('Image')->Setup($this->Ads->image_settings[$ad->location]);
        }
        if (isset($params['category'])) {
            $category = true;
        }


        if ($this->request->is('post')) {
            $ad = $this->Ads->patchEntity($ad, $this->request->data);
            if ($this->Ads->save($ad)) {
                $this->Flash->success(__('The ad has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ad could not be saved. Please, try again.'));
            }
        }

        $this->set('imageSettings', $this->Ads->behaviors()->get('Image')->getImageSettings());
        $categories = $this->Ads->Categories->GetCategoriesList();
        $this->set(compact('ad', 'categories'));
        $this->set('category', $category);
        $this->set('_serialize', ['ad']);
        $this->set('locations', $this->Ads->locations);
//        $this->set('location', $location);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $ad = $this->Ads->get($id, [
            'contain' => []
        ]);
        $this->Ads->behaviors()->get('Image')->Setup($this->Ads->image_settings[$ad->location]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ad = $this->Ads->patchEntity($ad, $this->request->data);
            if ($this->Ads->save($ad)) {
                $this->Flash->success(__('The ad has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The ad could not be saved. Please, try again.'));
            }
        }
        $this->set('imageSettings', $this->Ads->behaviors()->get('Image')->getImageSettings());
        $categories = $this->Ads->Categories->GetCategoriesList();
        $this->set(compact('ad', 'categories'));
        $this->set('_serialize', ['ad']);
        $this->set('locations', $this->Ads->locations);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Ad id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $ad = $this->Ads->get($id);
        if ($this->Ads->delete($ad)) {
            $this->Flash->success(__('The ad has been deleted.'));
        } else {
            $this->Flash->error(__('The ad could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
