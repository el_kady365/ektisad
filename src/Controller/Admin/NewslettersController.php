<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * Newsletters Controller
 *
 * @property \App\Model\Table\NewslettersTable $Newsletters
 */
class NewslettersController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($type) {
//        debug($this->Auth->user());
        $this->paginate = ['conditions' => ['Newsletters.type' => $type]];
        $newsletters = $this->paginate($this->Newsletters);
        $crumbs['controller'] = [
            'Subscriptions' => ['controller' => 'newsletters', 'action' => 'index', $type],
        ];
        $this->set('type', $type);
        $this->set(compact('newsletters', 'crumbs'));
        $this->set('_serialize', ['newsletters']);
    }

    /**
     * View method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $newsletter = $this->Newsletters->get($id, [
            'contain' => []
        ]);

        $this->set('newsletter', $newsletter);
        $this->set('_serialize', ['newsletter']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $newsletter = $this->Newsletters->newEntity();
        if ($this->request->is('post')) {
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->data);
            if ($this->Newsletters->save($newsletter)) {
                $this->Flash->success(__('The newsletter has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The newsletter could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $newsletter = $this->Newsletters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->data);
            if ($this->Newsletters->save($newsletter)) {
                $this->Flash->success(__('The newsletter has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The newsletter could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Newsletter id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $newsletter = $this->Newsletters->get($id);
        if ($this->Newsletters->delete($newsletter)) {
            $this->Flash->success(__('The newsletter has been deleted.'));
        } else {
            $this->Flash->error(__('The newsletter could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    function doOperation() {
//        debug($this->request->data);
//        exit;
        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->Newsletters->deleteAll(['Newsletters.id IN' => $ids])) {
                $this->Flash->success(__('Newsletters deleted successfully'));
            } else {
                $this->Flash->error(__('Newsletters can not be deleted'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
