<?php
namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * Polls Controller
 *
 * @property \App\Model\Table\PollsTable $Polls
 */
class PollsController extends AdminController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $polls = $this->paginate($this->Polls);

        $this->set(compact('polls'));
        $this->set('_serialize', ['polls']);
    }

    /**
     * View method
     *
     * @param string|null $id Poll id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $poll = $this->Polls->get($id, [
            'contain' => ['PollAnswers', 'PollQuestions']
        ]);

        $this->set('poll', $poll);
        $this->set('_serialize', ['poll']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $poll = $this->Polls->newEntity();
        if ($this->request->is('post')) {
            $poll = $this->Polls->patchEntity($poll, $this->request->data);
            if ($this->Polls->save($poll)) {
                $this->Flash->success(__('The poll has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The poll could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('poll'));
        $this->set('_serialize', ['poll']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Poll id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $poll = $this->Polls->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $poll = $this->Polls->patchEntity($poll, $this->request->data);
            if ($this->Polls->save($poll)) {
                $this->Flash->success(__('The poll has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The poll could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('poll'));
        $this->set('_serialize', ['poll']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Poll id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
//        $this->request->allowMethod(['post', 'delete']);
        $poll = $this->Polls->get($id);
        if ($this->Polls->delete($poll)) {
            $this->Flash->success(__('The poll has been deleted.'));
        } else {
            $this->Flash->error(__('The poll could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
