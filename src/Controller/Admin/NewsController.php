<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;
use Cake\ORM\TableRegistry;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 */
class NewsController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {

        $conditions = [];
        $order = [];
        $filters = ['category_id', 'editor_id', 'published'];
        foreach ($filters as $filter) {
            if (isset($_GET[$filter]) && $_GET[$filter] != NULL) {
                $conditions['News.' . $filter] = $_GET[$filter];
            }
        }
        if ($this->loggedUser['group_id'] != 1) {
            if ($this->loggedUser['group_id'] == 4) {
                $conditions['News.editor_id'] = $this->loggedUser['id'];
                $conditions['News.assigned_to'] = 4;
            }
        }
        $today = new \Cake\I18n\Time();
        $now = $today->format('Y-m-d H:i:s');
        $this->set('loadTime', $now);

        $order[] = 'News.created desc';

        $this->paginate = [
            'contain' => ['Categories', 'Editors', 'Groups'],
            'conditions' => $conditions,
            'order' => $order
        ];
        $news = $this->paginate($this->News);
        $categories = $this->News->Categories->GetCategoriesList();
        $editors = $this->News->Editors->find('list', ['valueField' => 'username',
            'keyField' => 'id', 'limit' => 200]);
        $reviewers = $this->News->Reviewers->find('list', ['valueField' => 'username',
            'keyField' => 'id', 'limit' => 200]);
        $publishers = $this->News->Publishers->find('list', ['valueField' => 'username',
            'keyField' => 'id', 'limit' => 200]);
        $this->set(compact('news', 'categories', 'editors', 'reviewers', 'publishers'));
        $this->set('_serialize', ['news']);
    }

    /**
     * View method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $news = $this->News->get($id, [
            'contain' => ['Categories', 'Editors', 'Reviewers', 'Publishers', 'NewsImages', 'Writers']
        ]);

        $this->set('news', $news);
        $this->set('_serialize', ['news']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $news = $this->News->newEntity();

        if ($this->request->is('post')) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos', 'NewsNotes']]);
            $news->editor_id = $this->loggedUser['id'];
            if ($news->reviewed) {
                $news->reviewer_id = $this->loggedUser['id'];
            }
            if ($news->published) {
                $news->publisher_id = $this->loggedUser['id'];
                $news->assigned_to = $this->loggedUser['group_id'];
                if (empty($article->publish_date)) {
                    $news->publish_date = date('Y-m-d H:i:s');
                    $news->last_updated_date = date('Y-m-d H:i:s');
                } else {
                    $news->last_updated_date = $article->publish_date;
                }
            }
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The news could not be saved. Please, try again.'));
            }
        }
        $this->set('imageSettings', $this->News->NewsImages->behaviors()->get('Image')->getImageSettings());
        $this->set('newsSettings', $this->News->behaviors()->get('Image')->getImageSettings());
        $categories = $this->News->Categories->GetCategoriesList();
        $editors = $this->News->Editors->find('list', ['limit' => 200]);
        $reviewers = $this->News->Reviewers->find('list', ['limit' => 200]);
        $publishers = $this->News->Publishers->find('list', ['limit' => 200]);
        $writers = $this->News->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id', 'limit' => 200, 'conditions' => ['Writers.active' => 1]]);
        $this->set(compact('news', 'categories', 'editors', 'reviewers', 'publishers', 'writers'));
        $this->set('_serialize', ['news']);
    }

    /**
     * Edit method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $news = $this->News->get($id, [
            'contain' => ['NewsImages', 'NewsVideos', 'NewsNotes.Users', 'Groups']
        ]);
        if ($news->editor_id != $this->loggedUser['id'] && !in_array($this->loggedUser['group_id'], [1, 2, 3])) {
            $this->Flash->error(__("You Don't have the right permission"));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos', 'NewsNotes']]);

            if ($news->getOriginal('reviewed') == false && $news->reviewed) {
                $news->reviewer_id = $this->loggedUser['id'];
            }
            if ($news->getOriginal('published') == false && $news->published) {
                $news->publisher_id = $this->loggedUser['id'];
                $news->publish_date = date('Y-m-d H:i:s');
            }
            $news->last_updated_date = date('Y-m-d H:i:s');
//            debug($news->errors());
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The news could not be saved. Please, try again.'));
            }
        }
        $this->set('imageSettings', $this->News->NewsImages->behaviors()->get('Image')->getImageSettings());
        $this->set('newsSettings', $this->News->behaviors()->get('Image')->getImageSettings());
        $categories = $this->News->Categories->GetCategoriesList();
        $writers = $this->News->Writers->find('list', ['valueField' => 'ar_title',
            'keyField' => 'id', 'limit' => 200, 'conditions' => ['Writers.active' => 1]]);
        $editors = $this->News->Editors->find('list', ['limit' => 200]);
        $reviewers = $this->News->Reviewers->find('list', ['limit' => 200]);
        $publishers = $this->News->Publishers->find('list', ['limit' => 200]);
        $this->set(compact('news', 'categories', 'editors', 'reviewers', 'publishers', 'writers', 'newsNotes'));
        $this->set('_serialize', ['news']);
        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id News id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $news = $this->News->get($id);
        if ($this->News->delete($news)) {
            $this->Flash->success(__('The news has been deleted.'));
        } else {
            $this->Flash->error(__('The news could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    function doOperation() {
//        debug($this->request->data);
//        exit;
        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->News->deleteAll(['News.id IN' => $ids])) {
                $this->Flash->success(__('News deleted successfully', true));
            } else {
                $this->Flash->error(__('News can not be deleted', true));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

    public function review($id) {
        $news = $this->News->get($id);
        $news->reviewed = true;
        $news->reviewer_id = $this->loggedUser['id'];
        if ($this->News->save($news)) {
            $this->Flash->success(__('The news has been reviewed successfully.'));
        } else {
            $this->Flash->error(__('The news could not be reviewed. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function publish($id) {
        $news = $this->News->get($id);
        if ($news->published) {
            $news->published = false;
        } else {
            $news->published = true;
            $news->publisher_id = $this->loggedUser['id'];
            $news->publish_date = date('Y-m-d H:i:s');
            $news->last_updated_date = date('Y-m-d H:i:s');
        }


        if ($this->News->save($news)) {
            if ($news->published) {
                $this->Flash->success(__('The news has been published successfully.'));
            } else {
                $this->Flash->success(__('The news has been un published successfully.'));
            }
        } else {
            $this->Flash->error(__('The news could not be published. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function sendToReviewers($id = false) {
        if (!$id) {
            $news = $this->News->newEntity();
        } else {
            $news = $this->News->get($id, [
                'contain' => ['NewsImages', 'NewsVideos']
            ]);
        }
        $today = new \Cake\I18n\Time();
        $now = $today->format('Y-m-d H:i:s');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos']]);
            if ($this->loggedUser['group_id'] == 4) {
                $news->editor_id = $this->loggedUser['id'];
            }
            $news->assigned_to = 3;

            if ($news->getOriginal('assigned_to') == 4 && $news->assigned_to == 3) {
                $news->send_to_up_date = $now;
            }

            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $this->Flash->error(__('The news could not be saved. Please, try again.'));
                return $this->redirect($this->referer());
            }
        }
        if ($this->request->is(['get'])) {
            if ($news->getOriginal('assigned_to') == 4) {
                $news->send_to_up_date = $now;
            }
            $news->assigned_to = 3;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function sendToDesks($id = false) {
        if (!$id) {
            $news = $this->News->newEntity();
        } else {
            $news = $this->News->get($id, [
                'contain' => ['NewsImages', 'NewsVideos']
            ]);
        }
        $today = new \Cake\I18n\Time();
        $now = $today->format('Y-m-d H:i:s');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos']]);
            $news->assigned_to = 2;

            $news->send_to_up_date = $now;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $this->Flash->error(__('The news could not be saved. Please, try again.'));
                return $this->redirect($this->referer());
            }
        }
        if ($this->request->is(['get'])) {
            $news->assigned_to = 2;
            $news->send_to_up_date = $now;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function returnToEditors($id = false) {
        if (!$id) {
            $news = $this->News->newEntity();
        } else {
            $news = $this->News->get($id, [
                'contain' => ['NewsImages', 'NewsVideos']
            ]);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos']]);
            $news->assigned_to = 4;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $this->Flash->error(__('The news could not be saved. Please, try again.'));
                return $this->redirect($this->referer());
            }
        }

        if ($this->request->is(['get'])) {
            $news->assigned_to = 4;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function publishForm($id = false) {

        $news = $this->News->get($id, [
            'contain' => ['NewsImages', 'NewsVideos']
        ]);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $news = $this->News->patchEntity($news, $this->request->data, ['associated' => ['NewsImages', 'NewsVideos']]);
            if ($news->getOriginal('published') == true) {
                $news->published = 0;
            } else {
                $news->published = 1;
            }
           // debug($news);
            //exit;
            if ($this->News->save($news)) {
                $this->Flash->success(__('The news has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {

                $this->Flash->error(__('The news could not be saved. Please, try again.'));
                return $this->redirect($this->referer());
            }
        }
    }

    public function getUnLoadedNews() {
        $date = $this->request->query('date');
        $news = $this->News->find()->where(['News.created >' => $date])->count();
        $this->set(compact('news'));
        $this->set('_serialize', ['news']);
    }

}
