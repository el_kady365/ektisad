<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * NewsImages Controller
 *
 * @property \App\Model\Table\NewsImagesTable $NewsImages
 */
class NewsImagesController extends AppController
{

    /**
     * Delete method
     *
     * @param string|null $id News Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $newsImage = $this->NewsImages->get($id);
        if ($this->NewsImages->delete($newsImage)) {
            $this->Flash->success(__('The news image has been deleted.'));
        } else {
            $this->Flash->error(__('The news image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    function doOperation() {

        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->NewsImages->deleteAll(['NewsImages.id IN' => $ids])) {
                $this->Flash->success(__('newsImages deleted successfully'));
            } else {
                $this->Flash->error(__('newsImages can not be deleted'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }
}
