<?php

namespace App\Controller\Admin;

use App\Controller\AdminController;

/**
 * PollQuestions Controller
 *
 * @property \App\Model\Table\PollQuestionsTable $PollQuestions
 */
class PollQuestionsController extends AdminController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $this->paginate = [
            'order' => ['PollQuestions.active desc']
        ];
        $pollQuestions = $this->paginate($this->PollQuestions);

        $this->set(compact('pollQuestions'));
        $this->set('_serialize', ['pollQuestions']);

        $crumbs['controller'] = [
            'Poll Questions' => ['controller' => 'poll_questions', 'action' => 'index']
        ];
        $this->set('crumbs', $crumbs);
    }

    /**
     * View method
     *
     * @param string|null $id Poll Question id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $pollQuestion = $this->PollQuestions->get($id, [
            'contain' => ['Polls', 'PollQuestionOptions']
        ]);

        $this->set('pollQuestion', $pollQuestion);
        $this->set('_serialize', ['pollQuestion']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $pollQuestion = $this->PollQuestions->newEntity();
        if ($this->request->is('post')) {
            $pollQuestion = $this->PollQuestions->patchEntity($pollQuestion, $this->request->data, ['associated' => ['PollQuestionOptions']]);
            $pollQuestion->required = 1;
            if ($this->PollQuestions->save($pollQuestion)) {
                $this->Flash->success(__('The poll question has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The poll question could not be saved. Please, try again.'));
            }
        }
        $crumbs['controller'] = [
            'Poll Questions' => ['controller' => 'poll_questions', 'action' => 'index']
        ];
        $this->set('crumbs', $crumbs);
        $this->set(compact('pollQuestion'));
        $this->set('_serialize', ['pollQuestion']);
        $this->set('crumbs', $crumbs);
    }

    /**
     * Edit method
     *
     * @param string|null $id Poll Question id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $pollQuestion = $this->PollQuestions->get($id, [
            'contain' => ['PollQuestionOptions']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pollQuestion = $this->PollQuestions->patchEntity($pollQuestion, $this->request->data, ['associated' => ['PollQuestionOptions']]);
            $pollQuestion->required = 1;
            if ($this->PollQuestions->save($pollQuestion)) {
                $this->Flash->success(__('The poll question has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The poll question could not be saved. Please, try again.'));
            }
        }
        $crumbs['controller'] = [
            'Poll Questions' => ['controller' => 'poll_questions', 'action' => 'index']
        ];
        $this->set('crumbs', $crumbs);
        $this->set(compact('pollQuestion'));
        $this->set('_serialize', ['pollQuestion']);



        $this->render('add');
    }

    /**
     * Delete method
     *
     * @param string|null $id Poll Question id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null) {
//        $this->request->allowMethod(['post', 'delete']);
        $pollQuestion = $this->PollQuestions->get($id);
        if ($this->PollQuestions->delete($pollQuestion)) {
            $this->Flash->success(__('The poll question has been deleted.'));
        } else {
            $this->Flash->error(__('The poll question could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function activate($id) {
        $pollQuestion = $this->PollQuestions->get($id);
        if ($pollQuestion->active) {
            $pollQuestion->set('active', 0);
        } else {
            $pollQuestion->set('active', 1);
            $this->PollQuestions->updateAll(['active' => 0], ['id !=' => $id]);
        }

        $this->PollQuestions->save($pollQuestion);
        return $this->redirect(['action' => 'index']);
    }

    function doOperation() {
//        debug($this->request->data);
//        exit;
        $ids = $this->request->data['chk'];
        $operation = $this->request->data['operation'];
        if ($operation == 'delete') {
            if ($this->PollQuestions->deleteAll(['PollQuestions.id IN' => $ids])) {
                $this->Flash->success(__('PollQuestions deleted successfully'));
            } else {
                $this->Flash->error(__('PollQuestions can not be deleted'));
            }
        }
        $this->redirect(array('action' => 'index'));
    }

}
