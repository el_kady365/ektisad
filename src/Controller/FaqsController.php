<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Faqs Controller
 *
 * @property \App\Model\Table\FaqsTable $Faqs
 */
class FaqsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $faqs = $this->Faqs->find()->where(['Faqs.active' => 1])->orderAsc('display_order')->all();

        $this->set(compact('faqs'));
        $this->set('title_for_layout', __('Frequently Asked Questions'));
        $this->set('_serialize', ['faqs']);
    }

}
