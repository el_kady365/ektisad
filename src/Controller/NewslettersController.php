<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Newsletters Controller
 *
 * @property \App\Model\Table\NewslettersTable $Newsletters
 */
class NewslettersController extends AppController {

    public function addPrinted() {
        $newsletter = $this->Newsletters->newEntity();
        if ($this->request->is('post')) {
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->data);
            $newsletter->type = 2;
            if ($this->Newsletters->save($newsletter)) {
                $this->Flash->success(__('You have subscribed in the printed version successfully'));
                return $this->redirect(['action' => 'add-printed']);
            } else {
                $this->Flash->error(__("you can't subscribe in the printed version, try again"));
            }
        }
        $this->set(compact('newsletter'));
        $this->set('_serialize', ['newsletter']);
        $this->set('title_for_layout', __('Printed version subscription'));
    }

    public function add() {
        $newsletter = $this->Newsletters->newEntity();
        $return = [];
        if ($this->request->is('ajax')) {
            $newsletter = $this->Newsletters->patchEntity($newsletter, $this->request->data);
            $newsletter->type = 1;
            if ($this->Newsletters->save($newsletter)) {
                $return['status'] = 1;
                $return['message'] = __('Thank you for your subscription in newsletter');
            } else {
                $return['status'] = 0;
                $return['message'] = __("Sorry you can't subscribe. Please try again");
                $return['errors'] = $newsletter->errors();
            }
        }

        $this->set(compact('return'));
        $this->set('_serialize', ['return']);
        $this->set('title_for_layout', __('Printed version subscription'));
    }

}
