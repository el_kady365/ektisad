<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $this->paginate = [
            'contain' => ['Writers'],
            'conditions' => [
                'Articles.published' => 1,
                'Articles.publish_date <= ' => $today,
            ],
            'order' => ['Articles.publish_date desc'],
            'limit' => 10
        ];


        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('title_for_layout', __('Articles'));
        $this->set('_serialize', ['articles']);
    }

    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $article = $this->Articles->find('all', [
                    'contain' => ['Writers'],
                    'conditions' => [
                        'Articles.id' => $id,
                        'Articles.published' => 1,
                        'Articles.publish_date <= ' => $today,
                    ]
                ])->firstOrFail();

        $this->set('article', $article);
        $this->set('title_for_layout', $article->{$this->lang . '_title'});
        $this->set('_serialize', ['article']);
    }

}
