<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Writers Controller
 *
 * @property \App\Model\Table\WritersTable $Writers
 */
class WritersController extends AppController {

    /**
     * View method
     *
     * @param string|null $id Writer id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {
        $writer = $this->Writers->get($id, [
            'contain' => ['Articles']
        ]);
        $today = new \Cake\I18n\Time();
        $today = $today->format('Y-m-d H:i:s');
        $this->paginate['Articles'] = [
            'contain' => ['Writers'],
            'conditions' => [
                'Articles.published' => 1,
                'Articles.writer_id' => $id,
                'Articles.publish_date <= ' => $today,
            ],
            'order' => ['Articles.publish_date desc'],
            'limit' => 10
        ];


        $articles = $this->paginate('Articles');

        $this->set(compact('articles'));
        $this->set('writer', $writer);
        $this->set('_serialize', ['writer']);
        $this->set('title_for_layout', __('Articles') . ' - ' . $writer->{$this->lang . '_title'});
    }

}
