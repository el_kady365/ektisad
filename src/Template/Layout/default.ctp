<?php
$page_title = $config['site_name_' . $lang] . ' - ' . $title_for_layout;
$this->assign('title', $page_title);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php if (isset($meta_description)) { ?>
            <meta content="<?php echo strip_tags($meta_description) ?>" name="description"  />
        <?php } ?>
        <?php if (isset($og_title)) { ?>
            <meta content="<?php echo $og_title ?>" property="og:title"/>
        <?php } ?>
        <meta content="article" property="og:type"/>
        <?php if (isset($og_description)) { ?>
            <meta content="<?php echo strip_tags($og_description) ?>" property="og:description" />
        <?php } ?>

        <?php if (isset($og_image)) { ?>
            <meta content="<?php echo $og_image ?>" property="og:image" />
        <?php } ?>
        <?php echo $this->fetch('meta') ?>
        <title><?php echo $this->fetch('title') ?></title>

        <?php echo $this->Html->css(array('bootstrap')) ?>
        <link rel="stylesheet" href="<?php echo $this->Url->build('/css/' . $lang . '.css') ?>">
        <?php echo $this->fetch('css') ?>
        <?php echo $this->Html->css(array('templatemo_misc', 'templatemo_style')) ?>
        <?php echo $this->Html->css(array('general', 'poll', 'font-awesome.min', 'li-scroller', 'theme', 'news-style', 'style')) ?>



        <!--[if lt IE 9]>
        <script src="<?php echo $this->Url->build('/js/html5shiv.min.js') ?>"></script>
        <script src="<?php echo $this->Url->build('/js/respond.min.js') ?>"></script>
        <![endif]-->

    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/<?php echo $locale ?>/sdk.js#xfbml=1&version=v2.7&appId=636992799782940";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <div class="the-wrapper">
            <div class="the-top top-one">
                <div class="box_wrapper">
                    <!-- start news sticker -->
                    <div class="latest_newsarea">      
                        <span><div class="stockkk">الأسهم المصرية</div></span>
                        <ul id="ticker01" class="news_sticker">
                            <li><a href="#">  كفر الزيات للمبيدات و الكيماويات 18.90</a><div class="down">1.39 %</div></li> 
                            <li><a href="#"> مجموعة جى للتنمية العقارية 11.70</a><div class="up">-0.32 %</div></li>
                            <li><a href="#">  كفر الزيات للمبيدات و الكيماويات 18.90</a><div class="down">1.39 %</div></li> 
                            <li><a href="#"> مجموعة جى للتنمية العقارية 11.70</a><div class="up">-0.32 %</div></li>
                            <li><a href="#">  كفر الزيات للمبيدات و الكيماويات 18.90</a><div class="down">1.39 %</div></li> 
                            <li><a href="#"> مجموعة جى للتنمية العقارية 11.70</a><div class="up">-0.32 %</div></li>
                            <li><a href="#">  كفر الزيات للمبيدات و الكيماويات 18.90</a><div class="down">1.39 %</div></li> 
                            <li><a href="#"> مجموعة جى للتنمية العقارية 11.70</a><div class="up">-0.32 %</div></li>
                            <li><a href="#">  كفر الزيات للمبيدات و الكيماويات 18.90</a><div class="down">1.39 %</div></li> 

                        </ul>    
                    </div>
                    <!-- End news sticker -->


                </div><!-- /.box_wrapper -->
                <p class="group">برعاية مجموعة اتصالات</p>
            </div><!-- /.container -->


            <!-- start logo div-->
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-3">
                        <div class="sms">
                            <div id="sms-slide" class="carousel slide" data-ride="carousel">

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <div>
                                            <p>ارسل 3030</p>
                                            <div>
                                                <span>الى 1666</span>
                                                <img src="<?php echo $this->Url->build('/css/img/Etisalat.png') ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div style="color:#ff0017">
                                            <p>ارسل 1515</p>
                                            <div>
                                                <span>الى 3838</span>
                                                <img src="<?php echo $this->Url->build('/css/img/Vodafone.png') ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div style="color:#ff6600">
                                            <p>ارسل 2929</p>
                                            <div>
                                                <span>الى 4040</span>
                                                <img src="<?php echo $this->Url->build('/css/img/Orange.png') ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--                        <div class="head-visitor">
                                                    <p>عدد الزوار</p>
                                                    <p>14852828223356</p>
                                                </div>-->
                    </div>
                    <div class="col-lg-7 col-sm-6">
                        <div class="banner1">
                            <?php
                            $header_ads = $ads->filter(function($row) {
                                return $row->location == 'header';
                            });
                            if (!$header_ads->isEmpty()) {
                                ?>
                                <div id="the-banner1" class="carousel slide" data-ride="carousel">
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                        $c1 = 0;
                                        foreach ($header_ads as $header_ad) {
                                            ?>

                                            <div class="item <?php echo $c1++ == 0 ? "active" : ''; ?>">
                                                <a href = "<?php echo $header_ad->url; ?>" target="_blank"><img class="item-slider-img" src="<?php echo $header_ad->image_info['path']; ?>"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class = "the-logo">
                            <a href="<?php echo \Cake\Routing\Router::url('/') ?>">
                                <img src="<?php echo $this->Url->build('/css/img/Logo-02.png') ?>">
                            </a>

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="search">
                            <form id="SearchForm" action="<?php echo $this->Url->build('/search'); ?>" method="get">
                                <input type="text" name="s" placeholder="<?php echo __('Search here') ?>">
                            </form>
                        </div>
                        <div class="social">
                            <ul>
                                <li>
                                    <?php if (!empty($config['facebook'])) { ?>
                                        <a href="<?php echo $config['facebook'] ?>"><img src="<?php echo $this->Url->build('/css/img/facebook.png') ?>"></a></li>
                                    <?php
                                }
                                if (!empty($config['twitter'])) {
                                    ?>
                                    <li><a href="<?php echo $config['twitter'] ?>"><img src="<?php echo $this->Url->build('/css/img/twitter.png') ?>"></a></li>
                                    <?php
                                }
                                if (!empty($config['linkedin'])) {
                                    ?>
                                    <li><a href="<?php echo $config['linkedin'] ?>"><img src="<?php echo $this->Url->build('/css/img/linkedin.png') ?>"></a></li>
                                    <?php
                                }
                                if (!empty($config['youtube'])) {
                                    ?>
                                    <li><a href="<?php echo $config['youtube'] ?>"><img src="<?php echo $this->Url->build('/css/img/youtube.png') ?>"></a></li>
                                    <?php
                                }
                                if (!empty($config['google_plus'])) {
                                    ?>
                                    <li><a href="<?php echo $config['google_plus'] ?>"><img src="<?php echo $this->Url->build('/css/img/google.png') ?>"></a></li>
                                <?php }
                                ?>
                                <li><a href="<?php echo $this->Url->build(['controller' => 'news', 'language' => $lang, '_ext' => 'rss']) ?>"><img src="<?php echo $this->Url->build('/css/img/rss.png') ?>"></a></li>
                            </ul>
                        </div>
                        <div class="lang">
                            <?php if ($lang != 'ar') { ?>
                                <a href="<?php echo $this->Url->build(['action' => 'change_lang', 'ar']) ?>" class="<?php echo ($lang == 'ar') ? 'active' : ''; ?> ar">عربى</a>
                            <?php } else { ?>
                                <a href="<?php echo $this->Url->build(['action' => 'change_lang', 'en']) ?>" class=" <?php echo ($lang == 'en') ? 'active' : ''; ?> eng">English</a>
                            <?php } ?>
                        </div>
                        <div class="head-data">
                            <p>
                                <?php
                                $date = new \Cake\I18n\Time();
                                $time = $date->i18nFormat('hh:m a');
                                ?>
                                <i class="fa fa-calendar-check-o"></i>  
                                <?php
                                echo $date->i18nFormat('EEEE dd LLLL Y');
                                ?> - <?php
                                $date->addDay($config['hijri_correction']);
                                echo $date->i18nFormat('dd LLLL Y ', null, $locale . '@calendar=islamic')
                                ?>
                                <!--                                <br /> 
                                                                <i class="fa fa-clock-o"></i> -->
                                <?php // echo $time ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end logo div-->
            <!-- start nav -->
            <div class="my-nav">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span> 
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <?php echo $this->element('top-nav'); ?>
                        </div>
                    </div>
                </nav>
            </div>
            <?php if (!$latest_news->isEmpty()) { ?>
                <div class="the-top top-two">
                    <div class="box_wrapper">
                        <!-- start news sticker -->
                        <div class="latest_newsarea">      
                            <span><?php echo __('Latest News') ?></span>
                            <ul id="ticker01" class="news_sticker">
                                <?php foreach ($latest_news as $latest_new) { ?>
                                    <li><a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $latest_new->id, $latest_new->{$lang . '_permalink'}]) ?>"><?php echo $latest_new->{$lang . '_title'} ?></a></li> 
                                <?php } ?>

                            </ul>    
                        </div>
                        <!-- End news sticker -->


                    </div><!-- /.box_wrapper -->
                </div><!-- /.container -->
            <?php } ?>
            <!-- start -->
            <!-- end nav -->
            <div class="clearfix"></div>
            <!-- start news slider -->

            <!-- start news slider -->
            <div class="clearfix"></div>
            <!-- start content -->
            <?php
            $right_ads = $ads->filter(function($row) {
                        return $row->location == 'banner_right';
                    })->first();
            $left_ads = $ads->filter(function($row) {
                        return $row->location == 'banner_left';
                    })->first();

            if (isset($featured_news) && !$featured_news->isEmpty()) {
                $container_div = "col-sm-8";
                ?>
                <div class="container-fluid slider-div">


                    <div class="col-sm-1 adv1 visible-lg">
                        <?php if ($right_ads) { ?>
                            <div class="banner2">
                                <a href = "<?php echo $right_ads->url; ?>" target="_blank"><img class="banner-1" src="<?php echo $right_ads->image_info['path']; ?>"></a>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                    echo $this->element('home-slider');
                    ?>

                    <div class="col-sm-1 adv2 visible-lg">
                        <?php if ($left_ads) { ?>
                            <div class="banner2">
                                <a href = "<?php echo $left_ads->url; ?>" target="_blank"><img class="banner-2" src="<?php echo $left_ads->image_info['path']; ?>"></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div class="<?php echo!isset($featured_news) || $featured_news->isEmpty() ? "container-fluid margin-top-30" : 'container'; ?>">
                <?php
                if (!isset($featured_news) || $featured_news->isEmpty()) {
                    $container_div = "col-sm-8 col-lg-6 one-new";
                    ?>

                    <div class="col-sm-1 adv1 visible-lg">
                        <?php if ($right_ads) { ?>
                            <div class="banner2">
                                <a href = "<?php echo $right_ads->url; ?>" target="_blank"><img class="banner-1" src="<?php echo $right_ads->image_info['path']; ?>"></a>
                            </div>
                        <?php } ?>
                    </div>
                    <?php
                }
                ?>



                <div class="<?php echo $container_div ?>">
                    <?php
                    $content_ads = $ads->filter(function($row) {
                        return $row->location == 'banner_bottom';
                    });
                    if (!$content_ads->isEmpty() && isset($featured_news) && !$featured_news->isEmpty()) {
                        ?>
                        <div class="banner1 my-padding">
                            <div id="the-banner1" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <?php
                                    $c2 = 0;
                                    foreach ($content_ads as $content_ad) {
                                        ?>

                                        <div class="item <?php echo $c2++ == 0 ? "active" : ''; ?>">
                                            <a href = "<?php echo $content_ad->url; ?>" target="_blank"><img src="<?php echo $content_ad->image_info['path']; ?>"></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>




                    <?php echo $this->Flash->render(); ?>

                    <?php echo $this->fetch('content'); ?>
                </div>
                <div class="col-sm-4">
                    <?php echo $this->element('sidebar'); ?>
                </div>
                <?php if (!isset($featured_news) || $featured_news->isEmpty()) {
                    ?>
                    <div class="col-sm-1 adv1 visible-lg">
                        <?php if ($left_ads) { ?>
                            <div class="banner2">
                                <a href = "<?php echo $left_ads->url; ?>" target="_blank"><img class="banner-2" src="<?php echo $left_ads->image_info['path']; ?>"></a>
                            </div>
                        <?php } ?>
                    </div>
                <?php }
                ?>
            </div>
            <!-- end content -->
            <div class="clearfix"></div>
            <footer>
                <div class="col-sm-8 col-xs-12">
                    <ul class="pull-right">
                        <li><a href="<?php echo $this->Url->build(['controller' => 'faqs']) ?>"><?php echo __('Frequently Asked Questions') ?></a></li>
                        <li><a href="<?php echo $this->Url->build('/contact-us') ?>"><?php echo __('Contact us') ?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'contacts', 'action' => 'advertise-with-us']) ?>"><?php echo __('Advertise with us') ?></a></li>
                        <li><a href="<?php echo $this->Url->build('/suggestions') ?>"><?php echo __('Suggestions') ?></a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'view', 18]) ?>"><?php echo __('About us') ?></a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-xs-12 text-center">
                    <p><?php echo __('All Rights Reserved') ?> @ <?php echo date('Y') ?></p>
                </div>
            </footer>

        </div>
        <!-- end -->



        <?php echo $this->Html->script(['jquery.min', 'bootstrap.min', 'jquery.li-scroller.1.0', 'custom', 'code', 'poll-results']); ?>
        <?php echo $this->Html->script(array('jquery.lightbox.js', 'templatemo_custom')) ?>
        <?php echo $this->fetch('script'); ?>


        <script src="https://code.highcharts.com/stock/highstock.js"></script>
        <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>

        <script>
            var request_url = '<?php echo $this->Url->build(); ?>';
            var $v = $("#SearchForm").validate({
                errorClass: "error-message",
                'ignore': [],
                errorElement: "div",
                rules: {
                    s: {
                        required: true,
                        minlength: 3
                    }
                },
                messages: {
                    s: {
                        required: '<?php echo __('Required', true) ?>',
                        minlength: '<?php echo __('This field should not less than {0} characters', 3) ?>',
                    }
                },
                success: function (label, element) {
                    label.parent().removeClass('has-errors');
                    label.remove();
                },
                errorPlacement: function (label, element) {
                    // position error label after generated textarea
                    label.insertAfter(element);
                }
            });
            $(function () {
                $('ul.nav li a').each(function () {
                    var url = decodeURI($(this).attr('href'));
                    if (decodeURI(request_url) == url) {
                        $(this).parent().addClass('active');
                    }
                });
            });


            $(function () {
                $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

                    $('#container1').highcharts({
                        chart: {
                            zoomType: 'x'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },
                        series: [{
                                type: 'area',
                                name: 'USD to EUR',
                                data: data
                            }]
                    });
                });
            });
        </script>
        <script>
            $(function () {
                $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

                    $('#container2').highcharts({
                        chart: {
                            zoomType: 'x'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },
                        series: [{
                                type: 'area',
                                name: 'USD to EUR',
                                data: data
                            }]
                    });
                });
            });
        </script>
        <script>
            $(function () {
                $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

                    $('#container3').highcharts({
                        chart: {
                            zoomType: 'x'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },
                        series: [{
                                type: 'area',
                                name: 'USD to EUR',
                                data: data
                            }]
                    });
                });
            });
        </script>
        <script>
            $(function () {
                $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=usdeur.json&callback=?', function (data) {

                    $('#container4').highcharts({
                        chart: {
                            zoomType: 'x'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },
                        series: [{
                                type: 'area',
                                name: 'USD to EUR',
                                data: data
                            }]
                    });
                });
            });
        </script> 
        <script>
            $(document).ready(function () {
                "use strict";
                //smooth scroll to div

                $('.the-result').click(function () {
                    $('.the-vote').hide();
                });

                $('.vote-step').click(function () {
                    $(".poll-results-list").hide();
                    $(".poll-vote-list").fadeIn();
                    $(".the-vote").fadeIn();
                    $(".the-result").fadeIn();
                });
                $("ul#ticker01").liScroll({
<?php
if ($lang == 'ar') {
    echo 'direction: "right"';
}else{
    echo 'direction: "left"';
}
?>

                });
            });
            $(window).on("load scroll", function () {
                if ($(this).scrollTop() >= 300) {
                    $(".banner-2")
                            .css({"position": "fixed", "top": "50px", "width": "80px", "height": "470px", "left": "1px"});
                    $(".banner-1")
                            .css({"position": "fixed", "top": "50px", "width": "80px", "height": "470px", "right": "1px"});
                } else {
                    $(".banner-2")
                            .css({"position": "absolute", "top": "auto", "width": "80px", "height": "470px", "left": "-13px"});
                    $(".banner-1")
                            .css({"position": "absolute", "top": "auto", "width": "80px", "height": "470px", "right": "-13px"});
                }
            });
        </script>


        <script>
            $(document).ready(function () {

                $('[data-toggle="popover"]').popover();
                $("ul#ticker01").liScroll();
            });
        </script>

        <script>
            (function () {
                var currentScale = 1;

                var cssPrefixesMap = [
                    'scale',
                    '-webkit-transform',
                    '-moz-transform',
                    '-ms-transform',
                    '-o-transform',
                    'transform'];

                function setScale(scale) {
                    var scaleCss = {};

                    cssPrefixesMap.forEach(function (prefix) {
                        scaleCss[prefix] = 'scale(' + scale + ')';
                    });

                    $('#img-A-Zoom').css(scaleCss);
                }

                $('body').on('click', '.lightbox-zoomOut', function () {
                    setScale(currentScale = currentScale - 0.1);
                });

                $('body').on('click', '.lightbox-zoomIn', function () {
                    setScale(currentScale = currentScale + 0.1);
                });
            })();
        </script>
    </body>
</html>
