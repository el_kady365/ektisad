<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Video'), ['action' => 'edit', $video->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Video'), ['action' => 'delete', $video->id], ['confirm' => __('Are you sure you want to delete # {0}?', $video->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Videos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Video'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="videos view large-9 medium-8 columns content">
    <h3><?= h($video->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ar Title') ?></th>
            <td><?= h($video->ar_title) ?></td>
        </tr>
        <tr>
            <th><?= __('En Title') ?></th>
            <td><?= h($video->en_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><?= h($video->url) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($video->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Display Order') ?></th>
            <td><?= $this->Number->format($video->display_order) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($video->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Update') ?></th>
            <td><?= h($video->update) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $video->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related News') ?></h4>
        <?php if (!empty($video->news)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('Ar Subtitle') ?></th>
                <th><?= __('Ar Permalink') ?></th>
                <th><?= __('Ar Description') ?></th>
                <th><?= __('Ar Keywords') ?></th>
                <th><?= __('Ar Meta Description') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('En Subtitle') ?></th>
                <th><?= __('En Permalink') ?></th>
                <th><?= __('En Description') ?></th>
                <th><?= __('En Keywords') ?></th>
                <th><?= __('En Meta Description') ?></th>
                <th><?= __('Ar Tags') ?></th>
                <th><?= __('En Tags') ?></th>
                <th><?= __('Active') ?></th>
                <th><?= __('Category Id') ?></th>
                <th><?= __('Editor Id') ?></th>
                <th><?= __('Reviewed') ?></th>
                <th><?= __('Reviewer Id') ?></th>
                <th><?= __('Publisher Id') ?></th>
                <th><?= __('Published') ?></th>
                <th><?= __('Publish Date') ?></th>
                <th><?= __('Ad Image1') ?></th>
                <th><?= __('Ad Image2') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($video->news as $news): ?>
            <tr>
                <td><?= h($news->id) ?></td>
                <td><?= h($news->ar_title) ?></td>
                <td><?= h($news->ar_subtitle) ?></td>
                <td><?= h($news->ar_permalink) ?></td>
                <td><?= h($news->ar_description) ?></td>
                <td><?= h($news->ar_keywords) ?></td>
                <td><?= h($news->ar_meta_description) ?></td>
                <td><?= h($news->en_title) ?></td>
                <td><?= h($news->en_subtitle) ?></td>
                <td><?= h($news->en_permalink) ?></td>
                <td><?= h($news->en_description) ?></td>
                <td><?= h($news->en_keywords) ?></td>
                <td><?= h($news->en_meta_description) ?></td>
                <td><?= h($news->ar_tags) ?></td>
                <td><?= h($news->en_tags) ?></td>
                <td><?= h($news->active) ?></td>
                <td><?= h($news->category_id) ?></td>
                <td><?= h($news->editor_id) ?></td>
                <td><?= h($news->reviewed) ?></td>
                <td><?= h($news->reviewer_id) ?></td>
                <td><?= h($news->publisher_id) ?></td>
                <td><?= h($news->published) ?></td>
                <td><?= h($news->publish_date) ?></td>
                <td><?= h($news->ad_image1) ?></td>
                <td><?= h($news->ad_image2) ?></td>
                <td><?= h($news->created) ?></td>
                <td><?= h($news->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'News', 'action' => 'view', $news->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'News', 'action' => 'edit', $news->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'News', 'action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
