
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$video->id) {
                    echo __('Add Video');
                    } else {
                    echo __('Edit Video');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($video) ?>

                <?php
                                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                                            echo $this->Form->input('en_title', ['class' => 'form-control']);
                                            echo $this->Form->input('url', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('display_order', ['class' => 'form-control']);
                                            echo $this->Form->input('update', ['empty' => true]);
                                            echo $this->Form->input('news._ids', ['options' => $news]);
                                        ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
