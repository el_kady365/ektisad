<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Category'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Category'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Category'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Categories'), ['controller' => 'Categories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Category'), ['controller' => 'Categories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List News'), ['controller' => 'News', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New News'), ['controller' => 'News', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ar Title') ?></th>
            <td><?= h($category->ar_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Ar Permalink') ?></th>
            <td><?= h($category->ar_permalink) ?></td>
        </tr>
        <tr>
            <th><?= __('En Title') ?></th>
            <td><?= h($category->en_title) ?></td>
        </tr>
        <tr>
            <th><?= __('En Permalink') ?></th>
            <td><?= h($category->en_permalink) ?></td>
        </tr>
        <tr>
            <th><?= __('Parent Category') ?></th>
            <td><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->id, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Lft') ?></th>
            <td><?= $this->Number->format($category->lft) ?></td>
        </tr>
        <tr>
            <th><?= __('Rght') ?></th>
            <td><?= $this->Number->format($category->rght) ?></td>
        </tr>
        <tr>
            <th><?= __('Display Order') ?></th>
            <td><?= $this->Number->format($category->display_order) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Updated') ?></th>
            <td><?= h($category->updated) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $category->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Categories') ?></h4>
        <?php if (!empty($category->child_categories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('Ar Permalink') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('En Permalink') ?></th>
                <th><?= __('Parent Id') ?></th>
                <th><?= __('Lft') ?></th>
                <th><?= __('Rght') ?></th>
                <th><?= __('Display Order') ?></th>
                <th><?= __('Active') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Updated') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->child_categories as $childCategories): ?>
            <tr>
                <td><?= h($childCategories->id) ?></td>
                <td><?= h($childCategories->ar_title) ?></td>
                <td><?= h($childCategories->ar_permalink) ?></td>
                <td><?= h($childCategories->en_title) ?></td>
                <td><?= h($childCategories->en_permalink) ?></td>
                <td><?= h($childCategories->parent_id) ?></td>
                <td><?= h($childCategories->lft) ?></td>
                <td><?= h($childCategories->rght) ?></td>
                <td><?= h($childCategories->display_order) ?></td>
                <td><?= h($childCategories->active) ?></td>
                <td><?= h($childCategories->created) ?></td>
                <td><?= h($childCategories->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $childCategories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $childCategories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $childCategories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCategories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related News') ?></h4>
        <?php if (!empty($category->news)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('Ar Permalink') ?></th>
                <th><?= __('Ar Description') ?></th>
                <th><?= __('Ar Keywords') ?></th>
                <th><?= __('Ar Meta Description') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('En Permalink') ?></th>
                <th><?= __('En Description') ?></th>
                <th><?= __('En Keywords') ?></th>
                <th><?= __('En Meta Description') ?></th>
                <th><?= __('Tags') ?></th>
                <th><?= __('Active') ?></th>
                <th><?= __('Category Id') ?></th>
                <th><?= __('Related News') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Reviewer Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($category->news as $news): ?>
            <tr>
                <td><?= h($news->id) ?></td>
                <td><?= h($news->ar_title) ?></td>
                <td><?= h($news->ar_permalink) ?></td>
                <td><?= h($news->ar_description) ?></td>
                <td><?= h($news->ar_keywords) ?></td>
                <td><?= h($news->ar_meta_description) ?></td>
                <td><?= h($news->en_title) ?></td>
                <td><?= h($news->en_permalink) ?></td>
                <td><?= h($news->en_description) ?></td>
                <td><?= h($news->en_keywords) ?></td>
                <td><?= h($news->en_meta_description) ?></td>
                <td><?= h($news->tags) ?></td>
                <td><?= h($news->active) ?></td>
                <td><?= h($news->category_id) ?></td>
                <td><?= h($news->related_news) ?></td>
                <td><?= h($news->user_id) ?></td>
                <td><?= h($news->reviewer_id) ?></td>
                <td><?= h($news->created) ?></td>
                <td><?= h($news->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'News', 'action' => 'view', $news->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'News', 'action' => 'edit', $news->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'News', 'action' => 'delete', $news->id], ['confirm' => __('Are you sure you want to delete # {0}?', $news->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
