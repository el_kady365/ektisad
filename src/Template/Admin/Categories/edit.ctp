
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$category->id) {
                        echo __('Add Category');
                    } else {
                        echo __('Edit Category');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($category) ?>

                <?php
                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                echo $this->Form->input('ar_permalink', ['class' => 'form-control']);
                echo $this->Form->input('en_title', ['class' => 'form-control']);
                echo $this->Form->input('en_permalink', ['class' => 'form-control']);
                echo $this->Form->input('parent_id', ['options' => $parentCategories, 'empty' => true]);
                echo $this->Form->input('display_order', ['class' => 'form-control']);
                echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
