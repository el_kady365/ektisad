<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Categories') ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('ar_title') ?></th>
                                    <!--<th><?= $this->Paginator->sort('ar_permalink') ?></th>-->
                                    <!--<th><?= $this->Paginator->sort('en_title') ?></th>-->
                                    <!--<th><?= $this->Paginator->sort('en_permalink') ?></th>-->
                                    <th><?= $this->Paginator->sort('parent_id') ?></th>
                                    <th><?= $this->Paginator->sort('display_order') ?></th>
                                    <th><?= $this->Paginator->sort('active') ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>
                                    <th><?= $this->Paginator->sort('updated') ?></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($categories as $category): ?>
                                    <tr>
                                        <td><input type="checkbox" name="chk[]" value="<?php echo $category->id ?>" /></td>
                                        <td><?php
                                            echo $category->has('parent_category') ? "<sup>|_ </sup>" : " ";
                                            echo h($category->ar_title)
                                            ?></td>
                                        <td><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->ar_title, ['controller' => 'Categories', 'action' => 'edit', $category->parent_category->id]) : '' ?></td>
                                        <td><?= $this->Number->format($category->display_order) ?></td>
                                        <td><?= ($category->active) ? __('Yes') : __('No') ?></td>
                                        <td><?= h($category->created) ?></td>
                                        <td><?= h($category->updated) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $category->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $category->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $category->ar_title)]) ?>


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
