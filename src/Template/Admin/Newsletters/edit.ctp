
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$newsletter->id) {
                    echo __('Add Newsletter');
                    } else {
                    echo __('Edit Newsletter');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($newsletter) ?>

                <?php
                                echo $this->Form->input('name', ['class' => 'form-control']);
                                            echo $this->Form->input('telephone', ['class' => 'form-control']);
                                            echo $this->Form->input('address', ['class' => 'form-control']);
                                            echo $this->Form->input('email', ['class' => 'form-control']);
                                            echo $this->Form->input('type', ['class' => 'form-control']);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
