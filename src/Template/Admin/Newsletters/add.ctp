
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$newsletter->id) {
                        echo __('Add Newsletter');
                    } else {
                        echo __('Edit Newsletter');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($newsletter) ?>

                <?php
                if ($newsletter->type == 2) {
                    echo $this->Form->input('name', ['class' => 'form-control']);
                    echo $this->Form->input('telephone', ['class' => 'form-control']);
                    echo $this->Form->input('address', ['class' => 'form-control']);
                }
                echo $this->Form->input('email', ['class' => 'form-control']);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script(array('jquery.validate'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("form").validate({
        errorClass: "error-message",
        'ignore': [],
        errorElement: "div",
        rules: {
            telephone: {
                number: true,
            },
            name: {
                required: true,
            }
        },
        messages: {
            name: {
                required: '<?php echo __('Required', true) ?>',
            },
            telephone: {
                required: '<?php echo __('Required', true) ?>',
                number: '<?php echo __('This field acceps numbers only', true) ?>'
            }
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea
            label.insertAfter(element);
        }
    });
</script>
<?php echo $this->end(); ?>