<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php
                    if ($type == 2) {
                        echo __('List Printed Version Subscription');
                    } else {
                         echo __('List Newsletters');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php if ($type == 2) {
                                    ?>
                                    <th><?= $this->Paginator->sort('name') ?></th>
                                    <th><?= $this->Paginator->sort('telephone') ?></th>
                                <?php } ?>
                                <th><?= $this->Paginator->sort('email') ?></th>
                                <!--<th><?= $this->Paginator->sort('type') ?></th>-->
                                <th><?= $this->Paginator->sort('created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($newsletters as $newsletter):
                                if ($type == 2) {
                                    ?>
                                    <tr>
                                        <td><?= h($newsletter->name) ?></td>
                                        <td><?= h($newsletter->telephone) ?></td>
                                    <?php } ?>
                                    <td><?= h($newsletter->email) ?></td>
                                    <!--<td><?= $this->Number->format($newsletter->type) ?></td>-->
                                    <td><?= h($newsletter->created) ?></td>

                                    <td class="actions">
                                        <?php //echo $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $newsletter->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $newsletter->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $newsletter->email)]) ?>


                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="text-right ls-button-group demo-btn ls-table-pagination">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
