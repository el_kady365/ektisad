
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$faq->id) {
                    echo __('Add Faq');
                    } else {
                    echo __('Edit Faq');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($faq) ?>

                <?php
                                echo $this->Form->input('question', ['class' => 'form-control']);
                                            echo $this->Form->input('answer', ['class' => 'form-control']);
                                            echo $this->Form->input('display_order', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
