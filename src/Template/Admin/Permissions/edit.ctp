
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$permission->id) {
                    echo __('Add Permission');
                    } else {
                    echo __('Edit Permission');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($permission) ?>

                <?php
                                echo $this->Form->input('title', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('groups._ids', ['options' => $groups]);
                                        ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
