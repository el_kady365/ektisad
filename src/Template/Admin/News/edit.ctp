
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$news->id) {
                    echo __('Add News');
                    } else {
                    echo __('Edit News');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($news) ?>

                <?php
                                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_permalink', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_description', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_keywords', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_meta_description', ['class' => 'form-control']);
                                            echo $this->Form->input('en_title', ['class' => 'form-control']);
                                            echo $this->Form->input('en_permalink', ['class' => 'form-control']);
                                            echo $this->Form->input('en_description', ['class' => 'form-control']);
                                            echo $this->Form->input('en_keywords', ['class' => 'form-control']);
                                            echo $this->Form->input('en_meta_description', ['class' => 'form-control']);
                                            echo $this->Form->input('tags', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('category_id', ['options' => $categories, 'empty' => true]);
                                            echo $this->Form->input('related_news', ['class' => 'form-control']);
                                            echo $this->Form->input('editor_id', ['options' => $editors, 'empty' => true]);
                                            echo $this->Form->input('reviewed',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('reviewer_id', ['options' => $reviewers, 'empty' => true]);
                                            echo $this->Form->input('publisher_id', ['options' => $publishers, 'empty' => true]);
                                            echo $this->Form->input('published',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('publish_date', ['empty' => true]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
