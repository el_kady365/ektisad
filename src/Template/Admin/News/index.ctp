<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('News') ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $this->Url->build(['action' => 'index']) ?>" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> <?php echo __('Category') ?></label>
                                <?php echo $this->Form->select('category_id', $categories, array('value' => isset($_GET['category_id']) ? $_GET['category_id'] : '', 'name' => 'category_id', 'class' => 'form-control', 'empty' => __('Select Category', true))) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> <?php echo __('Edited by') ?></label>
                                <?php echo $this->Form->select('editor_id', $editors, array('value' => isset($_GET['editor_id']) ? $_GET['editor_id'] : '', 'name' => 'editor_id', 'class' => 'form-control', 'empty' => __('Select Editor', true))) ?>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label> <?php echo __('Published') ?></label>
                                <?php echo $this->Form->select('published', [0 => __('Unpublished'), 1 => __('Published')], array('value' => isset($_GET['published']) ? $_GET['published'] : '', 'name' => 'published', 'class' => 'form-control', 'empty' => __('Select Status', true))) ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6"><button type="submit" class="btn btn-primary"><?php echo __('Search') ?></button></div>
                    </div>
                </form>
                <br />
                <br />
                <!--Table Wrapper Start-->
                <?php if (in_array($user['group_id'], [1, 2, 3])) { ?>
                    <div class="alert alert-danger label-danger" ><?php echo __('There are ') ?> <span class="label label-primary numbers">0</span> <?php echo __('new topics in data from the last loading') ?> <a href="#" class="btn btn-danger reload"><?php echo __('show the new') ?></a></div> 
                <?php } ?>
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('ar_title') ?></th>
                                    <th><?= $this->Paginator->sort('category_id') ?></th>
                                    <?php if (in_array($user['group_id'], [1, 2, 3])) {
                                        ?>
                                        <th><?= $this->Paginator->sort('editor_id', __('Edited by')) ?></th>
                                        <?php
                                    }
                                    ?>
                                    <?php if (in_array($user['group_id'], [1, 2, 3])) { ?>
                                        <th><?= $this->Paginator->sort('assigned_to', __('Assigned To')) ?></th>
                                        <th><?= $this->Paginator->sort('send_to_up_date', __('Send date')) ?></th>
                                    <?php } ?>
                                    <th><?= $this->Paginator->sort('publish_date') ?></th>
                                    <?php if (in_array($user['group_id'], [1])) {
                                        ?>
                                        <th><?= $this->Paginator->sort('views', __('Views')) ?></th>
                                        <?php
                                    }
                                    ?>
                                    <th><?= $this->Paginator->sort('editor_id', __('Created')) ?></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($news as $news): ?>
                                    <tr <?php echo$news->featured ? 'class="success"' : '' ?>>
                                        <td><input  type="checkbox" name="chk[]" value="<?php echo $news->id ?>" /></td>
                                        <td><?= h($news->ar_title) ?></td>
                                        <td><?= $news->has('category') ? $this->Html->link($news->category->ar_title, ['controller' => 'Categories', 'action' => 'edit', $news->category->id]) : '' ?></td>
                                        <?php if (in_array($user['group_id'], [1, 2, 3])) {
                                            ?>
                                            <td><?php echo $news->has('editor') ? $news->editor->username : '' ?></td>
                                            <?php
                                        }
                                        ?>
                                        <?php if (in_array($user['group_id'], [1, 2, 3])) { ?>
                                            <td><?php echo $news->has('group') ? $news->group->name : '' ?></td>
                                            <td><?php echo h($news->send_to_up_date) ?></td>
                                        <?php } ?>

                                        <td><?php echo h($news->publish_date) ?></td>
                                        <?php if (in_array($user['group_id'], [1])) {
                                            ?>
                                            <td><?php echo h($news->views) ?></td>
                                            <?php
                                        }
                                        ?>
                                        <td><?php echo h($news->created) ?></td>
                                        <td class="actions">
                                            <?php
                                            $edit = false;
                                            $publish = false;
                                            $review = false;
                                            $can_delete = false;
                                            $unpublish = false;
                                            if (
                                            //reviewers, desks
                                            //editors
                                                    (hasCapablity('news', 'send-news-to-reviewers', $user) && !in_array($news->assigned_to, [3, 2]) ) ||
                                                    //Admin
                                                    (hasCapablity('news', 'edit', $user) && in_array($user['group_id'], [1, 2, 3]))
                                            ) {
                                                $edit = true;
                                            }



                                            if (hasCapablity('news', 'publish', $user) && !$news->published) {
                                                $publish = true;
                                            }
                                            if (hasCapablity('news', 'unpublish', $user) && $news->published) {
                                                $unpublish = true;
                                            }

                                            if (hasCapablity('news', 'delete', $user)) {
                                                $can_delete = true;
                                            }

                                            echo $this->Html->link('<i class="fa fa-search"></i> ' . __('View'), ['action' => 'view', $news->id], ['class' => 'btn btn-default btn-xs', 'escape' => false]) . ' ';
                                            if (hasCapablity('news', 'return-news-to-editors', $user) &&
                                            $news->assigned_to != '' &&  ($news->assigned_to != '4') && in_array($user['group_id'], [1, 2, 3])
                                            ) {
                                                echo ' <a href="' . $this->Url->build(['action' => 'return-to-editors', $news->id]) . '" href="' . $this->Url->build(['action' => 'return-to-editors', $news->id]) . '" class="btn btn-xs btn-danger return-to-editors"> <i class="fa fa-reply"></i> ' . __('return news to editors') . '</a> ';
                                            }

                                            if (hasCapablity('news', 'return-news-to-reviewers', $user) && $news->assigned_to == 2 && in_array($user['group_id'], [1, 2])) {
                                                echo ' <a href="' . $this->Url->build(['action' => 'send-to-reviewers', $news->id]) . '" class="btn btn-xs   btn-danger return-to-reviewers"> <i class="fa fa-reply "></i> ' . __('return news to reviewers') . '</a> ';
                                            }

                                            if ($edit) {
                                                echo $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $news->id], ['class' => 'btn btn-primary btn-xs', 'escape' => false]) . ' ';
                                            }



                                            if (
                                                    hasCapablity('news', 'send-news-to-reviewers', $user) &&
                                                    ($news->assigned_to == '4') && in_array($user['group_id'], [1, 2, 4])
                                            ) {
                                                echo ' <a href="' . $this->Url->build(['action' => 'send-to-reviewers', $news->id]) . '" class="btn btn-xs btn-success"> <i class="fa fa-check"></i> ' . __('send news to reviewers') . '</a> ';
                                            }

                                            if (hasCapablity('news', 'send-news-to-desks', $user) && $news->assigned_to == '3' && in_array($user['group_id'], [1, 2, 3])) {
                                                echo '<a href="' . $this->Url->build(['action' => 'send-to-desks', $news->id]) . '"  class="btn btn-xs btn-success send-to-desks"> <i class="fa fa-check"></i> ' . __('send news to desks') . '</a> ';
                                            }
                                            if ($publish) {
                                                echo $this->Html->link('<i class="fa fa-check"></i> ' . __('Publish'), ['action' => 'publish', $news->id], ['class' => 'btn btn-success btn-xs', 'escape' => false]) . ' ';
                                            }
                                            if ($unpublish) {
                                                echo $this->Html->link('<i class="fa fa-close"></i> ' . __('Unpublish'), ['action' => 'publish', $news->id], ['class' => 'btn btn-warning btn-xs', 'escape' => false]) . ' ';
                                            }
                                            if ($can_delete) {
                                                echo $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $news->id], ['class' => 'btn btn-danger btn-xs', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $news->ar_title)]) . ' ';
                                            }
                                            ?>


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $this->append('script'); ?>
<script>
    $(function () {
        $('.reload').on('click', function () {
            location.reload();
            return false;
        })
        setInterval(function () {
            $.ajax({
                type: "GET",
                url: '<?php echo $this->Url->build(['action' => 'get-unLoaded-news']) ?>',
                data: {date: '<?php echo $loadTime ?>'},
                dataType: 'json',
            }).done(function (data) {
                $('.numbers').text(data.news);
            });
        }, 5000);
    })
</script>
<?php echo $this->end(); ?>
