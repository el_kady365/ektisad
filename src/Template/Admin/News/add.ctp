<style type="text/css">
    .url-div{
        border: 1px dotted #999999;
        padding-right: 10px;
        margin:8px;
        position: relative;
    }
    #Options{
        border: 1px dotted #999999;

        margin:4px 8px;
    }
    .delete-section2{
        position: absolute;
        top:0;
        left:0;
        width:16px;
    }
    .inline-labeles label
    {
        display: inline-block;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$news->id) {
                        echo __('Add News');
                    } else {
                        echo __('Edit News');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?php
                echo $this->Form->create($news, ['type' => 'file']);
                echo $this->Form->input('id');
                ?>

                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $xlang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a class="tab-link" data-value="<?php echo $key . '-description' ?>" href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $xlang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php
                        echo $this->Form->input('category_id', ['class' => 'form-control', 'options' => $categories, 'empty' => __('Select Category')]);
                        echo $this->Form->input('writer_id', ['class' => 'form-control', 'options' => $writers, 'empty' => __('Select Writer'), 'label' => __('Written by')]);
                        foreach ($languages as $key => $xlang) {
                            ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <?php
                                echo $this->Form->input($key . '_title', ['class' => 'form-control news-title', 'label' => __('Title', true), 'templateVars' => ['after' => $this->Html->tag('span', __('Maximum limit: {0}', $this->Html->tag('span', 60, ['class' => 'max-limit'])), ['class' => 'help_text'])]]);
                                echo $this->Form->input($key . '_subtitle', ['class' => 'form-control', 'label' => __('Subtitle', true)]);
                                echo $this->Form->input($key . '_description', ['class' => 'form-control ckeditor', 'label' => __('Description', true)]);
                                echo $this->Form->button('<i class="fa fa-facebook"></i> Insert facebook Post', array('type' => 'button', 'data-value' => Cake\Utility\Text::slug($key . '_description'), 'class' => 'facebookPosts btn btn-primary', 'escape' => false));
                                echo $this->Form->input($key . '_brief', ['class' => 'form-control', 'label' => __('Brief', true)]);
                                echo $this->Form->input($key . '_tags', ['class' => 'form-control tags', 'label' => __('Tags', true)]);
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="form-group">
                    <h2><?php echo __('Videos') ?></h2>
                    <a href="#" data-toggle="modal" disabled="disabled" data-target="#myModalSmall" id="videos-modal-link" class="btn btn-success btn-lg"><?php echo __('Related Videos') ?></a>
                </div>

                <div id="options-div">
                    <h2><?php echo __('Images') ?></h2>
                    <div id="Options" class="row">
                        <?php
                        $section_count = 0;
//                debug($news);
                        if (!empty($news->news_images)) {
                            foreach ($news->news_images as $j => $option) {
                                //debug($level);
                                ?>

                                <div id='row<?php echo $j ?>' class="col-md-11 url-div">
                                    <?php
                                    echo $this->Form->input('news_images.' . $j . '.id', array('value' => $option->id));
                                    echo $this->Form->input('news_images.' . $j . '.ar_title', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'col-md-6'], 'label' => __('Ar Title')]);
                                    echo $this->Form->input('news_images.' . $j . '.en_title', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'col-md-6'], 'label' => __('En Title')]);
                                    echo $this->Form->input('news_images.' . $j . '.type', array('class' => 'form-control asset-type', 'options' => ['0' => __('image'), 1 => __('video')], 'label' => __('Type')));
                                    echo $this->Form->input('news_images.' . $j . '.image', array('type' => 'file', 'templateVars' => ['div_classes' => 'news-image hide', 'between' => $this->element('image_between', ['file' => $option->image_info, 'settings' => $imageSettings['image']])]));
                                    echo $this->Form->input('news_images.' . $j . '.video', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'news-video hide'], 'label' => __('Youtube url')]);
//                                    debug($option->image_info);
                                    ?>
                                    &nbsp;
                                    &nbsp;
                                    <a href = '#' class = "delete-section2 btn btn-small">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </div>
                                <?php
                                $section_count++;
                            }
                        } else {
                            ?>
                            <div id='row0' class="url-div col-md-11">
                                <?php
                                echo $this->Form->input('news_images.0.ar_title', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'col-md-6'], 'label' => __('Ar Title')]);
                                echo $this->Form->input('news_images.0.en_title', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'col-md-6'], 'label' => __('En Title')]);
                                echo $this->Form->input('news_images.0.type', array('class' => 'form-control asset-type', 'options' => ['0' => __('image'), 1 => __('video')], 'label' => __('Type')));
                                echo $this->Form->input('news_images.0.image', array('type' => 'file', 'templateVars' => ['div_classes' => 'news-image hide', 'between' => $this->element('image_between', ['settings' => $imageSettings['image']])]));
                                echo $this->Form->input('news_images.0.video', ['class' => 'form-control', 'templateVars' => ['div_classes' => 'news-video hide'], 'label' => __('Youtube url')]);
                                ?>
                                <a href = '#' class = "delete-section2 btn btn-small">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        <?php } ?>

                    </div>
                    <a href="#" class="add-author btn">
                        <i class="fa fa-plus-circle"></i> <?php echo __('Add Image') ?>
                    </a>
                </div>
                <?php
                if ($news->publish_date != '') {
                    $publish_date = new Cake\I18n\Time($news->publish_date);
                    $news->publish_date = $publish_date->format('Y-m-d H:i');
                }

                echo $this->Form->input('featured', ['label' => ['class' => 'checkbox-inline']]);
                echo $this->Form->input('show_on_home', ['label' => ['class' => 'checkbox-inline']]);
                if (hasCapablity('news', 'linguistic-review', $user)) {
                    echo $this->Form->input('lingusitic_reviewed', ['label' => ['class' => 'checkbox-inline']]);
                }
                if (hasCapablity('news', 'publish', $user)) {
//                    echo $this->Form->input('published', ['label' => ['class' => 'checkbox-inline']]);
                    echo $this->Form->input('publish_date', ['type' => 'text', 'empty' => true, 'class' => 'form-control dateTimePicker', '']);
                }
                $notes_count = 0;
                if ((!hasCapablity('news', 'add-notes', $user) && !empty($news->news_notes)) || hasCapablity('news', 'add-notes', $user)) {
                    ?>

                    <div class="panel panel-light-blue">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo __('Notes') ?></h3>
                            <ul class="panel-control">
                                <li><a class="minus" href="javascript:void(0)"><i class="fa fa-minus"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="nano">
                                <div class="nano-content">
                                    <div class="goal-box">
                                        <ul class="goal-item">
                                            <?php
                                            if (!empty($news->news_notes)) {
                                                foreach ($news->news_notes as $j => $note) {
                                                    //debug($level);
                                                    ?>
                                                    <li>
                                                        <div id='row-note<?php echo $j ?>' class="row">
                                                            <?php
                                                            echo $this->Form->input('news_notes.' . $j . '.id', array('value' => $note->id));
                                                            echo $this->Form->input('news_notes.' . $j . '.user_id', array('value' => $note->user_id, 'type' => 'hidden'));
                                                            echo $this->Form->input('news_notes.' . $j . '.news_id', array('value' => $note->news_id, 'type' => 'hidden'));
                                                            echo $this->Form->input('news_notes.' . $j . '.note', array('value' => $note->note, 'type' => 'hidden'));
                                                            ?>

                                                            <div class="goal-content col-md-11">
                                                                <strong><?php echo $note->user->username ?></strong>
                                                                <p>
                                                                    <?php echo nl2br($note->note) ?>
                                                                </p>

                                                            </div>
                                                            <?php if ($user['group_id'] == 1) { ?>
                                                                <div class="col-md-1">
                                                                    <a href="#" class="delete-note btn ls-red-btn"><i class="fa fa-trash-o"></i></a>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $notes_count++;
                                                }
                                            }
                                            ?>

                                        </ul>

                                    </div>

                                </div>

                            </div>
                            <?php if (hasCapablity('news', 'add-notes', $user)) { ?>
                                <a href="#" data-toggle="modal" data-target="#myNoteModal" class="btn btn-info btn-danger"><?php echo __('Add Note') ?></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="modal fade" id="myNoteModal" tabindex="-1" role="dialog" aria-labelledby="myNoteModal" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header label-danger white">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModaDanger"><?php echo __('Add Note'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <?php
//                                echo $this->Form->create($newsNotes);
                                echo $this->Form->input('note', ['type' => 'textarea', 'class' => 'form-control', 'label' => __('Write Note', true)]);
                                echo $this->Form->button(__('Add Note'), ['type' => 'button', 'id' => 'newsNoteBtn']);
//                                echo $this->Form->end();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="myModalSmall" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header label-success white">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModaDanger"><?php echo __('Related Videos'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <div id="tabs">
                                    <ul class="nav nav-tabs" role="tablist">

                                        <li role="presentation" class="active">
                                            <a href="#tab-results" aria-controls="home" role="tab" data-toggle="tab"><?php echo __('Search Results') ?></a>

                                        </li>
                                        <li role="presentation">
                                            <a href="#tab-related" aria-controls="home" role="tab" data-toggle="tab"><?php echo __('Related Videos') ?></a>
                                        </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="tab-results">
                                            <div class="row">

                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="tab-related">
                                            <div class="row" id="NewsVideos">
                                                <?php
                                                $video_count = 0;
                                                if (!empty($news->news_videos)) {
                                                    foreach ($news->news_videos as $i => $video) {
                                                        //debug($level);
                                                        ?>

                                                        <div id='row-video<?php echo $i ?>' class="col-md-4 text-center">
                                                            <?php
                                                            echo $this->Form->input('news_videos.' . $i . '.id', array('value' => $video->id));
                                                            echo $this->Form->input('news_videos.' . $i . '.youtube_id', array('value' => $video->youtube_id, 'type' => 'hidden'));
                                                            ?>
                                                            <a href="http://youtube.com/watch?v=<?php echo $video->youtube_id ?>" target="_blank">
                                                                <img src="<?php echo "https://i.ytimg.com/vi/{$video->youtube_id}/default.jpg" ?>">
                                                            </a>
                                                            <br />
                                                            <a href = '#' class = "delete-video btn btn-danger btn-small">
                                                                <i class="fa fa-trash-o"></i> <?php echo __('Remove') ?> 
                                                            </a>
                                                        </div>
                                                        <?php
                                                        $video_count++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success btn-xs" data-dismiss="modal"><?php echo __('Close') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if (hasCapablity('news', 'return-news-to-editors', $user) &&
                        !empty($news->assigned_to) && ($news->assigned_to != '4') && in_array($user['group_id'], [1, 2, 3])
                ) {
                    echo ' <button type="submit" class="btn btn-lg btn-danger return-to-editors"> <i class="fa fa-reply"></i> ' . __('return news to editors') . '</button> ';
                }

                if (hasCapablity('news', 'return-news-to-reviewers', $user) && $news->assigned_to == '2' && in_array($user['group_id'], [1, 2])) {
                    echo ' <button type="submit" class="btn btn-lg btn-danger return-to-reviewers"> <i class="fa fa-reply "></i> ' . __('return news to reviewers') . '</button> ';
                }

                if (!empty($news->assigned_to)) {
                    $submit_label = __('Save in {0}', $news->group['name']);
                } else {
                    $submit_label = __('Submit');
                }
                echo $this->Form->button($submit_label);
                if (
                        hasCapablity('news', 'send-news-to-reviewers', $user) &&
                        ($news->assigned_to == '4') && in_array($user['group_id'], [1, 2, 4])
                ) {
                    echo ' <button type="submit" class="btn btn-lg btn-success send-to-reviewers"> <i class="fa fa-check"></i> ' . __('send news to reviewers') . '</button> ';
                }
                if (hasCapablity('news', 'send-news-to-desks', $user) && $news->assigned_to == '3' && in_array($user['group_id'], [1, 2, 3])) {
                    echo ' <button type="submit" class="btn btn-lg btn-success send-to-desks"> <i class="fa fa-check"></i> ' . __('send news to desks') . '</button> ';
                }
                if (!empty($news->id) && hasCapablity('news', 'publish', $user) && !$news->published) {
                    echo ' <button type="submit" class="btn btn-lg btn-success publish"> <i class="fa fa-check"></i> ' . __('Publish') . '</button> ';
                }
                if (hasCapablity('news', 'publish', $user) && $news->published) {
                    echo ' <button type="submit" class="btn btn-lg btn-warning publish"> <i class="fa fa-close"></i> ' . __('Unpublish') . '</button> ';
                }
                ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->css(array('admin/plugins/selectize.bootstrap3', 'admin/plugins/jquery.datetimepicker'), ['block' => true]) ?>
<?php echo $this->Html->script(array('admin/selectize.min', 'admin/jquery.datetimepicker', 'admin/bootbox.min.js', 'admin/jquery.nanoscroller.min', 'auth'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    $(function () {
        $('.send-to-reviewers').on('click', function () {
            $(this).parents('form').prop('action', '<?php echo $this->Url->build(['action' => 'send-to-reviewers', $news->id]) ?>');
        });
        $('.send-to-desks').on('click', function () {
            $(this).parents('form').prop('action', '<?php echo $this->Url->build(['action' => 'send-to-desks', $news->id]) ?>');
        });

        $('.return-to-editors').on('click', function () {
            $(this).parents('form').prop('action', '<?php echo $this->Url->build(['action' => 'return-to-editors', $news->id]) ?>');
            //            console.log($(this).parents('form').prop('action'));
            //            return false;
        });

        $('.return-to-reviewers').on('click', function () {
            $(this).parents('form').prop('action', '<?php echo $this->Url->build(['action' => 'send-to-reviewers', $news->id]) ?>');
        });
        $('.publish').on('click', function () {
            $(this).parents('form').prop('action', '<?php echo $this->Url->build(['action' => 'publish-form', $news->id]) ?>');
        });
        $('.facebookPosts').on('click', function () {
            var node = $('<div/>');
            node.addClass('text-center');
            current_description = $(this).data('value');
            bootbox.prompt("<?php echo __('Please enter post url'); ?>", function (result) {
                if (result !== null) {
                    var data = result;
                    //                    alert(data);
                    node.html('<iframe src="https://www.facebook.com/plugins/post.php?href=' + encodeURI(data) + '&width=500&show_text=true&appId=1620723841472882" width="500" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
                }

            });
            if (node.html != '') {
//                console.log('#' + current_description);
                $('#' + current_description).summernote('editor.insertNode', node.get(0));
            }

        });
        $('.news-title').on('keyup', function () {
            $lenght = $(this).val().length;
            $form_group = $(this).closest('.form-group');
            if ($lenght <= 60) {
                $form_group.removeClass('has-error').addClass('has-success');
                $form_group.find('.max-limit').text(60 - $lenght);
            } else {
                $form_group.removeClass('has-success').addClass('has-error');
                $form_group.find('.max-limit').text(60 - $lenght);
            }
        }).trigger('keyup');
        var notes_counter = '<?php echo $notes_count ?>';
        $('#newsNoteBtn').on('click', function () {
            var $noteElement = $(this).closest('.modal-body').find('#note');
            if ($noteElement.val() == '') {
                $errorElement = $('<div />').addClass('error-message').text('<?php echo __('Required') ?>');
                if (!$(this).closest('.modal-body').find('.error-message').length) {
                    $errorElement.insertAfter($noteElement);
                }
                return false;
            }

            x = '<input type="hidden" name="news_notes[0][note]" id="news-notes-0-note" value="' + $noteElement.val() + '">';
            x += '<input type="hidden" name="news_notes[0][user_id]" id="news-notes-0-user_id" value="<?php echo $user['id'] ?>">';
            x += '<div class="goal-content col-md-11"><strong><?php echo $user['username'] ?></strong>\n\
        <p>' + $noteElement.val() + '</p></div><div class="col-md-1"><a href="#" class="delete-note btn ls-red-btn"><i class="fa fa-trash-o"></i></a></div>';

//            x += '<a href="#" class = "delete-video btn btn-danger btn-small"><i class="fa fa-trash-o"></i></a>';
            y = x.replace(/news_notes-0/g, 'news_notes-' + notes_counter);
            y = y.replace(/\[0\]/g, '[' + notes_counter + ']');
            $('ul.goal-item').append('<li><div id="row-note' + notes_counter + '" class="row">' + y + '</div></li>');
            notes_counter++;
            $noteElement.val('');
            $('#myNoteModal').modal('hide');
            return false;


        });

    });

    var $v = $("form").validate({
        errorClass: "error-message",
        highlight: function (label) {
            $(label).closest('.form-group').addClass('has-errors');
            $fieldset = $(label).closest('.tab-content');
            if ($($fieldset).find(".tab-pane.active:has(div.has-errors)").length == 0) {
                $($fieldset).find(".tab-pane:has(div.has-errors)").each(function (index, tab) {
                    $('a[href=#' + $(label).closest('.tab-pane:not(.active)').attr('id') + ']').tab('show');
                });
            }
        },
        ignore: [],
        errorElement: "div",
        rules: {
            writer_id: {
                required: true
            }
        },
        messages: {
            category_id: {
                required: '<?php echo __('Required', true) ?>',
            },
            writer_id: {
                required: '<?php echo __('Required', true) ?>',
            },
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }, submitHandler: function (form) {
            $('#ar-description').val($('#ar-description').summernote('code'));
            $('#en-description').val($('#en-description').summernote('code'));
            form.submit();
        }

    });
    $(function () {
        var selectized = $('.tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            },
            render: {
                item: function (data, escape) {
                    return '<div>' + escape(data.text) + '</div>';
                }
            },
            onDelete: function (values) {
                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
            },
        });
        var writer = $('#writer-id').selectize({
            onChange: function (value) {
                var control = $('#writer-id').closest('.form-group');
                console.log(control);
                if (control.hasClass('has-errors') && value) {
                    control.removeClass('has-errors');
                    $('#writer-id').closest('.form-group').find('.error-message').remove();
                }
            }});
        $('.dateTimePicker').datetimepicker({
            format: 'Y-m-d H:i',
            lang: '<?php echo $lang ?>'
        });
        $('.add-author').on('click', function () {
            if ($('#Options').is(':visible')) {
                $('#Options').append('<div class="url-div col-md-11">' + $('.url-div:first').html().replace(/\[0\]/g, '[' + $('.url-div').length + ']').replace(/-0-/g, '-' + $('.url-div').length + '-') + '</div>');
                $('#Options .url-div:last input').val('');
                $("#Options .url-div:last .badge").remove();
                $('#Options .url-div:last textarea').val('');
                $('#Options .url-div:last .asset-type').trigger('change');
                $('#Options .url-div:last').find('.image_between').remove();
            } else {
                $('#Options').show();
            }
            return false;
        });
        $('.asset-type').on('change', function () {
            showHideNewsAssets($(this));
        }).trigger('change');
        $(document).on('change', '.asset-type', function () {
            showHideNewsAssets($(this));
        });
        function showHideNewsAssets($object) {
            $type = $object.val();
            $image_div = $object.closest('.url-div').find('.news-image');
            $video_div = $object.closest('.url-div').find('.news-video');
            if ($type == '0') {
                $image_div.removeClass('hide').show().find('input').removeAttr('disabled');
                $video_div.addClass('hide').hide().find('input').attr('disabled', true);
            } else {
                $video_div.removeClass('hide').show().find('input').removeAttr('disabled');
                $image_div.addClass('hide').hide().find('input').attr('disabled', true);
            }
        }

        $('textarea.selectized').on('change', function () {
            searchkeys = $(this).val();
            id = 'videos-container-' + $(this).attr('id');
            if (searchkeys != '') {
                keys = searchkeys.split(",");
                for (i in keys) {
                    $('#tab-results #' + id + i).empty();
                    searchYoutube(keys[i], id + i);
                }
            }
        });
        $('#videos-modal-link').on('click', function () {
            $('#tab-results').empty();
            $('textarea.selectized').each(function () {
                $(this).trigger('change');
            });
        });
    });
    $(document).on('click', '.delete-section2', function () {
        if (confirm('<?php echo __('Are you sure you want to delete this item?') ?>')) {
            if ($('#Options').find('.url-div').length <= 1) {
                $(this).parent('.url-div').find('input').val('');
                $('#Options .url-div:last').find('.image_between').remove();
            } else {
                $(this).parent('.url-div').remove();
            }
        }
        return false;
    });
    $(document).on('click', '.delete-video', function () {
        if (confirm('<?php echo __('Are you sure you want to delete this video?') ?>')) {
            $(this).parent('.col-md-4').remove();
        }
        return false;
    });
    $(document).on('click', '.delete-note', function () {
        if (confirm('<?php echo __('Are you sure you want to delete this note?') ?>')) {
            $(this).closest('li').remove();
        }
        return false;
    });
    function handleAPILoaded() {
        $('#videos-modal-link').removeAttr('disabled');
    }
    function searchYoutube($q, index, $pageToken, $appendTo) {
        var request = gapi.client.youtube.search.list({
            q: $q,
            type: 'video',
            part: 'snippet',
            //            order: 'viewCount',
            pageToken: $pageToken ? $pageToken : ''
        });
        if (request) {
            request.execute(function (response) {

                //            var str = JSON.stringify(response.result);
                //            console.log(response.result);
                var $nextPageToken = response.result.nextPageToken;
                var $videos = response.result.items;
                var $div = '';
                if ($appendTo != null) {
                    $id = $appendTo;
                    $div = $('#' + $appendTo);
                    $videos_div = $('#' + $appendTo + ' .videos');
                } else {
                    $id = index;
                    $div = $('<div />').addClass('row').attr('id', $id);
                    $div.append($('<div />').addClass('col-md-12').append($('<h1>').text($q)));
                    $videos_div = $('<div />').addClass('row videos');
                }
                for (var result in $videos) {
                    var $imgUrl = $videos[result].snippet.thumbnails.default.url;
                    var $title = $videos[result].snippet.title;
                    var $videoId = $videos[result].id.videoId;
                    var $videoUrl = 'http://youtube.com/watch?v=' + $videoId;
                    var html = '<div class="col-md-4 text-center"><a target="_blank" title="' + $title + '" href="' + $videoUrl + '">\n\
    <img src="' + $imgUrl + '" /></a>\n\
<br />\n\<a class="add-video btn btn-primary" data-youtube="' + $videoId + '"> <i class="fa fa-plus"></i> <?php echo __('Add') ?> </a></div>';
                    $videos_div.append(html)
                }
                $div.append($videos_div);
                if ($appendTo == null) {

                    $div.append($('<div />').addClass('loadbutton col-md-12 text-center').append('<a class="btn btn-primary" onclick="searchYoutube(\'' + $q + '\',\'' + index + '\',\'' + $nextPageToken + '\',\'' + $id + '\')"><i class="fa fa-plus"></i> <?php echo __('Load more results') ?> </a>'));
                    $('#tab-results').append($div);
                } else {
                    $('#' + $appendTo + ' .loadbutton').remove();
                    $div.append($('<div />').addClass('loadbutton col-md-12 text-center').append('<a class="btn btn-primary" onclick="searchYoutube(\'' + $q + '\',\'' + index + '\',\'' + $nextPageToken + '\',\'' + $id + '\')"><i class="fa fa-plus"></i> <?php echo __('Load more results') ?> </a>'));
                }

            });
        }
    }
    var sections_counter = '<?php echo $video_count ?>';
    $(document).on('click', '.add-video', function () {
        $videoId = $(this).data('youtube');
        x = '<input type="hidden" value="' + $videoId + '" name="news_videos[0][youtube_id]" />';
        x += '<a href="http://youtube.com/watch?v=' + $videoId + '">\n\
    <img src="https://i.ytimg.com/vi/' + $videoId + '/default.jpg">\n\
</a>\n\
<br />';
        x += '<a href = "#" class = "delete-video btn btn-danger btn-small"><i class="fa fa-trash-o"></i><?php echo __('Remove') ?> </a>';
        y = x.replace(/news_videos0/g, 'news_videos' + sections_counter);
        y = y.replace(/\[0\]/g, '[' + sections_counter + ']');
        $('#NewsVideos').append('<div class="col-md-4 text-center" id=\'row-video' + sections_counter + '\'>' + y + '</div>');
        $('a[href=#tab-related]').tab('show');
        sections_counter++;
        return false;
    });



</script>
<?php echo $this->Html->script(array('https://apis.google.com/js/client.js?onload=googleApiClientReady'), ['block' => true]) ?>
<?php echo $this->end(); ?>
