<style>

    .nanos-content{
        height:300px;
        overflow-y: scroll
    }
</style>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/ar_AR/sdk.js#xfbml=1&version=v2.7&appId=636992799782940";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo __('View News') ?>
                </h3>
            </div>
            <div class="panel-body">
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $xlang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $xlang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php foreach ($languages as $key => $xlang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <table class="table vertical-table">
                                    <tr>
                                        <th width="200"><?= __('Title') ?></th>
                                        <td><?= h($news->{$key . '_title'}) ?></td>
                                    </tr>
                                    <tr>
                                        <th width="200"><?= __('Subtitle') ?></th>
                                        <td><?= h($news->{$key . '_subtitle'}) ?></td>
                                    </tr>

                                    <tr>
                                        <th><?= __('Description') ?></th>
                                        <td>
                                            <div class="nanos">
                                                <div class="nanos-content">

                                                    <?php
                                                    $facebook_post = '<div class="text-center"><div class="fb-post" data-width="500" data-href="$1"></div></div>';
                                                    $description = $news->{$key . '_description'};
                                                    $description = preg_replace('/{%facebookpost\$\$(.+)\$\$%}/', $facebook_post, $description);
                                                    echo $description;
                                                    ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th><?= __('Meta Description') ?></th>
                                        <td><?= $this->Text->autoParagraph($news->{$key . '_meta_description'}) ?></td>
                                    </tr>
                                </table>
                            </div>
                        <?php } ?>

                    </div>



                    <table class="table vertical-table">
                        <tr>
                            <th><?= __('Category') ?></th>
                            <td><?= $news->has('category') ? $this->Html->link($news->category->id, ['controller' => 'Categories', 'action' => 'view', $news->category->id]) : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Written by') ?></th>
                            <td><?= $news->has('writer') ? $news->writer->ar_title : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Editor') ?></th>
                            <td><?= $news->has('editor') ? $news->editor->username : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Reviewer') ?></th>
                            <td><?= $news->has('reviewer') ? $news->reviewer->username : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Publisher') ?></th>
                            <td><?= $news->has('publisher') ? $news->publisher->username : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <td><?= $this->Number->format($news->id) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Publish Date') ?></th>
                            <td><?= h($news->publish_date) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Created') ?></th>
                            <td><?= h($news->created) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Modified') ?></th>
                            <td><?= h($news->modified) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Show On Home') ?></th>
                            <td><?= $news->show_on_home ? __('Yes') : __('No'); ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Reviewed') ?></th>
                            <td><?= $news->reviewed ? __('Yes') : __('No'); ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Published') ?></th>
                            <td><?= $news->published ? __('Yes') : __('No'); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($news->news_images)): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?= __('Related News Images') ?>
                    </h4>
                </div>
            </div>
            <div class="panel-body">
                <table cellpadding="0" cellspacing="0" class="table">
                    <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Image') ?></th>
                        <th><?= __('Created') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                    <?php foreach ($news->news_images as $newsImages): ?>
                        <tr>
                            <td><?= h($newsImages->id) ?></td>
                            <td><img src="<?= resizeOnFly($newsImages->image, 0, 50, 0, $newsImages->image_info['folder_path']) ?>"></td>
                            <td><?= h($newsImages->created) ?></td>
                            <td class="actions">
                                <a href="<?php echo $newsImages->image_info['path'] ?>"><?php echo __('View') ?></a>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'newsImages', 'action' => 'delete', $newsImages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $newsImages->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>

        </div>
    </div>
<?php endif; ?>

<?php echo $this->append('script'); ?>
<script>
</script>
<?php echo $this->end(); ?>