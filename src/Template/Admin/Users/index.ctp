<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Users') ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('username') ?></th>

                                    <th><?= $this->Paginator->sort('group_id', __('Group')) ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>
                                    <!--<th><?= $this->Paginator->sort('updated') ?></th>-->
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($uusers as $uuser):
//                                debug($uuser);
                                    ?>
                                    <tr>
                                        <td><input  type="checkbox" name="chk[]" value="<?php echo $uuser->id ?>" /></td>
                                        <td><?= h($uuser->username) ?></td>

                                        <td><?= $uuser->has('group') ? $uuser->group->name : '' ?></td>
                                        <td><?= h($uuser->created) ?></td>
                                        <!--<td><?= h($uuser->updated) ?></td>-->
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $uuser->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                            <?= $this->Form->postLink('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $uuser->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $uuser->username)]) ?>


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
