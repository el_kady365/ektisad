<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Add User') ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($uuser) ?>
                <?php
                echo $this->Form->input('id');
                ?>
                <?php
                echo $this->Form->input('username', ['class' => 'form-control']);
                echo $this->Form->input('password', ['class' => 'form-control', 'placeholder' => '*******']);
                echo $this->Form->input('passwd', ['class' => 'form-control', 'placeholder' => '*******', 'label' => __('Confirm password')]);
                echo $this->Form->input('group_id', ['class' => 'form-control']);
                ?>
                <div class="Writers hide">
                    <?php echo $this->Form->input('writer_id', array('options' => $writers, 'class' => 'form-control', 'empty' => __('Choose Writer'))); ?>

                </div> 

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->append('script'); ?>
<script>
    $(function () {
        $('#group-id').on('change', function () {
            if ($(this).val() == '5') {
                $('.Writers').removeClass('hide').show();
                $('.Writers select').removeProp('disabled');
            } else {
                $('.Writers').addClass('hide').hide();
                $('.Writers select').prop('disabled', 'disabled');
            }
        }).trigger('change');
    })
</script>
<?php echo $this->end(); ?>

