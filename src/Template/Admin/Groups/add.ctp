
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$group->id) {
                        echo __('Add Group');
                    } else {
                        echo __('Edit Group');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($group) ?>

                <?php
                echo $this->Form->input('name', ['class' => 'form-control']);
                echo $this->Form->input('permissions._ids', ['options' => $permissions]);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
<?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
