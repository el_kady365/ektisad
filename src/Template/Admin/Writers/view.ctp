<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Writer'), ['action' => 'edit', $writer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Writer'), ['action' => 'delete', $writer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $writer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Writers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Writer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Articles'), ['controller' => 'Articles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Article'), ['controller' => 'Articles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="writers view large-9 medium-8 columns content">
    <h3><?= h($writer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ar Title') ?></th>
            <td><?= h($writer->ar_title) ?></td>
        </tr>
        <tr>
            <th><?= __('En Title') ?></th>
            <td><?= h($writer->en_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Image') ?></th>
            <td><?= h($writer->image) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($writer->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($writer->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $writer->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Articles') ?></h4>
        <?php if (!empty($writer->articles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('Ar Permalink') ?></th>
                <th><?= __('Ar Description') ?></th>
                <th><?= __('Ar Keywords') ?></th>
                <th><?= __('Ar Meta Description') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('En Permalink') ?></th>
                <th><?= __('En Description') ?></th>
                <th><?= __('En Keywords') ?></th>
                <th><?= __('En Meta Description') ?></th>
                <th><?= __('Ar Tags') ?></th>
                <th><?= __('En Tags') ?></th>
                <th><?= __('Active') ?></th>
                <th><?= __('Writer Id') ?></th>
                <th><?= __('Category Id') ?></th>
                <th><?= __('Editor Id') ?></th>
                <th><?= __('Reviewed') ?></th>
                <th><?= __('Reviewer Id') ?></th>
                <th><?= __('Publisher Id') ?></th>
                <th><?= __('Published') ?></th>
                <th><?= __('Publish Date') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($writer->articles as $articles): ?>
            <tr>
                <td><?= h($articles->id) ?></td>
                <td><?= h($articles->ar_title) ?></td>
                <td><?= h($articles->ar_permalink) ?></td>
                <td><?= h($articles->ar_description) ?></td>
                <td><?= h($articles->ar_keywords) ?></td>
                <td><?= h($articles->ar_meta_description) ?></td>
                <td><?= h($articles->en_title) ?></td>
                <td><?= h($articles->en_permalink) ?></td>
                <td><?= h($articles->en_description) ?></td>
                <td><?= h($articles->en_keywords) ?></td>
                <td><?= h($articles->en_meta_description) ?></td>
                <td><?= h($articles->ar_tags) ?></td>
                <td><?= h($articles->en_tags) ?></td>
                <td><?= h($articles->active) ?></td>
                <td><?= h($articles->writer_id) ?></td>
                <td><?= h($articles->category_id) ?></td>
                <td><?= h($articles->editor_id) ?></td>
                <td><?= h($articles->reviewed) ?></td>
                <td><?= h($articles->reviewer_id) ?></td>
                <td><?= h($articles->publisher_id) ?></td>
                <td><?= h($articles->published) ?></td>
                <td><?= h($articles->publish_date) ?></td>
                <td><?= h($articles->created) ?></td>
                <td><?= h($articles->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Articles', 'action' => 'view', $articles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Articles', 'action' => 'edit', $articles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Articles', 'action' => 'delete', $articles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $articles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
