<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Polls') ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <!--<th><?= $this->Paginator->sort('id') ?></th>-->
                                <th><?= $this->Paginator->sort('ar_title', __('Title')) ?></th>
                                <th><?= $this->Paginator->sort('active') ?></th>
                                <th><?= $this->Paginator->sort('created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($polls as $poll): ?>
                                <tr>
                                    <!--<td><?= $this->Number->format($poll->id) ?></td>-->
                                    <td><?= h($poll->ar_title) ?></td>
                                    <td><?= h($poll->active) ?></td>
                                    <td><?= h($poll->created) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link('<i class="fa fa-question"></i> ' . __('Questions'), ['controller' => 'poll_questions', 'action' => 'index', $poll->id], ['class' => 'btn ls-light-blue-btn', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $poll->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $poll->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $poll->ar_title)]) ?>


                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="text-right ls-button-group demo-btn ls-table-pagination">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
