
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$poll->id) {
                    echo __('Add Poll');
                    } else {
                    echo __('Edit Poll');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($poll) ?>

                <?php
                                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                                            echo $this->Form->input('en_title', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
