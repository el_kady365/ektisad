<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poll'), ['action' => 'edit', $poll->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poll'), ['action' => 'delete', $poll->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poll->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Polls'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Poll Answers'), ['controller' => 'PollAnswers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll Answer'), ['controller' => 'PollAnswers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Poll Questions'), ['controller' => 'PollQuestions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll Question'), ['controller' => 'PollQuestions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="polls view large-9 medium-8 columns content">
    <h3><?= h($poll->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ar Title') ?></th>
            <td><?= h($poll->ar_title) ?></td>
        </tr>
        <tr>
            <th><?= __('En Title') ?></th>
            <td><?= h($poll->en_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($poll->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($poll->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($poll->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $poll->active ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Poll Answers') ?></h4>
        <?php if (!empty($poll->poll_answers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Poll Id') ?></th>
                <th><?= __('Answer') ?></th>
                <th><?= __('Ip') ?></th>
                <th><?= __('Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poll->poll_answers as $pollAnswers): ?>
            <tr>
                <td><?= h($pollAnswers->id) ?></td>
                <td><?= h($pollAnswers->poll_id) ?></td>
                <td><?= h($pollAnswers->answer) ?></td>
                <td><?= h($pollAnswers->ip) ?></td>
                <td><?= h($pollAnswers->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PollAnswers', 'action' => 'view', $pollAnswers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PollAnswers', 'action' => 'edit', $pollAnswers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PollAnswers', 'action' => 'delete', $pollAnswers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pollAnswers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Poll Questions') ?></h4>
        <?php if (!empty($poll->poll_questions)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('Type') ?></th>
                <th><?= __('Poll Id') ?></th>
                <th><?= __('Required') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Updated') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poll->poll_questions as $pollQuestions): ?>
            <tr>
                <td><?= h($pollQuestions->id) ?></td>
                <td><?= h($pollQuestions->ar_title) ?></td>
                <td><?= h($pollQuestions->en_title) ?></td>
                <td><?= h($pollQuestions->type) ?></td>
                <td><?= h($pollQuestions->poll_id) ?></td>
                <td><?= h($pollQuestions->required) ?></td>
                <td><?= h($pollQuestions->created) ?></td>
                <td><?= h($pollQuestions->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PollQuestions', 'action' => 'view', $pollQuestions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PollQuestions', 'action' => 'edit', $pollQuestions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PollQuestions', 'action' => 'delete', $pollQuestions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pollQuestions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
