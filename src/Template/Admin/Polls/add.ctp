
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$poll->id) {
                        echo __('Add Poll');
                    } else {
                        echo __('Edit Poll');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($poll,['id'=>'PollForm']) ?>
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $lang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $lang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php foreach ($languages as $key => $lang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <?php
                                echo $this->Form->input($key . '_title', ['class' => 'form-control', 'label' => __('Title', true)]);
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php
                echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->append('script'); ?>
<script>
    var $v = $("#PollForm").validate({
        errorClass: "error-message",
        highlight: function (label) {
            $(label).closest('.form-group').addClass('has-errors');
            $fieldset = $(label).closest('.tab-content');
            if ($($fieldset).find(".tab-pane.active:has(div.has-errors)").length == 0) {
                $($fieldset).find(".tab-pane:has(div.has-errors)").each(function (index, tab) {
                    $('a[href=#' + $(label).closest('.tab-pane:not(.active)').attr('id') + ']').tab('show');
                });
            }
        },
        'ignore': [],
        errorElement: "div",
        rules: {
            ar_title: {
                required: true,
            },
            en_title: {
                required: true,
            }
        },
        messages: {
            ar_title: {
                required: '<?php echo __('Required', true) ?>',
            },
            en_title: {
                required: '<?php echo __('Required', true) ?>',
            }
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }
    });
</script>
<?php echo $this->end(); ?>
