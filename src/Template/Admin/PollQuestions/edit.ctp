
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$pollQuestion->id) {
                    echo __('Add Poll Question');
                    } else {
                    echo __('Edit Poll Question');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($pollQuestion) ?>

                <?php
                                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                                            echo $this->Form->input('en_title', ['class' => 'form-control']);
                                            echo $this->Form->input('type', ['class' => 'form-control']);
                                            echo $this->Form->input('poll_id', ['options' => $polls, 'empty' => true]);
                                            echo $this->Form->input('required',['label'=>['class'=>'checkbox-inline']]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
