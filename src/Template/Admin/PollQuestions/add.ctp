<style type="text/css">
    .url-div{
        border: 1px dotted #999999;
        padding-right: 10px;
        margin:8px;
        position: relative;
    }
    #Options{
        border: 1px dotted #999999;

        margin:4px 8px;
    }
    .delete-section2{
        position: absolute;
        top:0;
        left:0;
        width:16px;
    }
    .inline-labeles label
    {
        display: inline-block;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$pollQuestion->id) {
                        echo __('Add Poll Question');
                    } else {
                        echo __('Edit Poll Question');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($pollQuestion, ['id' => 'PollQuestionForm']) ?>
                <?= $this->Form->hidden('id', []); ?>
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $lang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $lang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php foreach ($languages as $key => $lang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <?php
                                echo $this->Form->input($key . '_title', ['class' => 'form-control', 'label' => __('Title', true)]);
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php
                echo $this->Form->hidden('type', ['class' => 'form-control', 'value' => '2']);

//                echo $this->Form->input('required', ['label' => ['class' => 'checkbox-inline']]);
                ?>
                <div id="options-div">
                    <h2><?php echo __('Options')?></h2>
                    <div id="Options" class="row">
                        <?php
                        $section_count = 0;
//                debug($pollQuestion);
                        if (!empty($pollQuestion->poll_question_options)) {
                            foreach ($pollQuestion->poll_question_options as $j => $option) {
                                //debug($level);
                                ?>

                                <div id='row<?php echo $j ?>' class="col-md-11 url-div">
                                    <?php
                                    echo $this->Form->input('poll_question_options.' . $j . '.id', array('value' => $option->id));
                                    echo $this->Form->input('poll_question_options.' . $j . '.ar_title', array('class' => 'form-control', 'label' => __('Arabic title')));
                                    echo $this->Form->input('poll_question_options.' . $j . '.en_title', array('class' => 'form-control', 'label' => __('English title')));
                                    echo $this->Form->input('poll_question_options.' . $j . '.display_order', array('type' => 'text','class' => 'form-control digits'));
                                    ?>
                                    &nbsp;
                                    &nbsp;
                                    <a href = '#' class = "delete-section2 btn btn-small">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </div>
                                <?php
                                $section_count++;
                            }
                        } else {
                            ?>
                            <div id='row0' class="url-div col-md-11">
                                <?php
                                echo $this->Form->input('poll_question_options.' . 0 . '.ar_title', array('class' => 'form-control', 'label' => __('Arabic title')));
                                echo $this->Form->input('poll_question_options.' . 0 . '.en_title', array('class' => 'form-control', 'label' => __('English title')));
                                echo $this->Form->input('poll_question_options.' . 0 . '.display_order', array('type' => 'text','class' => 'form-control digits'));
                                ?>
                                <a href = '#' class = "delete-section2 btn btn-small">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        <?php } ?>

                    </div>
                    <a href="#" class="add-author btn">
                        <i class="fa fa-plus-circle"></i> <?php echo __('Add Option') ?>
                    </a>
                </div>
                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?php echo $this->append('script'); ?>
<script>
    jQuery.extend(jQuery.validator.messages, {
        required: "<?php echo __('Required', true) ?>"
    });
    var $v = $("#PollQuestionForm").validate({
        errorClass: "error-message",
        highlight: function (label) {
            $(label).closest('.form-group').addClass('has-errors');
            $fieldset = $(label).closest('.tab-content');
            if ($($fieldset).find(".tab-pane.active:has(div.has-errors)").length == 0) {
                $($fieldset).find(".tab-pane:has(div.has-errors)").each(function (index, tab) {
                    $('a[href=#' + $(label).closest('.tab-pane:not(.active)').attr('id') + ']').tab('show');
                });
            }
        },
        'ignore': [],
        errorElement: "div",
        rules: {
            ar_title: {
                required: true,
            },
            en_title: {
                required: true,
            }
        },
        
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }
    });
    $(function () {
        $('.add-author').on('click', function () {
            if ($('#Options').is(':visible')) {
                $('#Options').append('<div class="url-div col-md-11">' + $('.url-div:first').html().replace(/\[0\]/g, '[' + $('.url-div').length + ']').replace(/-0-/g, '-' + $('.url-div').length + '-') + '</div>');
                $('#Options .url-div:last input').val('');
                $('#Options .url-div:last textarea').val('');
                $('#Options .url-div:last').find('.image_between').remove();
            } else {
                $('#Options').show();
            }
            return false;

        });

    });
    $(document).on('click', '.delete-section2', function () {
        if (confirm('Are you sure?')) {

            if ($('#Options').find('.url-div').length <= 1) {
                $(this).parent('.url-div').find('input').val('');
            } else {
                $(this).parent('.url-div').remove();
            }
        }
        return false;
    });
</script>
<?php echo $this->end(); ?>
