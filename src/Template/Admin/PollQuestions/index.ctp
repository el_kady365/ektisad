<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Poll Questions') ?></h3>
            </div>
            <div class="panel-body">


                <!--Table Wrapper Start-->
                <div class="table-responsive ls-table">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <!--<th><?= $this->Paginator->sort('id') ?></th>-->
                                <th><?= $this->Paginator->sort('ar_title') ?></th>
                                <th><?= $this->Paginator->sort('active') ?></th>
                                <th><?= $this->Paginator->sort('created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pollQuestions as $pollQuestion): ?>
                                <tr>

                                    <td><?= h($pollQuestion->ar_title) ?></td>



                                    <td><a href="<?php echo $this->Url->build(['action' => 'activate', $pollQuestion->id]) ?>"><?php echo ($pollQuestion->active) ? '<span class="label label-success">'.__('Active').'</span>' : '<span class="label label-danger">'.__('Inactive').'</span>' ?></a></td>
                                    <td><?= h($pollQuestion->created) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $pollQuestion->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                        <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $pollQuestion->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $pollQuestion->ar_title)]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <div class="text-right ls-button-group demo-btn ls-table-pagination">
                        <ul class="pagination ls-pagination">
                            <?php
                            if ($this->Paginator->hasPrev()) {
                                echo $this->Paginator->prev('< ' . __('previous'));
                            }
                            echo $this->Paginator->numbers();
                            if ($this->Paginator->hasNext()) {
                                echo $this->Paginator->next(__('next') . ' >');
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
