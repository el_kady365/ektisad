<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Poll Question'), ['action' => 'edit', $pollQuestion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Poll Question'), ['action' => 'delete', $pollQuestion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pollQuestion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Poll Questions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll Question'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Polls'), ['controller' => 'Polls', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll'), ['controller' => 'Polls', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Poll Question Options'), ['controller' => 'PollQuestionOptions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Poll Question Option'), ['controller' => 'PollQuestionOptions', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pollQuestions view large-9 medium-8 columns content">
    <h3><?= h($pollQuestion->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Ar Title') ?></th>
            <td><?= h($pollQuestion->ar_title) ?></td>
        </tr>
        <tr>
            <th><?= __('En Title') ?></th>
            <td><?= h($pollQuestion->en_title) ?></td>
        </tr>
        <tr>
            <th><?= __('Poll') ?></th>
            <td><?= $pollQuestion->has('poll') ? $this->Html->link($pollQuestion->poll->id, ['controller' => 'Polls', 'action' => 'view', $pollQuestion->poll->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($pollQuestion->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Type') ?></th>
            <td><?= $this->Number->format($pollQuestion->type) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($pollQuestion->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Updated') ?></th>
            <td><?= h($pollQuestion->updated) ?></td>
        </tr>
        <tr>
            <th><?= __('Required') ?></th>
            <td><?= $pollQuestion->required ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Poll Question Options') ?></h4>
        <?php if (!empty($pollQuestion->poll_question_options)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Poll Question Id') ?></th>
                <th><?= __('Ar Title') ?></th>
                <th><?= __('En Title') ?></th>
                <th><?= __('Display Order') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($pollQuestion->poll_question_options as $pollQuestionOptions): ?>
            <tr>
                <td><?= h($pollQuestionOptions->id) ?></td>
                <td><?= h($pollQuestionOptions->poll_question_id) ?></td>
                <td><?= h($pollQuestionOptions->ar_title) ?></td>
                <td><?= h($pollQuestionOptions->en_title) ?></td>
                <td><?= h($pollQuestionOptions->display_order) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PollQuestionOptions', 'action' => 'view', $pollQuestionOptions->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PollQuestionOptions', 'action' => 'edit', $pollQuestionOptions->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PollQuestionOptions', 'action' => 'delete', $pollQuestionOptions->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pollQuestionOptions->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
