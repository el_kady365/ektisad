
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$menu->id) {
                    echo __('Add Menu');
                    } else {
                    echo __('Edit Menu');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($menu) ?>

                <?php
                                echo $this->Form->input('title', ['class' => 'form-control']);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
