
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$image->id) {
                        echo __('Add Image');
                    } else {
                        echo __('Edit Image');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($image, ['type' => 'file', 'id' => 'NewsForm']) ?>
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $lang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $lang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php foreach ($languages as $key => $lang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <?php
                                echo $this->Form->input($key . '_title', ['class' => 'form-control', 'label' => __('Title', true)]);
                                echo $this->Form->input($key . '_description', ['class' => 'form-control ckeditor', 'label' => __('Description', true)]);
                                echo $this->Form->input($key . '_tags', ['class' => 'form-control tags', 'label' => __('Tags', true)]);
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php
                echo $this->Form->input('image', array('type' => 'file', 'templateVars' => ['between' => $this->element('image_between', ['file' => $image->image_info, 'settings' => $imageSettings['image']])]));
                echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->css(array('admin/plugins/selectize.bootstrap3'), ['inline' => false]) ?>
<?php echo $this->Html->script(array('admin/selectize.min'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("form").validate({
        errorClass: "error-message",
        highlight: function (label) {
            $(label).closest('.form-group').addClass('has-errors');
            $fieldset = $(label).closest('.tab-content');
            if ($($fieldset).find(".tab-pane.active:has(div.has-errors)").length == 0) {
                $($fieldset).find(".tab-pane:has(div.has-errors)").each(function (index, tab) {
                    $('a[href=#' + $(label).closest('.tab-pane:not(.active)').attr('id') + ']').tab('show');
                });
            }
        },
        'ignore': [],
        errorElement: "div",
        messages: {
            ar_title: {
                required: '<?php echo __('Required', true) ?>',
            },
            en_title: {
                required: '<?php echo __('Required', true) ?>',
            }
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }
    });
    $(function () {
        $('.tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            },
            render: {
                item: function (data, escape) {
                    return '<div>' + escape(data.text) + '</div>';
                }
            },
            onDelete: function (values) {
                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
            }
        });

    });

</script>
<?php echo $this->end(); ?>
