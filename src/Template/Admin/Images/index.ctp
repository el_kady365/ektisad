<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Images') ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('ar_title', __('Title')) ?></th>
                                    <th><?= $this->Paginator->sort('image') ?></th>
                                    <th><?= $this->Paginator->sort('active') ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($images as $image): ?>
                                    <tr>
                                        <td><input type="checkbox" name="chk[]" value="<?php echo $image->id ?>" /></td>
                                        <td><?= h($image->ar_title) ?></td>
                                        <td><img src="<?= resizeOnFly($image->image, 100, 100, 1, $image->image_info['folder_path']) ?>"></td>
                                        <td><?= h($image->active) ?></td>
                                        <td><?= h($image->created) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $image->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $image->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $image->ar_title)]) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
