
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$image->id) {
                    echo __('Add Image');
                    } else {
                    echo __('Edit Image');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($image) ?>

                <?php
                                echo $this->Form->input('ar_title', ['class' => 'form-control']);
                                            echo $this->Form->input('en_title', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_description', ['class' => 'form-control']);
                                            echo $this->Form->input('en_description', ['class' => 'form-control']);
                                            echo $this->Form->input('image', ['class' => 'form-control']);
                                            echo $this->Form->input('ar_tags', ['class' => 'form-control']);
                                            echo $this->Form->input('en_tags', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            echo $this->Form->input('news._ids', ['options' => $news]);
                                        ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
