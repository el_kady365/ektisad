<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Ads') ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $this->Url->build([]) ?>" method="get">
                    <div class="row">
                        <?php if ($category) { ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> <?php echo __('Category') ?></label>
                                    <?php echo $this->Form->select('category_id', $categories, array('value' => isset($_GET['category_id']) ? $_GET['category_id'] : '', 'name' => 'category_id', 'class' => 'form-control', 'empty' => __('Select Category', true))) ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label> <?php echo __('Location') ?></label>
                                <?php echo $this->Form->select('location', $locations, array('value' => isset($_GET['location']) ? $_GET['location'] : '', 'name' => 'location', 'class' => 'form-control', 'empty' => __('Select Location', true))) ?>
                            </div>
                        </div>
                        <div class="clear"></div>


                        <div class="col-md-6"><button type="submit" class="btn btn-primary"><?php echo __('Search') ?></button></div>
                    </div>
                </form>
                <br />
                <br />
                <!--Table Wrapper Start-->
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('title') ?></th>
                                    <th style="width:100px;"><?= $this->Paginator->sort('url') ?></th>
                                    <th><?= $this->Paginator->sort('location') ?></th>
                                    <th><?= $this->Paginator->sort('image') ?></th>
                                    <th><?= $this->Paginator->sort('display_order') ?></th>
                                    <th><?= $this->Paginator->sort('active') ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>

                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ads as $ad): ?>
                                    <tr>
                                        <td><input type="checkbox" name="chk[]" value="<?php echo $ad->id ?>" /></td>
                                        <td><?= h($ad->title) ?></td>
                                        <td><a href="<?= h($ad->url) ?>"><?= $this->Text->truncate($ad->url, 20); ?></a></td>
                                        <td><?= $locations[$ad->location] ?></td>
                                        <td><img src="<?= resizeOnFly($ad->image, 0, 50, 0, $ad->image_info['folder_path']) ?>" /></td>
                                        <td><?= $this->Number->format($ad->display_order) ?></td>
                                        <td><?= ($ad->active) ? __('Yes') : __('No') ?></td>
                                        <td><?= h($ad->created) ?></td>

                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $ad->id], ['class' => 'btn btn-primary', 'escape' => false]) ?>
                                            <?= $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $ad->id], ['class' => 'btn btn-danger', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $ad->title)]) ?>


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
