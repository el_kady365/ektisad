
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$ad->id) {
                        echo __('Add Ad');
                    } else {
                        echo __('Edit Ad');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($ad, ['type' => 'file']) ?>

                <?php
                if ($ad->location && $this->request->params['action'] == 'edit') {
                    echo $this->Form->hidden('location');
                } else {
                    echo $this->Form->input('location', ['class' => 'form-control', 'empty' => __('Choose Location')]);
                }

                echo $this->Form->input('title', ['class' => 'form-control']);
                echo $this->Form->input('url', ['class' => 'form-control']);
                echo $this->Form->input('image', array('type' => 'file', 'templateVars' => ['between' => $this->element('image_between', ['file' => $ad->image_info, 'settings' => $imageSettings['image']])]));
                if (!empty($category) || $ad->category_id) {
                    echo $this->Form->input('category_id', ['class' => 'form-control', 'empty' => __('Select Category')]);
                }
                echo $this->Form->input('display_order', ['type' => 'text', 'min' => 0, 'class' => 'form-control']);
                echo $this->Form->input('active', ['label' => ['class' => 'checkbox-inline']]);
                ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("form").validate({
        errorClass: "error-message",
        ignore: [],
        errorElement: "div",
        rules: {
            title: {
                required: true,
            },
            url: {
                required: true,
                url: true
            },
            category_id: {
                required: true,
            },
            display_order: {
                digits: true
            }
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }
    });
    $(function () {
        $('#location').on('change', function () {
            window.location = '<?php echo $this->Url->build(['action' => 'add', 'category' => !empty($category) ? '1' : '']) ?>/' + $(this).val();
        })
    });
</script>
<?php echo $this->end(); ?>
