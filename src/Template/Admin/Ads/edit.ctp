
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$ad->id) {
                    echo __('Add Ad');
                    } else {
                    echo __('Edit Ad');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($ad) ?>

                <?php
                                echo $this->Form->input('title', ['class' => 'form-control']);
                                            echo $this->Form->input('url', ['class' => 'form-control']);
                                            echo $this->Form->input('location', ['class' => 'form-control']);
                                            echo $this->Form->input('image', ['class' => 'form-control']);
                                            echo $this->Form->input('display_order', ['class' => 'form-control']);
                                            echo $this->Form->input('active',['label'=>['class'=>'checkbox-inline']]);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
