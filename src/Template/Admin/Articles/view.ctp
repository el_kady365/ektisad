<style>

    .nanos-content{
        height:300px;
        overflow-y: scroll
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php echo __('View Article') ?>
                </h3>
            </div>
            <div class="panel-body">
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $xlang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $xlang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <?php foreach ($languages as $key => $xlang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <table class="table vertical-table">
                                    <tr>
                                        <th width="200"><?= __('Title') ?></th>
                                        <td><?= h($article->{$key . '_title'}) ?></td>
                                    </tr>
                                    

                                    <tr>
                                        <th><?= __('Description') ?></th>
                                        <td>
                                            <div class="nanos">
                                                <div class="nanos-content">
                                                    <?= $article->{$key . '_description'} ?>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <th><?= __('Meta Description') ?></th>
                                        <td><?= $this->Text->autoParagraph($article->{$key . '_meta_description'}) ?></td>
                                    </tr>
                                </table>
                            </div>
                        <?php } ?>

                    </div>



                    <table class="table vertical-table">
                        <tr>
                            <th><?= __('Written by') ?></th>
                            <td><?= $article->has('writer') ? $article->writer->ar_title : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Editor') ?></th>
                            <td><?= $article->has('editor') ? $article->editor->username : '' ?></td>
                        </tr>
                        
                        <tr>
                            <th><?= __('Reviewer') ?></th>
                            <td><?= $article->has('reviewer') ? $article->reviewer->username : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Publisher') ?></th>
                            <td><?= $article->has('publisher') ? $article->publisher->username : '' ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <td><?= $this->Number->format($article->id) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Publish Date') ?></th>
                            <td><?= h($article->publish_date) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Created') ?></th>
                            <td><?= h($article->created) ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Modified') ?></th>
                            <td><?= h($article->modified) ?></td>
                        </tr>
                        
                        <tr>
                            <th><?= __('Reviewed') ?></th>
                            <td><?= $article->reviewed ? __('Yes') : __('No'); ?></td>
                        </tr>
                        <tr>
                            <th><?= __('Published') ?></th>
                            <td><?= $article->published ? __('Yes') : __('No'); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


