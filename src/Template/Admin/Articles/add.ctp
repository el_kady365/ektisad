
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php
                    if (!$article->id) {
                        echo __('Add Article');
                    } else {
                        echo __('Edit Article');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($article, ['id' => 'ArticleForm']) ?>
                <div id="tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php foreach ($languages as $key => $lang) { ?>
                            <li role="presentation" class="<?php echo $key == 'ar' ? 'active' : ""; ?>">
                                <a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $lang ?></a>
                            </li>
                        <?php } ?>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        if (empty($user['writer_id']))
                            echo $this->Form->input('writer_id', ['class' => 'form-control', 'options' => $writers, 'empty' => __('Select Writer')]);
                        ?>
                        <?php foreach ($languages as $key => $lang) { ?>
                            <div role="tabpanel" class="tab-pane <?php echo $key == 'ar' ? 'active' : ""; ?>" id="tab-<?php echo $key ?>">
                                <?php
                                echo $this->Form->input($key . '_title', ['class' => 'form-control', 'label' => __('Title', true)]);
                                echo $this->Form->input($key . '_description', ['class' => 'form-control ckeditor', 'label' => __('Description', true)]);

                                echo $this->Form->input($key . '_meta_description', ['class' => 'form-control', 'label' => __('Meta Description', true)]);
                                echo $this->Form->input($key . '_tags', ['class' => 'form-control tags', 'label' => __('Tags', true)]);
                                ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <?php
                if (hasCapablity('articles', 'review', $user)) {
                    echo $this->Form->input('reviewed', ['label' => ['class' => 'checkbox-inline']]);
                }
                if ($article->publish_date != '') {
                    $publish_date = new Cake\I18n\Time($article->publish_date);
                    $article->publish_date = $publish_date->format('Y-m-d H:i');
                }
                if (hasCapablity('articles', 'publish', $user)) {
                    echo $this->Form->input('published', ['label' => ['class' => 'checkbox-inline']]);
                    echo $this->Form->input('publish_date', ['type' => 'text', 'class' => 'form-control dateTimePicker', 'value' => ($article->publish_date != '') ? $article->publish_date : date('Y-m-d H:i')]);
                }
                ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->css(array('admin/plugins/selectize.bootstrap3', 'admin/plugins/jquery.datetimepicker'), ['inline' => false]) ?>
<?php echo $this->Html->script(array('admin/selectize.min', 'admin/jquery.datetimepicker'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("#ArticleForm").validate({
        errorClass: "error-message",
        highlight: function (label) {
            $(label).closest('.form-group').addClass('has-errors');
            $fieldset = $(label).closest('.tab-content');
            if ($($fieldset).find(".tab-pane.active:has(div.has-errors)").length == 0) {
                $($fieldset).find(".tab-pane:has(div.has-errors)").each(function (index, tab) {
                    $('a[href=#' + $(label).closest('.tab-pane:not(.active)').attr('id') + ']').tab('show');
                });
            }
        },
        'ignore': [],
        errorElement: "div",
        rules: {
            ar_description: {
                required: function () {
                    CKEDITOR.instances['ar-description'].updateElement();
                },
            },
            en_description: {
                required: function () {
                    CKEDITOR.instances['en-description'].updateElement();
                },
            },
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea

            if (element.is("textarea")) {
                label.insertAfter(element.next());
            } else {
                label.insertAfter(element);
            }
        }
    });
    $(function () {
        var writer = $('#writer-id').selectize({
            onChange: function (value) {
                var control = $('#writer-id').closest('.form-group');
                console.log(control);
                if (control.hasClass('has-errors') && value) {
                    control.removeClass('has-errors');
                    $('#writer-id').closest('.form-group').find('.error-message').remove();
                }
            }});

        $('.tags').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            },
            render: {
                item: function (data, escape) {
                    return '<div>' + escape(data.text) + '</div>';
                }
            },
            onDelete: function (values) {
                return confirm(values.length > 1 ? 'Are you sure you want to remove these ' + values.length + ' items?' : 'Are you sure you want to remove "' + values[0] + '"?');
            }
        });
        $('.dateTimePicker').datetimepicker({
            format: 'Y-m-d H:i'
        });


    });
</script>
<?php echo $this->end(); ?>
