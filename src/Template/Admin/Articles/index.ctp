<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?= __('Articles') ?></h3>
            </div>
            <div class="panel-body">
                <!--Table Wrapper Start-->
                <form action="<?php echo $this->Url->build(array("action" => "do-operation")) ?>" method="post">
                    <div class="table-responsive ls-table">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="checkall" /></th>
                                    <th><?= $this->Paginator->sort('ar_title', __('Title')) ?></th>
                                    <?php if (in_array($user['group_id'], [1, 2, 3])) {
                                        ?>
                                        <th><?= $this->Paginator->sort('editor_id', __('Edited by')) ?></th>
                                        <?php
                                    }
                                    ?>
                                    <th><?= $this->Paginator->sort('reviewer_id', __('Reviewed by')) ?></th>
                                    <th><?= $this->Paginator->sort('publisher_id', __('Published by')) ?></th>
                                    <th><?= $this->Paginator->sort('publish_date') ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($articles as $article): ?>
                                    <tr>
                                        <td><input type="checkbox" name="chk[]" value="<?php echo $article->id ?>" /></td>
                                        <td><?= h($article->ar_title) ?></td>
                                        <?php if (in_array($user['group_id'], [1, 2, 3])) {
                                            ?>
                                            <td><?php echo $article->has('editor') ? $article->editor->username : '' ?></td>
                                            <?php
                                        }
                                        ?>
                                        <td><?php echo $article->has('reviewer') ? $article->reviewer->username : '' ?></td>
                                        <td><?php echo $article->has('publisher') ? $article->publisher->username : '' ?></td>

                                        <td><?= h($article->publish_date) ?></td>
                                        <td><?= h($article->created) ?></td>
                                        <td class="actions">
                                            <?php
                                            $edit = false;
                                            $publish = false;
                                            $review = false;
                                            $can_delete = false;
                                            if (
                                                    (hasCapablity('articles', 'manage', $user) && hasCapablity('articles', 'publish', $user)) ||
                                                    (isset($article->editor->id) && ($article->editor->id == $user['id']) && !$article->published && !$article->reviewed) ||
                                                    (!$article->published && hasCapablity('articles', 'review', $user) && hasCapablity('articles', 'manage', $user))
                                            ) {
                                                $edit = true;
                                            }


                                            if (hasCapablity('articles', 'review', $user) && !$article->reviewed) {
                                                $review = true;
                                            }
                                            if (hasCapablity('articles', 'publish', $user) && !$article->published) {
                                                $publish = true;
                                            }
                                            echo $this->Html->link('<i class="fa fa-search"></i> ' . __('View'), ['action' => 'view', $article->id], ['class' => 'btn btn-default btn-xs', 'escape' => false]) . ' ';
                                            if (isset($article->editor->id) && ($article->editor->id == $user['id']) && !$article->published && !$article->reviewed && hasCapablity('articles', 'delete', $user)) {
                                                $can_delete = true;
                                            } elseif (!$article->published && hasCapablity('articles', 'review', $user) && hasCapablity('articles', 'delete', $user)) {
                                                $can_delete = true;
                                            } elseif (hasCapablity('articles', 'publish', $user) && hasCapablity('articles', 'delete', $user)) {
                                                $can_delete = true;
                                            }

                                            if ($edit) {
                                                echo $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit'), ['action' => 'edit', $article->id], ['class' => 'btn btn-primary btn-xs', 'escape' => false]) . ' ';
                                            }
                                            if ($publish) {
                                                echo $this->Html->link('<i class="fa fa-check"></i> ' . __('Publish'), ['action' => 'publish', $article->id], ['class' => 'btn btn-success btn-xs', 'escape' => false]) . ' ';
                                            }
                                            if ($review) {
                                                echo $this->Html->link('<i class="fa fa-search"></i> ' . __('Reviewed'), ['action' => 'review', $article->id], ['class' => 'btn btn-success btn-xs', 'escape' => false]) . ' ';
                                            }
                                            if ($can_delete) {
                                                echo $this->Html->link('<i class="fa fa-trash"></i> ' . __('Delete'), ['action' => 'delete', $article->id], ['class' => 'btn btn-danger btn-xs', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $article->ar_title)]) . ' ';
                                            }
                                            ?>



                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control" id="acts" name="operation">
                                    <option value="">
                                        <?php echo __("Choose Operation") ?></option>
                                    <option value="delete"><?php echo __("Delete") ?></option>
                                </select>
                            </div>
                            <div class="col-md-9 text-right">
                                <ul class="pagination ls-pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('previous'));
                                    }
                                    echo $this->Paginator->numbers();
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('next') . ' >');
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
