
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?php if (!$contact->id) {
                    echo __('Add Contact');
                    } else {
                    echo __('Edit Contact');
                    }
                    ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($contact) ?>

                <?php
                                echo $this->Form->input('name', ['class' => 'form-control']);
                                            echo $this->Form->input('email', ['class' => 'form-control']);
                                            echo $this->Form->input('message', ['class' => 'form-control']);
                                            ?>

                <?= $this->Form->button(__('Submit')) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
