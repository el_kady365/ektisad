<div class="section my-padding">
    <?php
    if ($type == 1) {
        $title = __('Contact us');
    } else {
        $title = __('Suggestions');
    }
    ?>
    <h2 class="title clearfix"><span><?php echo $title ?></span></h2>
    <?php if ($type == 2) { ?>
        <p class="send-sugg"><?php echo __('You can contact us and send your suggestions and complaints') ?></p>
    <?php } ?>
    <div>
        <?= $this->Form->create($contact, ['id' => 'NewsletterAddPrinted']) ?>
        <?php echo $this->Form->input('name', ['label' => __('Name'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>
        <?php echo $this->Form->input('email', ['label' => __('Email'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>
        <?php echo $this->Form->input('message', ['label' => __('Message'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-12']]) ?>

        <div class="col-xs-12 text-center">
            <button type="submit" class="btn btn-default"><?php echo __('Send') ?></button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <?php if ($type == 1) { ?>
        <div class="clearfix"></div>
        <div class="text-center contact">
            <div class="col-xs-12" style="padding-bottom: 10px;">
                <h4 class="col-xs-4"><?php echo __('Telephone') ?></h4>
                <p class="col-xs-8"><?php echo $config['telephone'] ?></p>
            </div>
            <div class="col-xs-12"  style="padding-bottom: 10px;">
                <h4 class="col-xs-4"><?php echo __('Mobile') ?></h4>
                <p class="col-xs-8"><?php echo nl2br($config['mobile']) ?></p>
            </div>
            <div class="col-xs-12"  style="padding-bottom: 10px;">
                <h4 class="col-xs-4"><?php echo __('Email') ?></h4>
                <p class="col-xs-8" style="text-transform: lowercase"><?php echo $config['email'] ?></p>
            </div>
        </div>
    <?php } ?>
</div>
<?php echo $this->Html->script(array('jquery.validate'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("#NewsletterAddPrinted").validate({
        errorClass: "error-message",
        'ignore': [],
        errorElement: "div",
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            message: {
                required: true
            }
        },
        messages: {
            name: {
                required: '<?php echo __('Required', true) ?>',
            },
            message: {
                required: '<?php echo __('Required', true) ?>',
            },
            email: {
                required: '<?php echo __('Required', true) ?>',
                email: '<?php echo __('Please enter a valid email', true) ?>',
            },
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea
            label.insertAfter(element);
        }
    });
</script>
<?php echo $this->end(); ?>
