<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Advertise with us'); ?></span></h2>

    <div class="about">
        <div>
            <p class="adv"><?php echo __('To advertise on "{0}" site please contact on:', $config['site_name_' . $lang]); ?></p>

            <ul class="adv-contact">
                <li><span><?php echo __('Email') ?>:</span>
                    <p style="text-transform: lowercase;display:inline"><?php echo $config['email'] ?></p>
                </li>
                <li><span><?php echo __('Telephone') ?>:</span>
                    <p style="text-transform: lowercase;display:inline"><?php echo nl2br($config['telephone']) ?></p>
                </li>
                <li><span><?php echo __('Mobile') ?>:</span>
                    <p style="text-transform: lowercase;display:inline"><?php echo nl2br($config['mobile']) ?></p>
                </li>
                <li><span><?php echo __('Post Office') ?>:</span>
                    <p style="text-transform: lowercase;display:inline"><?php echo $config['post_office'] ?></p>
                </li>
            </ul>
        </div>
    </div>
</div>
