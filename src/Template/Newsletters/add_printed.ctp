<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Printed version subscription') ?></span></h2>
    <div>
        <?= $this->Form->create($newsletter, ['id' => 'NewsletterAddPrinted']) ?>
        <?php echo $this->Form->input('name', ['label' => __('Name'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>
        <?php echo $this->Form->input('telephone', ['label' => __('Telephone'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>
        <?php echo $this->Form->input('email', ['label' => __('Email'), 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>
        <?php echo $this->Form->input('address', ['label' => __('Address'), 'type' => 'text', 'class' => 'form-control', 'templateVars' => ['div_classes' => 'col-sm-6']]) ?>

        <div class="col-xs-12 text-center">
            <button type="submit" class="btn btn-default"><?php echo __('Send') ?></button>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="clearfix"></div>
</div>
<?php echo $this->Html->script(array('jquery.validate'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("#NewsletterAddPrinted").validate({
        errorClass: "error-message",
        'ignore': [],
        errorElement: "div",
        rules: {
            telephone: {
                required: true,
                number: true,
                maxlength: 9,
                minlength: 9
            },
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            Address: {
                required: true
            }
        },
        messages: {
            name: {
                required: '<?php echo __('Required', true) ?>',
            },
            telephone: {
                required: '<?php echo __('Required', true) ?>',
                number: '<?php echo __('This field accepts numbers only', true) ?>',
                maxlength: '<?php echo __('This field should not exceed {0} characters', 9) ?>',
                minlength: '<?php echo __('This field should not less than {0} characters', 9) ?>',
            },
            email: {
                required: '<?php echo __('Required', true) ?>',
                email: '<?php echo __('Please enter a valid email', true) ?>',
            },
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea
            label.insertAfter(element);
        }
    });
</script>
<?php echo $this->end(); ?>