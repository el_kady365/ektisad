<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Images') ?></span></h2>
    <div class="homepage">
        <div class="container-fluid">
            <div class="row">
                <?php foreach ($images as $image) { ?>
                    <div class="col-md-3 col-xs-4">
                        <div class="gallery-item">
                            <img src="<?php echo resizeOnFly($image->image, 107, 110, 1, $image->image_info['folder_path']) ?>" alt="<?php echo $image->{$lang . '_title'} ?>">
                            <div class="overlay">
                                <a href="<?php echo $image->image_info['path'] ?>" data-rel="lightbox" class="fa fa-expand"></a>
                            </div>
                        </div> <!-- /.gallery-item -->
                    </div> <!-- /.col-md-2 -->
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="more-news text-center">
    <ul class="pager">
        <?php
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev(__('Previous'), ['class' => '']);
        }
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next(__('Next'), ['class' => '']);
        }
        ?>

    </ul>
</div>

<?php echo $this->append('script'); ?>
<script>
    (function () {
        var currentScale = 1;

        var cssPrefixesMap = [
            'scale',
            '-webkit-transform',
            '-moz-transform',
            '-ms-transform',
            '-o-transform',
            'transform'];

        function setScale(scale) {
            var scaleCss = {};

            cssPrefixesMap.forEach(function (prefix) {
                scaleCss[prefix] = 'scale(' + scale + ')';
            });

            $('#img-A-Zoom').css(scaleCss);
        }

        $('body').on('click', '.lightbox-zoomOut', function () {
            setScale(currentScale = currentScale - 0.1);
        });

        $('body').on('click', '.lightbox-zoomIn', function () {
            setScale(currentScale = currentScale + 0.1);
        });
    })();
</script>
<?php echo $this->end() ?>