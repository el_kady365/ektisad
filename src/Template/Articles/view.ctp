<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Articles') ?></span></h2>
    <div class="articles the-articles">
        <div class="row">
            <div class="col-xs-12 my-right">

                <div class="block">
                    <div class="col-sm-3 box">
                        <?php if ($article->writer->image) { ?>
                            <img src="<?php echo $article->writer->image_info['thumb1'] ?>">
                        <?php } ?>
                    </div>
                    <div class="col-sm-9">
                        <h5><?php echo $article->{$lang . '_title'} ?></h5>

                        <div>
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <span><?php
                                $time = new Cake\I18n\Time($article->publish_date);
                                echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                                ?></span>
                        </div>
                        <h5><a href="<?php echo $this->Url->build(['controller' => 'writers', 'action' => 'view', $article->writer_id]) ?>"><?php echo $article->writer->{$lang . '_title'} ?></a></h5>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 my-article">
                <?php echo $article->{$lang . '_description'} ?>
            </div>
        </div>
    </div>

</div>