<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<font face="Arial" style="font-size:18px;" ><?php __('You have received a new contact us message') ?></font><br /><br />

<ul style="font-family:Arial, Helvetica, sans-serif; font-size:12px; ">    
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $this->Html->tag('li', $this->Html->tag('strong', __('Name', true)) . ': ' . $data['name']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $this->Html->tag('li', $this->Html->tag('strong', __('Email', true)) . ': ' . $data['email']); ?>
    </font>
    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $this->Html->tag('li', $this->Html->tag('strong', __('Message', true)) . ': ' . $data['message']); ?>
    </font>

    <font face="Arial" style="font-family:Arial; font-size:12px; text-align:left;">
    <?php echo $this->Html->tag('li', $this->Html->tag('strong', __('Date on', true)) . ': ' . date('d / m / Y')); ?>
    </font>

</ul>