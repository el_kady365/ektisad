<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo $page->{$lang . '_title'} ?></span></h2>
    <div>
        <?php if ($page->image) { ?>
            <img src="<?php echo $page->image_info['path'] ?>" class="new-img">
        <?php } ?>
    </div>
    <div class="about">
        <?php echo $page->{$lang . '_content'} ?>
    </div>
</div>
