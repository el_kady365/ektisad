<div id="the-video" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header video-modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-center"></p>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <iframe id="the-youtube" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer video-modal-footer">
                <div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span></span>
                </div>
            </div>


        </div>

    </div>
</div>



<div class="section my-padding the-video">
    <h2 class="title clearfix"><span><?php echo __('Multimedia') ?></span><h6 class="read-more">
            <a href="<?php echo $this->Url->build(['controller' => 'videos']) ?>"><?php echo __('More') ?></a></h6></h2>
    <div class="video3">
        <div id="my-video" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">


                <?php
                $counter = 0;
                foreach ($home_videos as $key => $video) {
                    $date = new Cake\I18n\Time($video->created);
                    $time = $date->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                    if ($counter == 0 || $counter % 4 == 0) {
                        ?>
                        <div class="item <?php echo $counter ?> <?php echo $counter == 0 ? "active" : "" ?>">
                            <div class="row">
                            <?php } ?>
                            <div class="col-sm-3 <?php echo $counter ?> <?php echo ($counter != 0 && ($counter + 1) % 4 == 0) ? "hidden-xs" : "col-xs-4" ?>">
                                <div class="one-video">
                                    <?php echo parseVideoUrl($video->url, 'embed', 150, 150); ?>
                                    <div class="open-video" data-url="<?php echo parseVideoUrl($video->url, 'iframe_url') ?>" data-text="<?php echo $video->{$lang . '_title'} ?>" data-time="<?php echo $time ?>" data-toggle="modal" data-target="#the-video"></div>
                                    <p class="text-center vid-name"><?php echo $this->Text->truncate($video->{$lang . '_title'}, 30, ['exact' => true]) ?></p>
                                </div>
                            </div>
                            <?php
                            if ($counter != 0 && ($counter + 1) % 4 == 0) {
                                echo '</div></div>';
                            }
                            $counter++;
                        }
                        if ($counter != 0 && ($counter) % 4 != 0) {
                            echo '</div></div>';
                        }
                        ?>



                    </div>

                    <!--Left and right controls -->
                    <a class = "left carousel-control hidden-xs" href = "#my-video" role = "button" data-slide = "prev">
                        <span class = "glyphicon glyphicon-chevron-left" aria-hidden = "true"></span>
                        <span class = "sr-only">Previous</span>
                    </a>
                    <a class = "right carousel-control hidden-xs" href = "#my-video" role = "button" data-slide = "next">
                        <span class = "glyphicon glyphicon-chevron-right" aria-hidden = "true"></span>
                        <span class = "sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <?php
        foreach ($home_categories as $category) {
            if (!$category['News']->isEmpty()) {
                $category_url = $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'index', $category['id'], $category[$lang . '_permalink']]);
                ?>
                <div class="section my-padding">
                    <h2 class="title clearfix">
                        <span>
                            <a href="<?php echo $category_url ?>"><?php echo $category[$lang . '_title'] ?></a>
                        </span>
                        <h6 class="read-more"><a href="<?php echo $category_url ?>"><?php echo __('More'); ?></a></h6>
                    </h2>
                    <?php foreach ($category['News'] as $key => $news) { ?>
                        <div class="col-xs-6 my-right">
                            <a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]) ?>">
                                <div class="block">
                                    <div class="col-xs-3 box">
                                        <?php if (isset($news->news_images[0]['image'])) { ?>
                                            <img src="<?php echo resizeOnFly($news->news_images[0]['image'], 92, 78, 1, $news->news_images[0]['image_info']['folder_path']) ?>">
                                        <?php } ?>
                                    </div>
                                    <div class="col-xs-9">
                                        <h5><?php echo $news->{$lang . '_title'} ?></h5>
                                        <div>
                                            <i class="fa fa-clock-o"></i>
                                            <span><?php
                                                $time = new Cake\I18n\Time($news->publish_date);
                                                echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                                                ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php
                        if ($key == 1) {
                            echo '<div class="clearfix"></div>';
                        }
                        ?>
                        <?php
                        if ($key == 3) {
                            break;
                        }
                    }
                    ?>

                </div>
                <div class="clearfix"></div>
                <?php
            }
        }
        ?>

        <?php echo $this->append('script'); ?>
        <script>
            $(function () {
                $('.open-video').on('click', function () {
                    $('#the-youtube').prop('src', $(this).data('url'));
                    console.log($(this).data('text'));
                    console.log($('.modal-header p.text-center'));
                    $('.modal-header p.text-center').text($(this).data('text'));
                    $('.modal-footer span').text($(this).data('time'));

                });

                $("#the-video").on('hidden.bs.modal', function () {
                    $('#the-youtube').prop('src', '');
                    $('.modal-header p.text-center').text('');
                    $('.modal-footer span').text('');
                });
            });
        </script>
        <?php
        echo $this->end()?>