<div id="the-video" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header video-modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="text-center"></p>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <iframe id="the-youtube" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="modal-footer video-modal-footer">
                <div>
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span></span>
                </div>
            </div>


        </div>

    </div>
</div>
<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Videos') ?></span></h2>
    <div class="homepage">
        <div class="container-fluid">
            <div class="row videos">

                <?php
                foreach ($videos as $video) {
                    $date = new Cake\I18n\Time($video->created);
                    $time = $date->i18nFormat('dd LLLL YYYY - hh:m:ss a')
                    ?>
                    <div class="col-md-3 col-xs-4">
                        <div class="one-video">
                            <div class="one-video">
                                <?php echo parseVideoUrl($video->url, 'embed', 150, 150); ?>
                                <div class="open-video" data-url="<?php echo parseVideoUrl($video->url, 'iframe_url') ?>" data-text="<?php echo $video->{$lang . '_title'} ?>" data-time="<?php echo $time ?>" data-toggle="modal" data-target="#the-video"></div>
                                <p class="text-center vid-name"><?php echo $this->Text->truncate($video->{$lang . '_title'}, 30, ['exact' => true]) ?></p>
                            </div>
                        </div> <!-- /.gallery-item -->
                    </div> <!-- /.col-md-2 -->
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div class="more-news text-center">
    <ul class="pager">
        <?php
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev(__('Previous'), ['class' => '']);
        }
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next(__('Next'), ['class' => '']);
        }
        ?>

    </ul>
</div>
<?php echo $this->append('script'); ?>
<script>
    $(function () {
        $('.open-video').on('click', function () {
            $('#the-youtube').prop('src', $(this).data('url'));
            console.log($(this).data('text'));
            console.log($('.modal-header p.text-center'));
            $('.modal-header p.text-center').text($(this).data('text'));
            $('.modal-footer span').text($(this).data('time'));

        });

        $("#the-video").on('hidden.bs.modal', function () {
            $('#the-youtube').prop('src', '');
            $('.modal-header p.text-center').text('');
            $('.modal-footer span').text('');
        });
    });
</script>
<?php
echo $this->end()?>