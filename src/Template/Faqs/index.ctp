<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Frequently Asked Questions')?></span></h2>
    <div class="row">
        <div class="col-xs-12">
            <?php foreach ($faqs as $faq) { ?>
                <div class="faq">
                    <p class="ask"><span class="glyphicon glyphicon-circle-arrow-left"></span><?php echo $faq->{$lang.'_question'}?></p>
                    <p class="answer"><?php echo $faq->{$lang.'_answer'}?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
