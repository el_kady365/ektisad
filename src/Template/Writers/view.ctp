<div class="section my-padding">
    <div class="articles the-articles">
        <div class="row">
            <div class="col-xs-12 my-right">

                <div class="block">
                    <div class="col-sm-3 box">
                        <?php if ($writer->image) { ?>
                            <img class="my-img writer" src="<?php echo $writer->image_info['thumb1'] ?>">
                        <?php } ?>
                    </div>
                    <div class="col-sm-9">
                        <h5><?php echo $writer->{$lang . '_title'}?></h5>
                        <div>
                            <p class="writer-text"><?php echo $writer->{$lang . '_about'}?></p>
                        </div>                               
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="clearfix"></div>
<div class="section my-padding the-news articles">
    <h2 class="title clearfix"><span><?php echo __('Articles') ?></span></h2>
    <?php
    foreach ($articles as $article) {
        $article_url = $this->Url->build(['language' => $lang, 'controller' => 'articles', 'action' => 'view', $article->id, $article->{$lang . '_permalink'}]);
        ?>
        <div class="col-xs-12 my-right">
            <div class="block">
                <div class="col-xs-3 box">
                    <?php if ($article->writer->image) { ?>
                    <a href="<?php echo $article_url; ?>"><img class="my-img writer" src="<?php echo $article->writer->image_info['path'] ?>"></a>
                    <?php } ?>
                </div>
                <div class="col-xs-9">
                    <h5><a href="<?php echo $article_url; ?>"><?php echo $article->{$lang . '_title'} ?></a></h5>
                    <div>    
                        <span><i class="fa fa-user" aria-hidden="true"></i> <a href="<?php echo $this->Url->build(['controller' => 'writers', 'action' => 'view', $article->writer_id]) ?>"><?php echo $article->writer->{$lang . '_title'} ?></a></span>
                        <span style="float: left;"> <i class="fa fa-clock-o" aria-hidden="true"></i> <?php
                            $time = new Cake\I18n\Time($article->publish_date);
                           echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                            ?></span>
                    </div>
                    <p><?php echo $this->Text->truncate(strip_tags($article->{$lang . '_description'}), 200) ?><a href="<?php echo $article_url; ?>" class="read2"><?php echo __("Read more") ?></a></p>

                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    <?php } ?>
</div>
<div class="more-news text-center">
    <ul class="pager">
        <?php
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev(__('Previous'), ['class' => '']);
        }
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next(__('Next'), ['class' => '']);
        }
        ?>

    </ul>
</div>