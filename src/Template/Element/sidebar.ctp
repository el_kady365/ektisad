<a href="<?php echo $config['printed_version'] ?>" class="printed" target="_blank">
    <div class="read my-padding ">
        <img src="<?php echo $this->Url->build('/css/img/read-512.png') ?>">
        <h5><?php echo __('Browse the printed version') ?></h5>
    </div>
</a>
<a href="<?php echo $this->Url->build(['controller' => 'newsletters', 'action' => 'add-printed']) ?>" class="printed">
    <div class="read my-padding2">
        <img src="<?php echo $this->Url->build('/css/img/e-mail_envelope_marketing-512.png') ?>">
        <h5><?php echo __('Printed version subscription') ?></h5>
    </div>
</a>
<?php
$side_ads = $ads->filter(function($row) {
    return $row->location == 'side_bar';
});

if (!$side_ads->isEmpty()) {
    ?>
    <div class = "banner3 text-center">

        <div id = "the-banner3" class = "carousel slide" data-ride = "carousel">
            <!--Wrapper for slides -->
            <div class = "carousel-inner" role = "listbox">
                <?php
                $c2 = 0;
                foreach ($side_ads as $side_ad) {
                    ?>
                    <div class="item <?php echo $c2++ == 0 ? "active" : ''; ?>">
                        <a href = "<?php echo $side_ad->url; ?>" target="_blank"><img src="<?php echo $side_ad->image_info['path']; ?>"></a>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
<?php } ?>


<div class = "section mysection">
    <h2 class = "title clearfix"><span><a href = "javascript:;"><?php echo __('Indices and currencies') ?></a></span></h2>
    <div>
        <ul class = "nav nav-tabs">
            <li class = "active"><a data-toggle = "tab" href = "#chart1"><?php echo __('Indices') ?></a></li>
            <li><a data-toggle = "tab" href = "#chart2"><?php echo __('Currencies') ?></a></li>
            <li><a data-toggle = "tab" href = "#chart3"><?php echo __('Gold') ?></a></li>
            <li><a data-toggle = "tab" href = "#chart4"><?php echo __('Silver') ?></a></li>
        </ul>

        <div class = "tab-content my-chart">
            <div id = "chart1" class = "tab-pane fade in active">

                <div id = "container1" style = "height: 400px; min-width: 350px"></div>
                <div class = "text-center vote">
                    <a href = "chart.html" > <input type = "button" value = "<?php echo __('More') ?>"></a>
                </div>
            </div>
            <div id = "chart2" class = "tab-pane fade">
                <div id = "container2" style = "height: 400px; min-width: 350px"></div>
                <div class = "text-center vote">
                    <a href = "chart.html" > <input type = "button" value = "<?php echo __('More') ?>"></a>
                </div>
            </div>
            <div id = "chart3" class = "tab-pane fade">
                <div id = "container3" style = "height: 400px; min-width: 350px"></div>
                <div class = "text-center vote">
                    <a href = "chart.html" > <input type = "button" value = "<?php echo __('More') ?>"></a>
                </div>
            </div>
            <div id = "chart4" class = "tab-pane fade">
                <div id = "container4" style = "height: 400px; min-width: 350px"></div>
                <div class = "text-center vote">
                    <a href = "chart.html" > <input type = "button" value = "<?php echo __('More') ?>"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class = "section">
    <h2 class = "title clearfix"><span><a href = "javascript:;"><?php echo __('Top news')
?></a></span></h2>
    <div>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"><?php echo __('Most viewed') ?></a></li>
            <li><a data-toggle="tab" href="#menu1"><?php echo __('Most commented') ?></a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <?php foreach ($most_viewed as $news) { ?>
                    <a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]) ?>">
                        <div class="block">
                            <div class="col-xs-3 box">
                                <?php if (isset($news->news_images[0]['image'])) { ?>
                                    <img src="<?php echo resizeOnFly($news->news_images[0]['image'], 92, 78, 1, $news->news_images[0]['image_info']['folder_path']) ?>">
                                <?php } ?>
                            </div>
                            <div class="col-xs-9">
                                <h5><?php echo $news->{$lang . '_title'} ?></h5>
                                <div>
                                    <img src="<?php echo $this->Url->build('/css/img/1467551979_10.png') ?>" width="12">
                                    <span><?php
                                        $time = new Cake\I18n\Time($news->publish_date);
                                        echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                                        ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php } ?>

            </div>
            <div id="menu1" class="tab-pane fade">
                <?php foreach ($most_commented as $news) { ?>
                    <a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]) ?>">
                        <div class="block">
                            <div class="col-xs-3 box">
                                <?php if (isset($news->news_images[0]['image'])) { ?>
                                    <img src="<?php echo resizeOnFly($news->news_images[0]['image'], 92, 78, 1, $news->news_images[0]['image_info']['folder_path']) ?>">
                                <?php } ?>
                            </div>
                            <div class="col-xs-9">
                                <h5><?php echo $news->{$lang . '_title'} ?></h5>
                                <div>
                                    <i class="fa fa-clock-o"></i>
                                    <span><?php
                                        $time = new Cake\I18n\Time($news->publish_date);
                                        echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                                        ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php if ($side_articles) { ?>
    <div class="section my-padding">
        <h2 class="title clearfix"><span><a href="javascript:;"><?php echo __('Articles') ?></a></span>
            <h6 class="read-more"><a href="<?php echo $this->Url->build(['controller' => 'articles']) ?>"><?php echo __('More') ?></a></h6>
        </h2>
        <div class="article">

            <?php
            foreach ($side_articles as $side_article) {
                $article_url = $this->Url->build(['controller' => 'articles', 'action' => 'view', $side_article->id, $side_article->{$lang . '_permalink'}]);
                ?>
                <div class="block">
                    <div class="col-xs-3 box">
                        <?php if (isset($side_article->writer->image_info['thumb1'])) { ?>
                            <a href="<?php echo $article_url; ?>">
                                <img src="<?php echo $side_article->writer->image_info['thumb1'] ?>" class="my-img">
                            </a>
                        <?php } ?>
                    </div>
                    <div class="col-xs-9 artic">
                        <h5><a href="<?php echo $article_url; ?>"><?php echo $side_article->{$lang . '_title'} ?></a></h5>
                    </div>
                </div>
                <hr>
            <?php } ?>

        </div>
    </div>
<?php } ?>
<div class="section my-padding">
    <h2 class="title clearfix"><span><a href="javascript:;"><?php echo __('Images') ?></a></span>
        <h6 class="read-more"><a href="<?php echo $this->Url->build(['controller' => 'images']) ?>"><?php echo __('More') ?></a></h6></h2>
    <div class="images">
        <?php
        foreach ($side_images as $image) {
            if (isset($image->image_info['thumb1'])) {
                ?>
                <div class="col-lg-3 col-sm-4 col-xs-4">
                    <div class="gallery-item">
                        <img src="<?php echo $image->image_info['thumb1'] ?>" alt="<?php echo $image->{$lang . '_title'} ?>">
                        <div class="overlay">
                            <a href="<?php echo $image->image_info['path'] ?>" data-rel="lightbox" class="fa fa-expand"></a>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>

    </div>
</div>
<div class="clearfix"></div>
<?php
if (!empty($polls)) {
    ?>
    <div class="section my-padding">
        <h2 class="title clearfix"><span><a href="javascript:;"><?php echo __('Poll') ?></a></span></h2>


        <div class="poll-block">
            <h3><?php echo $polls->{$lang . '_title'} ?></h3>
            <?php if (!empty($polls->poll_question_options) && (!isset($_COOKIE['poll_question']) || $_COOKIE['poll_question'] != $polls->id)) { ?>
                <form method="post" action="<?php echo $this->Url->build(['controller' => 'pages', 'action' => 'answer', $polls->id]) ?>">
                    <ul class="poll-vote-list">
                        <?php foreach ($polls->poll_question_options as $opkey => $opt) { ?>
                            <li class="clearfix">
                                <input id="poll-choice1" value="<?php echo $opt->id ?>" type="radio" name="poll_question_option_id">
                                <label for="poll-choice1"><?php echo $opt->{$lang . '_title'} ?></label>
                            </li>
                        <?php } ?>

                    </ul>

                    <div class="vote-button clearfix text-center">
                        <input class="the-vote btn btn-custom" type="submit" value="<?php echo __('Vote') ?>" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="تم التصويت بنجاح">
                        <a class="results-trigger btn btn-custom the-result" href="javascript:;"><span><?php echo __('Results') ?></span></a>
                    </div>
                </form>
            <?php } else {
                ?>
                <a class="results-trigger btn btn-custom the-result hide" href="javascript:;"><span><?php echo __('Results') ?></span></a>
            <?php }
            ?>



            <ul class="poll-results-list" >

                <?php
                if (!empty($poll_options)) {
                    foreach ($poll_options as $option) {
                        ?>
                        <li>
                            <div class="clearfix">
                                <span class="percentage"><?php
                                    if ($total_results[0]['count'])
                                        $percent = ($option->count / $total_results[0]['count']) * 100;
                                    else
                                        $percent = 0;
                                    echo Cake\I18n\Number::format($percent, ['after' => '%', 'precision' => 0, 'locale' => 'en_US']);
                                    ?></span>
                                <span class="respond"><?php echo $option->{$lang . "_title"} ?></span>

                            </div>
                            <div class="out-bar">
                                <div class="inner-bar"></div>
                            </div>
                        </li>
                    <?php
                    }
                }else {
                    ?>
                        <div class="alert alert-warning">
                            <?php echo  __("There aren't any answers")?>
                        </div>
                    <?php
                }if (!isset($_COOKIE['poll_question']) || $_COOKIE['poll_question'] != $polls->id) {
                    ?>
                    <div class="vote text-center">
                        <input type="button" value="<?php echo __('Back') ?>" class="vote-step">
                    </div>
    <?php } ?>
            </ul>




        </div>



    </div>
<?php } ?>
<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo __('Follow us on Facebook') ?></span></h2>
    <div class="vote text-center">
        <div class="fb-page" data-href="<?php echo $config['facebook'] ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
    </div>
</div>
<div class="section my-padding">
<?php echo $this->Form->create('Newsletters', ['url' => ['controller' => 'newsletters', 'action' => 'add'], 'id' => 'NewsletterForm']); ?>
    <h2 class="title clearfix"><span><a href="javascript:;"><?php echo __('Newsletter') ?></a></span></h2>
    <div class="vote text-center">
        <p><?php echo __('Subscribe in our newsletter to get the latest news of stock markets') ?></p>
        <div class="text-center">
            <div class="result-message hide alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <div class="msgg"></div>
            </div>
            <input type="text" name="email" required="required" />
        </div>
        <div class="text-center">
            <button type="submit" class="btn btn-primary"><?php echo __('Subscribe') ?></button>

<?php //data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="تم الاشتراك بنجاح"           ?>
        </div>
    </div>
<?php echo $this->Form->end(); ?>
</div>

<?php echo $this->Html->script(array('jquery.validate'), ['block' => true]) ?>
<?php echo $this->append('script'); ?>
<script>
    var $v = $("#NewsletterForm").validate({
        errorClass: "error-message",
        'ignore': [],
        errorElement: "div",
        rules: {
            email: {
                required: true,
                email: true,
            }
        },
        messages: {
            email: {
                required: '<?php echo __('Required', true) ?>',
                email: '<?php echo __('Please enter a valid email', true) ?>',
            }
        },
        success: function (label, element) {
            label.parent().removeClass('has-errors');
            label.remove();
        },
        errorPlacement: function (label, element) {
            // position error label after generated textarea
            label.insertAfter(element);
        }, submitHandler: function (form) {
            $(form).find('.result-message').hide();
            $(form).find('.error-message').remove();
            $.ajax({
                type: "POST",
                url: $(form).prop('action'),
                data: $(form).serialize(),
                dataType: 'json',
            }).done(function (data) {
                data = data.return;
                if (data.status) {
                    $(form).find('.result-message').removeClass('hide alert-danger').addClass('alert-success').show().end().find('.msgg').text(data.message);
                    $(form).get(0).reset();
                } else {
                    $(form).find('.result-message').removeClass('hide alert-success').addClass('alert-danger').show().end().find('.msgg').text(data.message);
                    if (data.errors) {
                        for (i in data.errors) {
                            for (j in data.errors[i]) {
                                $(form).find('*[name="' + i + '"]').parent().append('<div class="error-message">' + data.errors[i][j] + '</div>');
                            }
                        }
                    }
                }
            });
            return false;
        }
    });
    $(window).load(function () {
<?php if (isset($_COOKIE['poll_question']) && ($_COOKIE['poll_question'] == $polls->id)) {
    ?>
            $('.results-trigger').click();
<?php } ?>
    });
    $(function () {

        $('.side-image').on('click', function () {
            $('#the-images .modal-body img').prop('src', $(this).data('url'));
        });
        $("#the-images").on('hidden.bs.modal', function () {
            $('#the-images .modal-body img').prop('src', '');
        });
    });
</script>
<?php echo $this->end() ?>

