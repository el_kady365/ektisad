
<div class="image_between">
    <?php
//    debug($settings);
    if (isset($settings)) {
        if ($settings['height'] == 0 && $settings['width'] == 0) {
            $message = __('You can upload image in any size');
        } elseif ($settings['width'] == 0) {
            $message = __('Image will be resized to adopt with height {0} px', $settings['height']);
        } elseif ($settings['height'] == 0) {
            $message = __('Image will be resized to adopt with width {0} px', $settings['width']);
        } else {
            $message = __('It is recommended to upload image {0}px X {1}px', $settings['width'], $settings['height']);
        }
        ?>
        <p class="hint image_desc"><?php echo $message ?></p>
    <?php } ?>
    <?php
    if (isset($file)) {
        ?>
        <span class="image_base_name"><?php echo $file['file_name'] ?></span>
        <a href="<?php echo $file['path'] ?>" target="_blank" class="btn btn-default btn-small"><i class="fa fa-search"></i> <?php echo __('Preview') ?></a>
    <?php } ?>
    <div class="clear"></div>
</div>

