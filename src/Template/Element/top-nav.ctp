<?php
if ($lang == "ar") {
    $homeUrl = $this->Url->build('/ar/');
} else {
    $homeUrl = $this->Url->build('/en/');
}
?>
<ul class="nav navbar-nav">
    <li ><a href="<?php echo $this->Url->build('/') ?>"><?php echo __('Home'); ?></a></li>
    <?php get_categories($this, $nav_categories); ?>
</ul>

<?php

function get_categories($view, $categories) {
    $lang = $view->viewVars['lang'];
    $selectedCategory = isset($view->viewVars['selectedCategory']) ? $view->viewVars['selectedCategory'] : '';
    foreach ($categories as $category) {

        $url = $view->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'index', $category->id, $category->{$lang . '_permalink'}])
        ?>
        <li <?php echo isset($selectedCategory) && $selectedCategory == $category->id ? 'class="active"' : '' ?>><a <?php echo $category->children ? 'class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"' : 'href="' . $url . '"' ?> ><?php echo $category->{$lang . '_title'} ?><?php echo $category->children ? '<span class="caret"></span>' : '' ?></a>
            <?php
            if ($category->children) {
                echo '<ul class="dropdown-menu sub-dropdown-menu">';
                get_categories($view, $category->children);
                echo '</ul>';
            }
            ?>
        </li>
        <?php
    }
}
