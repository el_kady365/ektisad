
<?php
$admin_menu = array(
    'Pages' => array('icon' => '<i class="fa fa-file"></i>',
        'subs' => array(
            __('List Pages', true) => array('url' => array('controller' => 'pages', 'action' => 'index')),
//            __('Add Page', true) => array('url' => array('controller' => 'pages', 'action' => 'add')),
        )
    ),
    'Contacts' => array('icon' => '<i class="fa fa-envelope"></i>',
        'subs' => array(
            __('List Contact Us messages', true) => array('url' => array('controller' => 'contacts', 'action' => 'index')),
        )
    ),
    'Categories' => array('icon' => '<i class="fa fa-folder"></i>',
        'subs' => array(
            __('List Categories', true) => array('url' => array('controller' => 'categories', 'action' => 'index')),
            __('Add Category', true) => array('url' => array('controller' => 'categories', 'action' => 'add')),
        )
    ),
    'News' => array('icon' => '<i class="fa fa-newspaper-o"></i>',
        'subs' => array(
            __('List News', true) => array('url' => array('controller' => 'news', 'action' => 'index')),
            __('Add News item', true) => array('url' => array('controller' => 'news', 'action' => 'add')),
        )
    ),
    'Ads' => array('icon' => '<i class="fa fa-bullhorn"></i>',
        'subs' => array(
            __('List Main Ads', true) => array('url' => array('controller' => 'ads', 'action' => 'index')),
            __('Add Main Ad', true) => array('url' => array('controller' => 'ads', 'action' => 'add')),
            __('List Categories Ads', true) => array('url' => array('controller' => 'ads', 'action' => 'index', 1)),
            __('Add Categories Ad', true) => array('url' => array('controller' => 'ads', 'action' => 'add', 1)),
        )
    ),
    'FAQ' => array('icon' => '<i class="fa fa-question-circle"></i>', 'id' => "Faqs",
        'subs' => array(
            __('List FAQ', true) => array('url' => array('controller' => 'faqs', 'action' => 'index')),
            __('Add FAQ', true) => array('url' => array('controller' => 'faqs', 'action' => 'add')),
        )
    ),
    'Polls' => array('icon' => '<i class="fa fa-tasks"></i>',
        'subs' => array(
            __('List Polls', true) => array('url' => array('controller' => 'poll-questions', 'action' => 'index')),
            __('Add Poll', true) => array('url' => array('controller' => 'poll-questions', 'action' => 'add')),
        )
    ),
    'Writers' => array('icon' => '<i class="fa fa-edit"></i>',
        'subs' => array(
            __('List Writers', true) => array('url' => array('controller' => 'writers', 'action' => 'index')),
            __('Add Writer', true) => array('url' => array('controller' => 'writers', 'action' => 'add')),
        )
    ),
    'Articles' => array('icon' => '<i class="fa fa-files-o"></i>',
        'subs' => array(
            __('List Articles', true) => array('url' => array('controller' => 'articles', 'action' => 'index')),
            __('Add Article', true) => array('url' => array('controller' => 'articles', 'action' => 'add')),
        )
    ),
    'Images' => array('icon' => '<i class="fa fa-file-image-o"></i>',
        'subs' => array(
            __('List Images', true) => array('url' => array('controller' => 'images', 'action' => 'index')),
            __('Add Image', true) => array('url' => array('controller' => 'images', 'action' => 'add')),
        )
    ),
    'Videos' => array('icon' => '<i class="fa fa-youtube-play"></i>',
        'subs' => array(
            __('List Videos', true) => array('url' => array('controller' => 'videos', 'action' => 'index')),
            __('Add Video', true) => array('url' => array('controller' => 'videos', 'action' => 'add')),
        )
    ),
    'Subscriptions' => array('icon' => '<i class="fa fa-users"></i>',
        'subs' => array(
            __('List Newsletters', true) => array('url' => array('controller' => 'newsletters', 'action' => 'index', 1)),
            __('List Printed Version Subscription', true) => array('url' => array('controller' => 'newsletters', 'action' => 'index', 2)),
        )
    ),
    'Users' => array('icon' => '<i class="fa fa-users"></i>',
        'subs' => array(
            __('List Users', true) => array('url' => array('controller' => 'users', 'action' => 'index')),
            __('Add User', true) => array('url' => array('controller' => 'users', 'action' => 'add')),
        )
    ),
    'Settings' => array('icon' => '<i class="fa fa-cogs"></i>',
        'subs' => array(
//            __('List Settings', true) => array('url' => array('controller' => 'settings', 'action' => 'index')),
//            __('Add Setting', true) => array('url' => array('controller' => 'settings', 'action' => 'add')),
            __('General configuration', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'general')),
            __('Social network links', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'social_networks')),
            __('Contact information', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'contact')),
            __('Printed version', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'printed_version')),
            __('Hijri Correction', true) => array('url' => array('controller' => 'settings', 'action' => 'edit-configs', 'hijri_correction')),
        )
    ),
);

$publisher_menu = array(
    __('Control panel', true) => array('url' => '#',
        'subs' => array(
            'Items' => array(
                'subs' => array(
                    'List Items' => array('url' => array('controller' => 'items', 'action' => 'index')),
                    'Add Item' => array('url' => array('controller' => 'items', 'action' => 'add')),
                )
            ),
        )
    )
);
?>


<ul class="mainNav">

    <?php
//    if ($admin['Admin']['publisher'] == 1) {
//        $menu = $publisher_menu;
//    } else {
//        $menu = $admin_menu;
//    }
    side_menu($this, $admin_menu);
    ?>

</ul>

<?php echo $this->append('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var $id = '<?php echo isset($menuId) ? $menuId : \Cake\Utility\Inflector::camelize($this->request->params['controller']); ?>';
        $('#' + $id).addClass('active');
    });
</script>
<?php echo $this->end(); ?>

<?php

function side_menu($view, $menu = array()) {
    foreach ($menu as $key => $item) {
        $class = (!empty($item['subs'])) ? 'folder' : 'file';
        $class2 = (!empty($item['class'])) ? $item['class'] : '';
        if (hasCabalities($view, $item)) {
            ?>
            <li id="<?php echo (isset($item['id'])) ? \Cake\Utility\Inflector::camelize($item['id']) : \Cake\Utility\Inflector::camelize($key); ?>">
                <a href="<?php echo (!empty($item['url'])) ? Cake\Routing\Router::url($item['url']) : "#" ?>">
                    <?php
                    if (!empty($item['icon'])) {
                        echo $item['icon'];
                    }
                    if ($class == 'folder') {
                        echo $view->Html->tag('span', __($key, true));
                    } else {
                        echo __($key, true);
                    }
                    ?>

                </a>


                <?php
                if ($class == 'folder') {
                    echo '<ul>';
                    side_menu($view, $item['subs']);
                    echo '</ul>';
                }
                ?>
            </li>

            <?php
        }
    }
}

function hasCabalities($view, $items) {
    $user = $view->viewVars['user'];
//    debug($items);
    if (!empty($items['subs'])) {
        $return = false;
        foreach ($items['subs'] as $key => $item) {
            if (isset($item['url'])) {
                $return = hasCapablity($item['url']['controller'], $item['url']['action'], $user);
                if ($return) {
                    return $return;
                }
            }
        }
        return $return;
    } else {
        return hasCapablity($items['url']['controller'], $items['url']['action'], $user);
    }
//    return false;
}
?>






