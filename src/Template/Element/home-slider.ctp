<?php if (!$featured_news->isEmpty()) { ?>
    <div class="col-lg-10 col-xs-12 my-slider">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php
                foreach ($featured_news as $key => $news) {
                    $class = "";
                    if ($key == 0) {
                        $class = "active";
                    }
                    ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $key ?>" class="<?php echo $class; ?> item<?php echo $key + 1 ?>">
                        <div class="col-xs-12">
                            <div class="col-xs-4">
                                <?php if (!empty($news->news_images[0]['image'])) { ?>
                                    <img src="<?php echo resizeOnFly($news->news_images[0]['image'], 85, 60, 1, $news->news_images[0]['image_info']['folder_path']) ?>">
                                <?php } ?>
                            </div>
                            <div class="col-xs-8">
                                <div class="h5"><a href="#"><?php echo $this->Text->truncate($news->{$lang . '_title'},60,['exact'=>true]) ?></a></div>
                            </div>
                        </div> 

                    </li>
                <?php } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner main-carousel-inner" role="listbox">
                <?php
                foreach ($featured_news as $key => $news) {
                    $class = "";
                    if ($key == 0) {
                        $class = "active";
                    }
                    ?>
                    <div class="item <?php echo $class; ?>">
                        <?php if (!empty($news->news_images[0]['image'])) { ?>
                            <img src="<?php echo resizeOnFly($news->news_images[0]['image'], 527, 292, 1, $news->news_images[0]['image_info']['folder_path']) ?>">
                        <?php } ?>
                        <div class="carousel-caption">
                            <h3><a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]) ?>"><?php echo $news->{$lang . '_title'} ?></a></h3>
                            <p><?php echo strip_tags($news->{$lang . '_brief'}) ?><a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]) ?>"><?php echo __("Read more") ?></a></p>
                        </div>
                        <div class="shadow"></div>
                    </div>

                <?php } ?>

            </div>
        </div>
    </div>
<?php } ?>
