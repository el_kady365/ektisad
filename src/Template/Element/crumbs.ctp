<?php

/* @var $this->Html HtmlHelper */
$this->Html->addCrumb('<i class="fa fa-home"></i> ', '/', ['escape' => false, 'target' => '_blank']);
if (!isset($crumbs['prefix']))
    $this->Html->addCrumb(__(\Cake\Utility\Inflector::humanize($this->request->params['prefix'])), "/{$this->request->params['prefix']}");
else if (!empty($crumbs['prefix'][0]))
    $this->Html->addCrumb(__($crumbs['prefix'][0]), "/{$crumbs['prefix'][1]}");

if (!isset($crumbs['controller']))
    $this->Html->addCrumb(__($this->request->params['controller']), array('controller' => $this->request->params['controller'], 'action' => 'index'));
else if (!empty($crumbs['controller'])) {
    foreach ($crumbs['controller'] as $controller => $url) {
        $this->Html->addCrumb(__($controller), $url);
    }
}

if (!isset($crumbs['action'])) {
    $action = $this->request->params['action'];
    if ($action != 'index') {
        $this->Html->addCrumb(__(\Cake\Utility\Inflector::humanize($action)));
    }
} else if (!empty($crumbs['action'])) {
    $this->Html->addCrumb(__($crumbs['action']));
}



if (isset($crumbs['more'])) {
    foreach ($crumbs['more'] as $crumb)
        $this->Html->addCrumb($crumb[0], $crumb[1] ? "/{$crumb[1]}" : null);
}
echo $this->Html->getCrumbs(' / ');
?>