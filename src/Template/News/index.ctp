<div class="section my-padding the-news">
    <h2 class="title clearfix"><span><?php echo $category->{$lang . '_title'} ?></span></h2>
    <?php
    if (!$news->isEmpty()) {
        foreach ($news as $news) {
            $news_url = $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $news->id, $news->{$lang . '_permalink'}]);
            ?>
            <div class="col-xs-12 my-right">
                <div class="block">
                    <div class="col-xs-3 box">
                        <?php if (!empty($news->news_images[0]['image'])) { ?>
                            <a href="<?php echo $news_url; ?>"><img src="<?php echo resizeOnFly($news->news_images[0]['image'], 190, 120, 1, $news->news_images[0]['image_info']['folder_path']) ?>"></a>
                        <?php } ?>
                    </div>
                    <div class="col-xs-9">
                        <h5><a href="<?php echo $news_url; ?>"><?php echo $news->{$lang . '_title'} ?></a></h5>
                        <div>    
                            <span class="writer-name"><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo $this->Url->build(['controller' => 'writers', 'action' => 'view', $news->writer_id]) ?>"> <?php echo $news->writer->{$lang . '_title'} ?></a></span>
                            <span style="float: left;"> <i class="fa fa-clock-o" aria-hidden="true"></i> <?php
                                $time = new Cake\I18n\Time($news->publish_date);
                                echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                                ?></span>
                        </div>
                        <p><?php echo strip_tags($news->{$lang . '_brief'}) ?><a href="<?php echo $news_url; ?>" class="read2"><?php echo __("Read more") ?></a></p>

                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        <?php
        }
    } else {
        ?>
        <div class="alert alert-warning"><?php echo __("There aren't any news in this category") ?></div>
        <?php
    }
    ?>
</div>
<div class="more-news text-center">
    <ul class="pager">
        <?php
        if ($this->Paginator->hasPrev()) {
            echo $this->Paginator->prev(__('Previous'), ['class' => '']);
        }
        echo $this->Paginator->numbers();
        if ($this->Paginator->hasNext()) {
            echo $this->Paginator->next(__('Next'), ['class' => '']);
        }
        ?>

    </ul>
</div>
