<?php

foreach ($news as $news) {
    $created = new Cake\I18n\Time($news->publish_date);
    $created = $created->format('Y-m-d H:i');

    $link = [
        'controller' => 'news',
        'action' => 'view',
        $news->id,
        $news->{$lang . '_permalink'}
    ];

    // Remove & escape any HTML to make sure the feed content will validate.
    $body = h(strip_tags($news->{$lang . '_description'}));
    
    $body = $this->Text->truncate($body, 400, [
        'ending' => '...',
        'exact' => true,
        'html' => true,
    ]);

    echo $this->Rss->item([], [
        'title' => $news->{$lang . '_title'},
        'link' => $link,
        'guid' => ['url' => $link, 'isPermaLink' => 'true'],
        'description' => $body,
        'pubDate' => $created
    ]);
}

