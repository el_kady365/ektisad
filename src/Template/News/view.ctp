<div id="the-video" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <iframe id="the-youtube" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="section my-padding">
    <h2 class="title clearfix"><span><?php echo $news->category[$lang . '_title'] ?></span></h2>
    <div class="new-detail">
        <?php if (!empty($news->{$lang . '_subtitle'})) { ?>
            <h3 class="sub-title"><?php echo $news->{$lang . '_subtitle'} ?></h3>   
        <?php } ?>
        <h3><?php echo $news->{$lang . '_title'} ?></h3>   

        <div class="row">    
            <span class="col-md-4 writer-name"><i class="fa fa-user" aria-hidden="true"></i><a href="<?php echo $this->Url->build(['controller' => 'writers', 'action' => 'view', $news->writer_id]) ?>"> <?php echo $news->writer->{$lang . '_title'} ?></a></span>

            <span class="col-md-4"> <i class="fa fa-clock-o" aria-hidden="true"></i> <?php
                $time = new Cake\I18n\Time($news->publish_date);
                echo $time->i18nFormat('dd LLLL YYYY - hh:m:ss a');
                ?></span>
            <span class="col-md-4">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <?php
                $now = $news->last_updated_date->diffForHumans();
                echo $now;
                ?>
            </span>
        </div>


        <div>
            <div class="banner-bottom">
                <!-- container -->
                <div class="container width">
                    <div>
                        <!--single-page-->
                        <div class="single-page">
                            <div class="col-md-12">
                                <?php if (!empty($news->news_images)) { ?>
                                    <div class="flexslider">
                                        <ul class="slides">
                                            <?php
                                            foreach ($news->news_images as $news_image) {
                                                if (isset($news_image)) {
                                                    ?>
                                                    <li><?php if ($news_image->type == 0) { ?>
                                                            <img src="<?php echo $news_image->image_info['path'] ?>" alt=""/>
                                                            <?php
                                                        } elseif ($news_image->type == 1) {
                                                            echo parseVideoUrl($news_image->video, 'embed', 570, 400);
                                                        }
                                                        ?>


                                                        <p class="flex-caption"><?php echo $news_image->{$lang . '_title'} ?></p>
                                                    </li>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                <?php } ?>



                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <!--//single-page-->
                    </div>

                </div>
                <!-- //container -->
            </div>

        </div>
        <div class="text-center">
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_inline_share_toolbox"></div>
        </div>
        <div class="the-text">
            <?php
            $description = $news->{$lang . '_description'};
//            $facebook_post = '<div class="text-center"><div class="fb-post" data-width="500" data-href="$1"></div></div>';
//            $description = preg_replace('/{%facebookpost\$\$(.+)\$\$%}/', $facebook_post, $description);
            /* if (preg_match_all("/<p.*?>(.+)<\/p>/", $description, $matches)) {
              //            debug($matches);
              //            $paras = preg_split('#<p([^>])*>#', $description);
              ?>


              <p><?php echo $matches[0][0]; ?></p>
              <?php if ($news->ad_image1) { ?>
              <div class="new-adv">
              <img src="<?php echo $news->ad_image1_info['path'] ?>">
              </div>
              <?php } ?>
              <?php
              foreach ($matches[0] as $key => $para) {
              if ($key != 0) {
              echo $para;
              }
              }
              ?>
              <?php if ($news->ad_image2) { ?>
              <div class="new-adv">
              <img src="<?php echo $news->ad_image2_info['path'] ?>">
              </div>
              <?php
              }
              } else { */
            echo $description;
            if ($news->ad_image) {
                ?>
                <div class="new-adv">
                    <img src="<?php echo $news->ad_image_info['path'] ?>">
                </div>
                <?php
            }
//            }
            ?>
        </div>
    </div>

</div>
<div class="clearfix"></div>
<?php if ($related_news->count()) { ?>
    <div class="section my-padding">
        <h2 class="title clearfix"><span><?php echo __('Related News') ?></span></h2>
        <?php foreach ($related_news as $related_new) { ?>
            <div class="col-xs-6 my-right">
                <a href="<?php echo $this->Url->build(['language' => $lang, 'controller' => 'news', 'action' => 'view', $related_new->id, $related_new->{$lang . '_permalink'}]) ?>">
                    <div class="block">
                        <div class="col-xs-3 box">
                            <?php if (isset($related_new->news_images[0]['image'])) { ?>
                                <img src="<?php echo resizeOnFly($related_new->news_images[0]['image'], 92, 78, 1, $related_new->news_images[0]['image_info']['folder_path']) ?>">
                            <?php } ?>
                        </div>
                        <div class="col-xs-9">
                            <h5><?php echo $related_new->{$lang . '_title'} ?></h5>
                            <div>
                                <i class="fa fa-clock-o"></i>
                                <span><?php
                                    $time = new Cake\I18n\Time($news->publish_date);
                                    echo $time->i18nFormat('dd LLLL YYYY');
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>

    </div>
    <div class="clearfix"></div>
<?php }if ($related_images->count()) { ?>
    <div class="section my-padding">
        <h2 class="title clearfix"><span><?php echo __('Related Images') ?></span></h2>
        <div class="images images2"> 
            <div class="row">
                <?php foreach ($related_images as $key => $related_image) { ?>
                    <div class="col-sm-3 <?php echo ($key != 0 && $key % 3 == 0) ? "hidden-xs" : "col-xs-4" ?>">
                        <img data-toggle="modal" class="side-image" data-url="<?php echo $related_image->image_info['path'] ?>" data-target="#the-images" src="<?php echo resizeOnFly($related_image->image, 190, 130, 1, $related_image->image_info['folder_path']) ?>"> 
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>
<?php } ?>
<?php if ($news->news_videos) { ?>
    <div class="section my-padding">
        <h2 class="title clearfix"><span><?php echo __('Related Videos') ?></span></h2>
        <div class="video3">
            <div id="my-video" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    foreach ($news->news_videos as $key => $news_video) {
                        if ($key == 0 || $key % 4 == 0) {
                            ?>
                            <div class="item <?php echo $key == 0 ? "active" : "" ?>"><div class="row">
                                <?php } ?>
                                <div class="col-sm-3 <?php echo ($key != 0 && $key % 3 == 0) ? "hidden-xs" : "col-xs-4" ?>">
                                    <div class="one-video">
                                        <?php echo parseVideoUrl('https://www.youtube.com/watch?v=' . $news_video->youtube_id, 'embed', 150, 150); ?>
                                        <div class="open-video" data-url="<?php echo parseVideoUrl('https://www.youtube.com/watch?v=' . $news_video->youtube_id, 'iframe_url') ?>" data-toggle="modal" data-target="#the-video"></div>
                                    </div>
                                </div>
                                <?php
                                if ($key != 0 && $key % 3 == 0) {
                                    echo '</div></div>';
                                }
                            }
                            ?>
                        </div>
                    </div>


                </div>

                <!--Left and right controls -->
                <a class = "left carousel-control hidden-xs" href = "#my-video" role = "button" data-slide = "prev">
                    <span class = "glyphicon glyphicon-chevron-left" aria-hidden = "true"></span>
                    <span class = "sr-only">Previous</span>
                </a>
                <a class = "right carousel-control hidden-xs" href = "#my-video" role = "button" data-slide = "next">
                    <span class = "glyphicon glyphicon-chevron-right" aria-hidden = "true"></span>
                    <span class = "sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
<?php } ?>
<div class="section my-padding comments">
    <h2 class="title clearfix"><span> <?php echo __('Comments') ?></span></h2>
    <div class="text-center">
        <div class="fb-comments" data-href="<?php echo $this->Url->build(['action' => 'view', $news->id], true) ?>" data-numposts="5"></div>
    </div>
</div>
<div class="clearfix"></div>
<?php echo $this->Html->css(array('flexslider'), ['inline' => false]) ?>
<?php echo $this->Html->script(array('jquery.flexslider', 'jquery.li-scroller.1.0'), ['block' => true]) ?>
<?php echo $this->Html->script('//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50714157462c53dd', ['block' => true]) ?>



<?php echo $this->append('script'); ?>
<script>
// Can also be used with $(document).ready()
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
            video: true,
            slideshow: true

        });
    });

    $(function () {
        $('.open-video').on('click', function () {
            $('#the-youtube').prop('src', $(this).data('url'));
        });

        $("#the-video").on('hidden.bs.modal', function () {
            $('#the-youtube').prop('src', '');
        });
    });
</script>
<?php echo $this->end() ?>

