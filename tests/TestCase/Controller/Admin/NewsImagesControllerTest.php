<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\NewsImagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\NewsImagesController Test Case
 */
class NewsImagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.news_images',
        'app.news',
        'app.categories',
        'app.editors',
        'app.groups',
        'app.users',
        'app.writers',
        'app.articles',
        'app.reviewers',
        'app.publishers',
        'app.permissions',
        'app.groups_permissions',
        'app.news_videos'
    ];

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test doOperation method
     *
     * @return void
     */
    public function testDoOperation()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
