<?php
namespace App\Test\TestCase\Controller;

use App\Controller\WritersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\WritersController Test Case
 */
class WritersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.writers',
        'app.articles',
        'app.editors',
        'app.groups',
        'app.users',
        'app.permissions',
        'app.groups_permissions',
        'app.reviewers',
        'app.publishers'
    ];

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
