<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NewsNotesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NewsNotesTable Test Case
 */
class NewsNotesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NewsNotesTable
     */
    public $NewsNotes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.news_notes',
        'app.news',
        'app.categories',
        'app.editors',
        'app.groups',
        'app.users',
        'app.writers',
        'app.articles',
        'app.reviewers',
        'app.publishers',
        'app.permissions',
        'app.groups_permissions',
        'app.news_images',
        'app.news_videos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NewsNotes') ? [] : ['className' => 'App\Model\Table\NewsNotesTable'];
        $this->NewsNotes = TableRegistry::get('NewsNotes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NewsNotes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
