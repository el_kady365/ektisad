<?php

return [
    'inputContainer' => '<div class="form-group {{div_classes}}">{{content}}</div>',
    'button' => '<button{{attrs}} class="btn btn-lg btn-primary">{{text}}</button>',
    'inputContainerError' => '<div class="form-group{{required}} {{div_classes}} error">{{content}}{{error}}</div>',
    'formGroup' => '{{label}}{{between}}{{input}}{{after}}',
    'label' => '<label class="control-label" {{attrs}}>{{text}}</label>'
];

