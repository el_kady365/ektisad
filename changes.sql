/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  mohammed.elkady
 * Created: Sep 6, 2016
 */

ALTER TABLE `writers`
ADD COLUMN `ar_about`  text NULL AFTER `image`,
ADD COLUMN `en_about`  text NULL AFTER `ar_about`;

ALTER TABLE `news`
ADD COLUMN `assigned_to`  varchar(50) NULL AFTER `modified`;

ALTER TABLE `news`
MODIFY COLUMN `published`  tinyint(1) NOT NULL AFTER `publisher_id`,
MODIFY COLUMN `show_on_home`  tinyint(1) NOT NULL AFTER `last_updated_date`,
MODIFY COLUMN `featured`  tinyint(1) NOT NULL AFTER `show_on_home`,
MODIFY COLUMN `comments_count`  int(10) UNSIGNED NOT NULL AFTER `featured`,
MODIFY COLUMN `views`  int(10) UNSIGNED NOT NULL AFTER `comments_count`;


ALTER TABLE `news`
DROP COLUMN `ad_image`,
DROP COLUMN `reviewed`,
MODIFY COLUMN `editor_id`  int(11) NULL DEFAULT NULL AFTER `writer_id`,
MODIFY COLUMN `reviewer_id`  int(11) NULL DEFAULT NULL AFTER `editor_id`,
ADD COLUMN `lingusitic_reviewed`  tinyint(1) NOT NULL AFTER `assigned_to`;

---NEW



ALTER TABLE `news`
MODIFY COLUMN `assigned_to`  int(2) NULL DEFAULT 4 AFTER `modified`;
ALTER TABLE `news`
ADD COLUMN `send_to_up_date`  datetime NULL AFTER `assigned_to`;



---8/11
ALTER TABLE `news_images`
ADD COLUMN `title`  varchar(255) NULL AFTER `id`,
ADD COLUMN `type`  int(1) NOT NULL DEFAULT 0 COMMENT '0->image , 1-> Video' AFTER `title`,
ADD COLUMN `video`  varchar(255) NULL AFTER `image`;

ALTER TABLE `news_images`
CHANGE COLUMN `title` `ar_title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `en_title`  varchar(255) NULL AFTER `ar_title`;

ALTER TABLE `news`
CHANGE COLUMN `ar_meta_description` `ar_brief`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `ar_keywords`,
CHANGE COLUMN `en_meta_description` `en_brief`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `en_keywords`;

