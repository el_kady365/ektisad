/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : ektisad

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2016-11-10 18:20:56
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `ads`
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `display_order` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ads
-- ----------------------------
INSERT INTO `ads` VALUES ('1', 'Ads Header', 'http://www.google.com', 'header', '57d0261f388c8_760.gif', '1', '1', '2016-07-12 10:43:58', '2016-09-07 16:38:47', null);
INSERT INTO `ads` VALUES ('2', 'Home Ads Left', 'http://www.yahoo.com', 'banner_left', '57d0269fde749_banner2.jpg', '2', '1', '2016-07-12 10:45:25', '2016-09-07 16:40:02', null);
INSERT INTO `ads` VALUES ('3', 'Manage Users', 'http://hello.com', 'header', '57d037034f5ba_Untitled-1.png', '2', '1', '2016-09-07 17:49:23', '2016-09-07 17:49:23', null);
INSERT INTO `ads` VALUES ('4', 'Home Ads right', 'http://hello.com', 'banner_right', '57d03a0ae2437_banner2.jpg', '1', '1', '2016-09-07 18:02:19', '2016-09-07 18:02:19', null);
INSERT INTO `ads` VALUES ('5', 'Manage Users', 'http://hello.com', 'banner_bottom', '57d03c3d70627_760.gif', '1', '1', '2016-09-07 18:11:41', '2016-09-07 18:11:41', null);
INSERT INTO `ads` VALUES ('6', 'Content ads', 'http://hello.com', 'banner_bottom', '57d03c9b5ea66_banner1.jpg', '2', '1', '2016-09-07 18:12:21', '2016-09-07 18:13:15', null);
INSERT INTO `ads` VALUES ('7', 'Sidebar', 'http://yahoo.com', 'side_bar', '57d03cd1419eb_300banner1.gif', '1', '1', '2016-09-07 18:14:09', '2016-09-07 18:14:09', null);
INSERT INTO `ads` VALUES ('8', 'Add News', 'https://www.youtube.com/embed/NclZ776Hv4k', 'header', '57ecdb7e1289d_dd.jpg', null, '1', '2016-09-29 10:31:03', '2016-09-29 11:14:38', null);
INSERT INTO `ads` VALUES ('9', 'hello category as', 'http://dd.com', 'header', '5808b91316604_760.gif', '1', '1', '2016-10-20 14:31:15', '2016-10-20 14:31:15', '7');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `ar_permalink` varchar(255) DEFAULT NULL,
  `ar_description` text,
  `ar_keywords` varchar(255) DEFAULT NULL,
  `ar_meta_description` text,
  `en_title` varchar(255) DEFAULT NULL,
  `en_permalink` varchar(255) DEFAULT NULL,
  `en_description` text,
  `en_keywords` varchar(255) DEFAULT NULL,
  `en_meta_description` text,
  `ar_tags` text,
  `en_tags` text,
  `active` tinyint(1) DEFAULT NULL,
  `writer_id` int(11) DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `reviewed` tinyint(1) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('3', 'خبر جديد', null, '<p>خبر جديد</p>\r\n', '', '', 'new news item', null, '<p>new news item</p>\r\n', '', '', '', '', '1', null, null, null, null, null, null, '2016-07-30 10:11:00', '2016-07-13 10:11:12', '2016-07-13 12:07:52');
INSERT INTO `articles` VALUES ('5', 'asdasd', null, '<p>asdasd</p>\r\n', '', '', 'asdasd', null, '<p>5456546</p>\r\n', '', '', 'asdasd', '', '0', '2', '6', '0', null, null, '0', '0000-00-00 00:00:00', '2016-07-27 13:37:23', '2016-07-28 10:49:33');
INSERT INTO `articles` VALUES ('6', 'مقال بواسطة كاتب', 'مقال-بواسطة-كاتب', '<p>مقال بواسطة كاتب</p>\r\n', '', '', 'مقال بواسطة كاتب', 'مقال-بواسطة-كاتب', '<p>مقال بواسطة كاتب</p>\r\n', '', '', '', '', null, '2', '6', '1', '2', '2', '1', '2016-09-08 13:06:00', '2016-07-28 11:20:02', '2016-09-08 13:19:49');
INSERT INTO `articles` VALUES ('7', 'البورصة تبدأ تعاملاتها بارتفاع جماعى', 'البورصة-تبدأ-تعاملاتها-بارتفاع-جماعى', '<p>ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى.</p>\r\n\r\n<p>على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى.</p>\r\n\r\n<p>ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى.</p>\r\n\r\n<p>على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى.</p>\r\n\r\n<p>ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى ارتفاع التضخم ل 8.41 فى مارس على أساس سنوى.</p>\r\n', null, '', 'Lorem ipsum dolor sit amet,', 'Lorem-ipsum-dolor-sit-amet', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed consequat rhoncus justo eget hendrerit. In non neque et sapien congue malesuada. Integer auctor dignissim aliquet. Nullam mattis, nulla nec lobortis tempor, purus urna convallis lacus, non lobortis mi magna ac augue. In pharetra tincidunt felis, sit amet tempor augue congue nec. Nulla est tortor, luctus sed metus at, consectetur malesuada leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque auctor, ante sed porta rutrum, turpis lacus commodo massa, quis venenatis dui ipsum sagittis libero. In posuere accumsan urna eget tempus. Vivamus nec ante turpis. Suspendisse porta pharetra erat at efficitur. Sed et tristique lorem, at finibus ex. Nullam pellentesque odio felis, cursus tincidunt sem lacinia id. Aenean facilisis, tellus eget ultrices feugiat, lacus sapien lobortis sapien, id vulputate diam nulla ac est. Etiam odio mauris, consequat eu mattis commodo, porta vel justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n\r\n<p>Sed at vehicula augue. Proin a quam id enim feugiat auctor. Suspendisse eget convallis odio, id posuere metus. Duis vulputate risus arcu. Donec eleifend nibh leo, ac porta odio placerat id. Ut vestibulum nisi eget neque auctor gravida. Donec metus quam, hendrerit sit amet ex in, scelerisque ultrices dui. Sed non orci nisi. Quisque sit amet interdum risus, non tincidunt eros. Etiam et tristique sem. Pellentesque pellentesque arcu nec orci volutpat tempor. Aenean consectetur nunc ac justo aliquet, eget faucibus nisl consectetur. Integer blandit ipsum eget orci posuere, sit amet gravida massa interdum. Curabitur eleifend suscipit tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse vel quam sed mauris luctus condimentum.</p>\r\n\r\n<p>Praesent tincidunt ullamcorper neque, id lobortis ipsum blandit nec. Maecenas varius ultrices ante, ut dignissim mauris elementum at. Vestibulum sagittis in nisl ullamcorper auctor. Sed libero mi, commodo ut luctus id, sollicitudin eu nulla. Ut eleifend augue consectetur mauris mattis, non vehicula elit consequat. Donec pretium tincidunt eros a cursus. Donec placerat molestie aliquet. Proin eu odio ac nulla dapibus luctus nec in elit. Ut turpis ex, congue et viverra dictum, luctus vel velit. Cras viverra justo ut leo suscipit pharetra. Vestibulum interdum urna felis. Phasellus ultricies consectetur lacus, a laoreet elit vehicula a. Donec eleifend purus ac tincidunt dictum.</p>\r\n\r\n<p>Aenean ut nulla mi. Etiam ullamcorper ante quis arcu tristique, tincidunt egestas metus mattis. Morbi fringilla mi et euismod fermentum. Phasellus maximus consequat lacus, sit amet fringilla lorem molestie eget. Proin interdum tortor vel tellus accumsan mattis. Vestibulum dignissim vitae sapien vel pretium. Donec eu elit eu magna pulvinar auctor. Praesent quis nisl nec orci euismod facilisis fringilla in justo.</p>\r\n\r\n<p>Aenean dui nisi, rhoncus hendrerit pharetra sed, hendrerit vitae mi. Vestibulum laoreet ante id leo lacinia sodales vel id metus. Curabitur hendrerit ac arcu vitae laoreet. Sed pulvinar ligula et cursus elementum. Sed lobortis, urna sit amet blandit efficitur, mi lorem tincidunt neque, eu hendrerit ante sapien ac dolor. Nam ut sodales nulla, nec tincidunt velit. Ut sed velit condimentum, volutpat elit in, varius tortor. Proin non sem ut magna facilisis tempus id ultrices arcu. Morbi leo massa, ullamcorper non lectus non, pulvinar mattis tortor. Duis vel augue ac dui fringilla tempus a ut nisi. Ut faucibus varius porta. Aenean enim odio, aliquam id ipsum vitae, pellentesque fringilla odio.</p>\r\n', null, '', '', '', null, '2', '3', '1', '2', '2', '1', '2016-09-08 13:05:00', '2016-09-08 13:03:42', '2016-09-08 13:59:50');
INSERT INTO `articles` VALUES ('8', 'خطاب السيسى', 'خطاب-السيسى', '<p>خطاب السيسى</p>\r\n', null, '', 'ELsisi speech', 'ELsisi-speech', '<p>ELsisi speech</p>\r\n', null, '', '', '', null, '2', '2', '1', null, null, '1', '2016-09-29 11:33:00', '2016-09-29 11:33:36', '2016-09-29 11:33:36');

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `ar_permalink` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `en_permalink` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'بورصات و شركات', 'بورصات-و-شركات', 'Stock markets & categories', 'Stock-markets-categories', null, '1', '2', '1', '1', '2016-06-26 08:26:04', null);
INSERT INTO `categories` VALUES ('2', 'بنوك و تأمين', 'بنوك-و-تأمين', 'banks & insurence', 'banks-insurence', null, '3', '4', '2', '1', '2016-06-27 11:38:31', null);
INSERT INTO `categories` VALUES ('3', 'اتصالات و تكنولوجيا', 'اتصالات-و-تكنولوجيا', 'Technology', 'Technology', null, '5', '6', '3', '1', '2016-07-31 13:05:41', null);
INSERT INTO `categories` VALUES ('4', 'عقارات', 'عقارات', 'Real estate', 'Real-estate', null, '7', '8', '4', '1', '2016-07-31 13:17:02', null);
INSERT INTO `categories` VALUES ('5', 'سياحة و اثار', 'سياحة-و-اثار', 'Torism', 'Torism', null, '9', '10', '5', '1', '2016-07-31 13:17:57', null);
INSERT INTO `categories` VALUES ('6', 'المزيد', 'المزيد', 'More', 'More', null, '11', '14', '6', '1', '2016-07-28 11:46:45', null);
INSERT INTO `categories` VALUES ('7', 'اسعار الذهب ', 'اسعار-الذهب', 'Gold price', 'Gold-price', '6', '12', '13', '1', '1', '2016-07-28 11:47:31', null);

-- ----------------------------
-- Table structure for `contacts`
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` text,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES ('3', 'asdasdasd', 'el_kady365@yahoo.com', 'asdasdasd', '2016-09-08 16:48:43');
INSERT INTO `contacts` VALUES ('4', 'asdasdasd', 'el_kady365@yahoo.com', 'asdasdasd', '2016-09-08 16:49:17');
INSERT INTO `contacts` VALUES ('5', 'asdasdasd', 'el_kady365@yahoo.com', 'asdasdasd', '2016-09-08 16:49:41');
INSERT INTO `contacts` VALUES ('6', 'asdasdasd', 'm.elkady365@gmail.com', 'm.elkady365@gmail.com', '2016-09-08 16:51:17');
INSERT INTO `contacts` VALUES ('7', 'asdasdasd', 'm.elkady365@gmail.com', 'true', '2016-09-08 16:55:08');
INSERT INTO `contacts` VALUES ('8', 'asdasdasd', 'm.elkady365@gmail.com', 'true', '2016-09-08 16:55:42');
INSERT INTO `contacts` VALUES ('9', 'asdasdasd', 'm.elkady365@gmail.com', 'true', '2016-09-08 16:56:32');
INSERT INTO `contacts` VALUES ('10', 'asdasdasd', 'm.elkady365@gmail.com', 'true', '2016-09-08 16:56:55');
INSERT INTO `contacts` VALUES ('11', 'asdasdasd', 'm.elkady365@gmail.com', 'true', '2016-09-08 16:57:32');
INSERT INTO `contacts` VALUES ('12', 'asdasdasd', 'm.elkady365@gmail.com', 'asdasdasd', '2016-09-08 16:59:15');
INSERT INTO `contacts` VALUES ('13', 'developer', 'el_kady365@yahoo.com', 'helllo', '2016-09-08 17:00:36');
INSERT INTO `contacts` VALUES ('14', 'developer', 'm.elkady365@gmail.com', 'sdasdasd', '2016-09-08 17:03:30');
INSERT INTO `contacts` VALUES ('15', 'dsdsd', 'ee@dd.com', 'asdasdasd', '2016-10-30 16:21:55');
INSERT INTO `contacts` VALUES ('16', 'developer', 'ad@ee.com', 'fffff', '2016-10-30 16:22:58');
INSERT INTO `contacts` VALUES ('17', 'developer', 'ddd@kk.com', 'dsadasd', '2016-10-30 16:25:02');

-- ----------------------------
-- Table structure for `faqs`
-- ----------------------------
DROP TABLE IF EXISTS `faqs`;
CREATE TABLE `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_question` text,
  `ar_answer` text,
  `en_question` text,
  `en_answer` text,
  `display_order` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of faqs
-- ----------------------------
INSERT INTO `faqs` VALUES ('1', 'بماذا يتميز موقع الاقتصاد اليوم ؟', 'هو موقع اقتصادى وواجهة إلكترونية للأخبار. وضع تصور مهنيون وخبراء في الإعلام، إذ يمنح مكانة هامة لتحليل الأحداث والأفكار. فهو واجهة إعلامية منفتحة على محيطها، تولي الأهمية للثقافة التشاركية والنقاش واختلاف الآراء.', 'How are you ?', 'I\'m Fine. Thank you', '1', '1', '2016-07-12 14:46:45', null);

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'مديرى الموقع', '2016-07-17 09:52:31', '2016-10-26 14:09:09');
INSERT INTO `groups` VALUES ('2', 'الدسك', '2016-07-14 16:53:24', '2016-11-03 14:21:04');
INSERT INTO `groups` VALUES ('3', 'رئيس القسم', '2016-07-17 10:08:53', '2016-11-03 13:24:19');
INSERT INTO `groups` VALUES ('4', 'المحرر', '2016-07-17 10:08:35', '2016-11-03 13:24:49');
INSERT INTO `groups` VALUES ('5', 'الكتاب', '2016-07-27 11:13:00', '2016-10-26 10:24:14');

-- ----------------------------
-- Table structure for `groups_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `groups_permissions`;
CREATE TABLE `groups_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups_permissions
-- ----------------------------
INSERT INTO `groups_permissions` VALUES ('22', '7', '1');
INSERT INTO `groups_permissions` VALUES ('32', '5', '1');
INSERT INTO `groups_permissions` VALUES ('33', '6', '1');
INSERT INTO `groups_permissions` VALUES ('34', '16', '1');
INSERT INTO `groups_permissions` VALUES ('35', '17', '1');
INSERT INTO `groups_permissions` VALUES ('36', '18', '1');
INSERT INTO `groups_permissions` VALUES ('37', '19', '1');
INSERT INTO `groups_permissions` VALUES ('38', '20', '1');
INSERT INTO `groups_permissions` VALUES ('39', '21', '1');
INSERT INTO `groups_permissions` VALUES ('41', '23', '1');
INSERT INTO `groups_permissions` VALUES ('42', '24', '1');
INSERT INTO `groups_permissions` VALUES ('43', '25', '1');
INSERT INTO `groups_permissions` VALUES ('44', '26', '1');
INSERT INTO `groups_permissions` VALUES ('45', '26', '2');
INSERT INTO `groups_permissions` VALUES ('46', '26', '3');
INSERT INTO `groups_permissions` VALUES ('47', '26', '4');
INSERT INTO `groups_permissions` VALUES ('48', '27', '1');
INSERT INTO `groups_permissions` VALUES ('49', '28', '1');
INSERT INTO `groups_permissions` VALUES ('50', '3', '1');
INSERT INTO `groups_permissions` VALUES ('51', '3', '2');
INSERT INTO `groups_permissions` VALUES ('56', '29', '1');
INSERT INTO `groups_permissions` VALUES ('57', '29', '3');
INSERT INTO `groups_permissions` VALUES ('58', '30', '1');
INSERT INTO `groups_permissions` VALUES ('59', '30', '2');
INSERT INTO `groups_permissions` VALUES ('63', '31', '1');
INSERT INTO `groups_permissions` VALUES ('64', '32', '1');
INSERT INTO `groups_permissions` VALUES ('65', '33', '3');
INSERT INTO `groups_permissions` VALUES ('66', '33', '4');
INSERT INTO `groups_permissions` VALUES ('68', '34', '1');
INSERT INTO `groups_permissions` VALUES ('69', '35', '1');
INSERT INTO `groups_permissions` VALUES ('70', '35', '2');
INSERT INTO `groups_permissions` VALUES ('71', '35', '3');
INSERT INTO `groups_permissions` VALUES ('72', '35', '4');
INSERT INTO `groups_permissions` VALUES ('73', '36', '1');
INSERT INTO `groups_permissions` VALUES ('74', '36', '2');
INSERT INTO `groups_permissions` VALUES ('75', '36', '3');
INSERT INTO `groups_permissions` VALUES ('76', '36', '4');
INSERT INTO `groups_permissions` VALUES ('77', '37', '4');
INSERT INTO `groups_permissions` VALUES ('78', '33', '1');
INSERT INTO `groups_permissions` VALUES ('79', '38', '1');
INSERT INTO `groups_permissions` VALUES ('80', '38', '3');
INSERT INTO `groups_permissions` VALUES ('81', '39', '1');
INSERT INTO `groups_permissions` VALUES ('82', '39', '2');
INSERT INTO `groups_permissions` VALUES ('83', '39', '3');
INSERT INTO `groups_permissions` VALUES ('84', '40', '1');
INSERT INTO `groups_permissions` VALUES ('85', '40', '2');
INSERT INTO `groups_permissions` VALUES ('86', '40', '3');
INSERT INTO `groups_permissions` VALUES ('87', '41', '1');
INSERT INTO `groups_permissions` VALUES ('88', '41', '2');
INSERT INTO `groups_permissions` VALUES ('89', '42', '1');
INSERT INTO `groups_permissions` VALUES ('90', '42', '2');
INSERT INTO `groups_permissions` VALUES ('91', '37', '1');
INSERT INTO `groups_permissions` VALUES ('92', '37', '2');
INSERT INTO `groups_permissions` VALUES ('93', '37', '3');
INSERT INTO `groups_permissions` VALUES ('95', '43', '1');
INSERT INTO `groups_permissions` VALUES ('96', '44', '1');
INSERT INTO `groups_permissions` VALUES ('97', '44', '2');
INSERT INTO `groups_permissions` VALUES ('98', '45', '1');
INSERT INTO `groups_permissions` VALUES ('99', '45', '2');
INSERT INTO `groups_permissions` VALUES ('100', '45', '3');
INSERT INTO `groups_permissions` VALUES ('101', '38', '2');
INSERT INTO `groups_permissions` VALUES ('102', '33', '2');
INSERT INTO `groups_permissions` VALUES ('103', '46', '1');
INSERT INTO `groups_permissions` VALUES ('104', '46', '2');
INSERT INTO `groups_permissions` VALUES ('105', '46', '3');
INSERT INTO `groups_permissions` VALUES ('106', '46', '4');
INSERT INTO `groups_permissions` VALUES ('107', '46', '5');
INSERT INTO `groups_permissions` VALUES ('108', '47', '1');
INSERT INTO `groups_permissions` VALUES ('109', '47', '2');
INSERT INTO `groups_permissions` VALUES ('110', '47', '3');
INSERT INTO `groups_permissions` VALUES ('111', '47', '4');
INSERT INTO `groups_permissions` VALUES ('112', '47', '5');
INSERT INTO `groups_permissions` VALUES ('113', '48', '1');
INSERT INTO `groups_permissions` VALUES ('114', '48', '2');
INSERT INTO `groups_permissions` VALUES ('115', '48', '3');
INSERT INTO `groups_permissions` VALUES ('116', '48', '4');
INSERT INTO `groups_permissions` VALUES ('117', '48', '5');
INSERT INTO `groups_permissions` VALUES ('118', '49', '1');

-- ----------------------------
-- Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `ar_description` text,
  `en_description` text,
  `image` varchar(255) DEFAULT NULL,
  `ar_tags` text,
  `en_tags` text,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1', 'Image 1', 'Image 1', '<p>Image 1</p>\r\n', '<p>Image 1</p>\r\n', '57b5997194dfe_201512094.jpg', 'القاهرة,الأسكندرية,السويس', 'Cairo,Suez', '1', '2016-07-14 10:09:08');
INSERT INTO `images` VALUES ('2', 'Image2', 'Image2', '<p>Image2</p>\r\n', '', '57b59934b7577_Banque-Misr-Desouk.JPG', 'القاهرة', 'Cairo', '1', '2016-08-18 13:16:34');
INSERT INTO `images` VALUES ('3', 'Large Image', 'Large Image', '', '', '57b59f142b557_dd.jpg', 'السويس,القاهرة', 'Suez', '1', '2016-08-18 13:42:12');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'القائمة الرئيسية', '2016-07-20 15:19:14');
INSERT INTO `menus` VALUES ('2', 'القائمة السفلية', '2016-07-20 15:19:33');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `ar_subtitle` varchar(255) DEFAULT NULL,
  `ar_permalink` varchar(255) DEFAULT NULL,
  `ar_description` text,
  `ar_keywords` varchar(255) DEFAULT NULL,
  `ar_brief` text,
  `en_title` varchar(255) DEFAULT NULL,
  `en_subtitle` varchar(255) DEFAULT NULL,
  `en_permalink` varchar(255) DEFAULT NULL,
  `en_description` text,
  `en_keywords` varchar(255) DEFAULT NULL,
  `en_brief` text,
  `ar_tags` text,
  `en_tags` text,
  `active` tinyint(1) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `writer_id` int(11) DEFAULT NULL,
  `editor_id` int(11) DEFAULT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `publisher_id` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `last_updated_date` datetime DEFAULT NULL,
  `show_on_home` tinyint(1) NOT NULL,
  `featured` tinyint(1) NOT NULL,
  `comments_count` int(10) unsigned NOT NULL,
  `views` int(10) unsigned NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `assigned_to` int(2) DEFAULT '4',
  `send_to_up_date` datetime DEFAULT NULL,
  `lingusitic_reviewed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `search` (`ar_title`,`ar_description`,`en_title`,`en_description`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('3', 'البورصة تربح 1.5 مليار جنيه عقب مرور نصف ساعة من بدء التداول', 'البورصة ', 'البورصة-تربح-1-5-مليار-جنيه-عقب-مرور-نصف-ساعة-من-بدء-التداول', '<p>خبر جديد</p>\r\n', null, '', 'Stock profit of 1.5 billion pounds after the half-hour from the start of trading', 'trading ', 'Stock-profit-of-1-5-billion-pounds-after-the-half-hour-from-the-start-of-trading', '<p>Profit market capitalization of the Egyptian Stock Exchange, about 1.5 billion pounds, after the half-hour from the start of the trading session, on Sunday, the beginning of the week&#39;s sessions, bringing the capital to 418.229 billion pounds, and tended Egyptians transactions for the purchase, while the IFO dealings Arabs and foreigners for sale.</p>\r\n', null, 'trading ', '', 'trading', '1', '1', '2', null, '2', '5', '1', '2016-07-24 21:00:00', '2016-09-01 00:00:00', '1', '1', '0', '4', '2016-07-13 10:11:12', '2016-10-05 16:02:36', '4', null, '0');
INSERT INTO `news` VALUES ('4', 'رونالدو يعود للتدريبات مع الريال', 'الدوري الإسباني', 'رونالدو-يعود-للتدريبات-مع-الريال', '<p dir=\"rtl\">لم يخف الفرنسي زين الذين زيدان مدرب ريال مدريد سعادته إزاء معاودة النجم البرتغالي كريستيانو رونالدو تدريباته مع النادي الملكي.</p>\r\n\r\n<p dir=\"rtl\">وتدرب رونالدو منفردا عن زملائه&nbsp;مع سعيه للتعافي من إصابة ألمت به في أربطة ركبته اليسرى في نهائي كأس أوروبا 2016، التي توج منتخب بلاده بلقبها على حساب فرنسا منظمة البطولة.</p>\r\n\r\n<p dir=\"rtl\">ولم يعط &quot;زيزو&quot; موعدا محددا لعودة هداف الملكي، لكنه قال إنه &quot;من الجيد أن نرى رونالدو داخل المستطيل الأخضر، إنها إشارة جيدة، نرغب بعودته في أسرع وقت ممكن&quot;.</p>\r\n\r\n<p dir=\"rtl\">ويستهل ريال مدريد -حامل لقب دوري أبطال أوروبا والكأس السوبر الأوروبية- حملته في الليغا بمواجهة صعبة أمام مضيفه ريال سوسييداد، وسيفتقد نادي العاصمة الإسبانية لخدمات رونالدو والمهاجم الفرنسي كريم ينزيمة الذي يتعافى بدوره من إصابة في الظهر.</p>\r\n\r\n<p dir=\"rtl\">ويشكل الظفر بلقب الليغا أولوية لزيدان بعد أربعة مواسم عجاف، خصوصا أن عشاق الميرينغي لم يقبلوا معانقة الخصمين&nbsp;التقليديين برشلونة وأتلتيكو اللقب العتيد (برشلونة 2013، 2015 و2016 وأتلتيكو 2014)&nbsp;مع&nbsp;هذا العدد&nbsp;الهائل من النجوم في&nbsp;الفريق الملكي.</p>\r\n', null, 'الدوري الإسباني', 'Ronaldo back in training with Real', 'Spanish league', 'Ronaldo-back-in-training-with-Real', '<p>Zinedine Zidane, who Real Madrid coach did not hide his pleasure about the resumption of Portuguese star Cristiano Ronaldo his training with the club.</p>\r\n\r\n<p>Ronaldo trained alone all his colleagues with his quest to recover from the injury he sustained ligament damage in his left knee in the European Cup final in 2016, which culminated in his national team title as the expense of France championship organization.</p>\r\n\r\n<p>&quot;Zizou&quot; did not give a specific date for the return of the royal scorer, but he said: &quot;It was good to see Ronaldo inside the green rectangle, it&#39;s a good sign, we want him back as soon as possible.&quot;</p>\r\n\r\n<p>Real Madrid begins -haml Champions League and Super Cup Euro campaign in La Liga to face a tough defeat at Real Sociedad, and will miss the Spanish capital services Ronaldo Club and the French striker Karim Enzemh, who is recovering from the turn of a back injury.</p>\r\n\r\n<p>The nail the title of La Liga priority to Zidane after four lean seasons, especially the lovers Almiringi did not accept the embrace of traditional rivals Barcelona and Atletico illustrious surname (Barcelona 2013.2015 and 2016 and Atletico in 2014) with the vast number of stars in the Royal Group.</p>\r\n', null, 'Spanish league', 'الدوري الإسباني', 'Spanish league', null, '4', '2', '2', '4', null, '1', '2016-07-24 11:24:00', '2016-09-01 00:00:00', '1', '1', '2', '1', '2016-07-24 11:24:57', '2016-11-02 17:21:47', '4', null, '0');
INSERT INTO `news` VALUES ('5', 'البرازيل تحقق حلمها بذهبية الكرة بأولمبياد ريو', 'أولمبياد ريو', 'البرازيل-تحقق-حلمها-بذهبية-الكرة-بأولمبياد-ريو', '<p dir=\"rtl\">قاد المهاجم الفذ&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/fdd68154-5cb7-4edd-a8d0-fc680f0d7696/88a7acd6-bd1a-4d4d-b2d4-f243fa047616\" target=\"_self\">نيمار</a>&nbsp;منتخب&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/12835b50-a872-4466-b351-a0204482c134/085ba7fd-f4db-4aae-b278-7fb7911160d2\" target=\"_self\">البرازيل</a>&nbsp;لإحراز ذهبيته الأولى في تاريخ مشاركاته في&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/79bd6d3d-04cf-4270-bd27-02eaa9aa5f7a/89ee81f1-568c-4098-b123-cd64fc1b62da\" target=\"_self\">الألعاب الأولمبية</a>، بتسجيله هدف الافتتاح في الوقت الأصلي أمام&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/12835b50-a872-4466-b351-a0204482c134/db58a003-d63c-4902-8cc1-fab2ca595bdc\" target=\"_self\">ألمانيا</a>&nbsp;(1-1) ثم ركلة الترجيح الحاسمة (5-4) على ملعب&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/37c0a303-0881-4323-898f-4d28bb5b7cd8/edac9cff-7443-4324-adb6-d8124e881cdd\" target=\"_self\">ماراكانا</a>، في نهائي مسابقة كرة القدم للرجال بأولمبياد ريو.</p>\r\n\r\n<p dir=\"rtl\">وحقق منتخب &quot;السامبا&quot; أخيرا حلمه الأولمبي وتوج بالميدالية الذهبية التي طال انتظارها.</p>\r\n\r\n<p dir=\"rtl\">وأنهى المنتخب البرازيلي الشوط الأول لصالحه بهدف سجله نيمار من ركلة حرة (28).</p>\r\n\r\n<p dir=\"rtl\">وفي الشوط الثاني، سجل ماكسيميليان ماير هدف التعادل لألمانيا (59)، لينتهي الوقت الأصلي للمباراة بالتعادل 1-1.</p>\r\n\r\n<p dir=\"rtl\">ولجأ الفريقان إلى وقت إضافي لنصف ساعة على شوطين، لكن التعادل ظل قائما ليحتكما إلى ركلات الترجيح. وفيها&nbsp;سجل لألمانيا ماتياس غينتر وسيرجي غنابري وجوليان براند ونيكلاس سولي، وأهدر نيلز بيترسن الركلة الخامسة التي تصدى لها الحارس، وسجل للبرازيل أوغوستو ريناتو وماركينوس ورافاييل ألكانتارا ولوان ونيمار.</p>\r\n\r\n<p dir=\"rtl\">كما أحرز المنتخب النيجيري الميدالية البرونزية للمسابقة بفوز ثمين 3-2 على منتخب&nbsp;<a href=\"http://sport.aljazeera.net/home/getpage/12835b50-a872-4466-b351-a0204482c134/51b15866-c375-4fbe-9f55-8414cc3266c2\" target=\"_self\">هندوراس</a>&nbsp;في وقت سابق يوم السبت في مباراة تحديد المركز الثالث.</p>\r\n', null, 'أولمبياد ريو', 'Brazil achieved its dream of gold ball Rio Olympics', ' Rio Olympics', 'Brazil-achieved-its-dream-of-gold-ball-Rio-Olympics', '<p>Ehsan Abd Elkodous</p>\r\n\r\n<p>Feat striker Neymar led Brazil to make the first gold medal in the history of his participation in the Olympics, scoring the opening goal in extra time against Germany (1-1) and then the decisive penalty (5-4) at the Maracana Stadium, in the final of the football tournament for men Rio Olympics .</p>\r\n\r\n<p>And achieved the team &quot;samba&quot; Finally his dream of Olympic gold medal capped a long-awaited.</p>\r\n\r\n<p>Brazil finished the first half with a goal scored in his favor Neymar&#39;s free-kick (28).</p>\r\n\r\n<p>In the second half, scoring the equalizer Maximilian Mayer of Germany (59), to end the original time of the match in a 1-1 draw.</p>\r\n\r\n<p>The two teams have resorted to extra time for half an hour on two games, but the draw has existed to judge between the penalty shootout. And the record of Germany Matthias Gintr and Sergey Gnapra and Julian Brand and Nicklas Soleil, and Niels Petersen missed a fifth-kick saved by goalkeeper, scored Brazil&#39;s Renato Augusto and Marquinhos and Rafael Alcantara and Wan Neymar.</p>\r\n\r\n<p>Eagles also scored the bronze medal of the competition a precious win 3-2 at Honduras earlier on Saturday in the third place match</p>\r\n', null, ' Rio Olympics', 'أولمبياد ريو', 'Rio Olympics', null, '4', '2', '2', '2', '2', '1', '2016-08-16 13:44:00', '2016-09-01 00:00:00', '1', '1', '0', '4', '2016-08-17 13:44:51', '2016-11-09 14:00:14', '4', null, '0');
INSERT INTO `news` VALUES ('6', '\"الأطباء\": أخذ عينة من جسم المريض مقصورة على أعضائنا.. و\"الصيادلة\" تتنازل عن قضايا ضد \"الصحة\" لمنحها التراخيص', 'الأطباء', 'الأطباء-أخذ-عينة-من-جسم-المريض-مقصورة-على-أعضائنا-و-الصيادلة-تتنازل-عن-قضايا-ضد-الصحة-لمنحها-التراخيص', '<p>تتنافس نقابات &quot;الأطباء البشريين، والصيادلة، والعلميين، والبيطريين والزراعيين&quot;، حول إثبات أحقية أعضائها فى فتح معامل ومزاولة مهنة التحاليل الطبية، على أساس الحصول على بكالوريوس فى الطب والجراحة أو فى الصيدلة أو فى العلوم &quot;الكيمياء&quot; أو فى الطب البيطرى أو فى الزراعة.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir=\"RTL\">وأكدت نقابة الأطباء، أن الحكم الصادر من المحكمة الإدارية العليا (دائرة توحيد المبادئ)، نص على أن الترخيص بإجراء التحاليل الطبية لعينات جسم الإنسان مقصورة على الأطباء البشريين المقيدين بسجل الأطباء بوزارة الصحة، وبجدول النقابة، مطالبة إدارة التراخيص الطبية بوزارة الصحة والسكان الالتزام بالحكم حرصا على حياة المواطنين، مؤكدة على استعداداها التام للتدخل الانضمامى مع الوزارة فى الطعن المرفوع ضدها بخصوص هذا الأمر، تفعيلا لدور النقابة في الحفاظ علي حقوق أعضائها من خريجى كليات الطب البشرى.</p>\r\n', null, 'الأطباء', '\"Doctors\": take a sample of the patient\'s body are limited to members .. and \"pharmacists\" waive the cases against the \"health\" of granting licenses', 'Doctors', 'Doctors-take-a-sample-of-the-patient-s-body-are-limited-to-members-and-pharmacists-waive-the-cases-against-the-health-of-granting-licenses', '<p>Compete &quot;medical doctors, pharmacists, scientists, veterinarians and agricultural&quot; unions, about to prove the eligibility of its members in the open labs and practicing medical analysis, based on obtaining a Bachelor of Medicine and Surgery in pharmacy or in science &quot;chemistry&quot; or in veterinary medicine, or in agriculture .</p>\r\n\r\n<p>She stressed the Medical Association, that the judgment of the Supreme Administrative Court (the Department of uniting principles), provided that the license to conduct medical tests of samples of the human body are limited to medical doctors registered a record of the doctors at the Ministry of Health, and the schedule of the union, the Medical Licensing Department at the Ministry of Health and Population claim commitment to governance in order on the lives of citizens, emphasizing the full Astaadadaha Alandmamy to interfere with the ministry to challenge raised against it regarding this matter, in order to effectuate the role of the union in preserving the rights of members of the graduates of the colleges of Medicine.</p>\r\n', null, 'Doctors', 'الأطباء', 'Doctors', null, '3', '2', '2', '2', '2', '1', '2016-08-21 11:51:00', '2016-07-01 00:00:00', '1', '1', '1', '6', '2016-08-21 11:41:01', '2016-11-03 11:35:17', '4', null, '0');
INSERT INTO `news` VALUES ('7', 'لجنة القوى العاملة بالبرلمان تبدأ مناقشة قانون منع العمل فوق 60 عاما', 'القوى العاملة', 'لجنة-القوى-العاملة-بالبرلمان-تبدأ-مناقشة-قانون-منع-العمل-فوق-60-عاما', '', null, 'القوى العاملة', 'Commission workforce parliament starts to discuss the prevention of work over the 60-year-old law', 'working force', 'Commission-workforce-parliament-starts-to-discuss-the-prevention-of-work-over-the-60-year-old-law', '<p>Began a short while ago, the Commission on Workforce House of Representatives, for the consideration of the draft law submitted by MP Atef Abdel Gawad, secretary of the Housing Committee, and 67 other deputies, on the prevention of work for those meeting reached the age the age of sixty in government jobs, in the presence of representatives from the Ministry of Planning and Follow-up.</p>\r\n\r\n<p>Article I of the bill states: &quot;apply the law to all government agencies and public sector companies and public business sector and all the bodies that represent the budget part of the state budget, funds or a portion of their funds public money belongs to the people within the Republic or elsewhere&quot;.</p>\r\n', null, 'working force', 'القوى العاملة', 'working force', null, '1', '2', '2', '2', '2', '1', '2016-08-21 11:47:00', '2016-10-19 16:49:12', '1', '1', '0', '0', '2016-08-21 11:49:22', '2016-10-19 16:49:12', '4', null, '0');
INSERT INTO `news` VALUES ('9', 'مرض السكر يرفع نسب وفيات مرضى القلب والشرايين والسرطان', 'مرض السكر', 'مرض-السكر-يرفع-نسب-وفيات-مرضى-القلب-والشرايين-والسرطان', '', null, 'مرض السكر', 'Diabetes raises the mortality rate of cardiovascular disease and cancer patients', 'cardiovascular', 'Diabetes-raises-the-mortality-rate-of-cardiovascular-disease-and-cancer-patients', '<p>A recent scientific study led by Spanish researchers, that diabetes raises the death rate from cardiovascular disease and cancer.<br />\r\n&nbsp;<br />\r\nAccording to the study, which was recently published in &quot;Diabetes care&quot; magazine, researchers from Delmar Hospital Institute analyzed in the Spanish city of Barcelona, ​​the data of about 55 people Spain aged 35-79 years for 10 years to the study of the relationship between diabetes and its association with death.<br />\r\n&nbsp;<br />\r\nThe researchers found that 15.6% of the participants had diabetes, was the total number of deaths 9.1%, and it was people who suffer from diabetes, the owners of the highest mortality rate in all cases of cardiovascular disease.<br />\r\n&nbsp;<br />\r\nThe researchers added that diabetes is associated with premature deaths due to heart disease and vascular and cancer.<br />\r\n&nbsp;<br />\r\nTheir findings were published across the US medical website &quot;Health day News&quot;, and that in the twentieth of the month of Aug..</p>\r\n', null, 'cardiovascular', 'مرض السكر', 'cardiovascular', null, '3', '2', '2', '2', '2', '1', '2016-08-16 14:09:00', '2016-09-01 00:00:00', '1', '1', '0', '3', '2016-08-21 14:11:17', '2016-10-19 12:09:52', '4', null, '0');
INSERT INTO `news` VALUES ('10', 'إعطاء الكورتيزون لتكملة رئة المبتسرين يعرضهم لاعتلال شبكية العين', 'الكورتيزون', 'إعطاء-الكورتيزون-لتكملة-رئة-المبتسرين-يعرضهم-لاعتلال-شبكية-العين', '', null, 'الكورتيزون', 'Give cortisone to supplement the lungs of premature expose them to retinopathy', 'lungs', 'Give-cortisone-to-supplement-the-lungs-of-premature-expose-them-to-retinopathy', '<p>A recent scientific study led by US researchers that giving steroids to supplement the lungs of premature babies in the nursery expose them to infection impairment of the retina.<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\nThe researchers explained in a study recently published cross-site medical US &quot;Health Day News&quot;, that steroids, which are used to help promote the work of the lungs of premature babies may increase their risk for eye problems.<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\nThe researchers pointed out that doctors often give cortisone drugs for preterm infants &quot;born too early&quot; to improve lung function and weight at birth, but has long been suspected that these drugs may have adverse side effects.<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\n&nbsp;<br />\r\nAnd he found the head of researchers &quot;Tami Muvsas&quot; Pediatrics assistant and human development professor at the University of Michigan, US, during the study, which included the 1,500 infants who were weighing less than 500 grams at birth, the risk of a condition called retinopathy preterm was higher than 60% for babies, which took steroids to enhance lung function they have.</p>\r\n', null, 'lungs', 'الكورتيزون', 'lungs', null, '3', '2', '2', '2', '2', '1', '2016-08-15 14:14:00', '2016-09-01 00:00:00', '1', '1', '0', '15', '2016-08-21 14:14:31', '2016-10-31 13:54:46', '4', null, '0');
INSERT INTO `news` VALUES ('11', 'مصنع \"أندرا\" الفرنسى يعيد تدوير 280 ألف سيارة مستعملة سنويا', 'اعاده التدوير', 'مصنع-أندرا-الفرنسى-يعيد-تدوير-280-ألف-سيارة-مستعملة-سنويا', '<p>يارب</p>', null, 'مصنع \"أندرا\" الفرنسى يعيد تدوير', '\"Onedra\" French plant recycles 280 thousand cars annually Used', 'recycles', 'Onedra-French-plant-recycles-280-thousand-cars-annually-Used', '<p>The currently used French car recycling plant known as the \"Andhra\" - a branch of the French company Renault for cars - recycles 280 thousand used cars per year, this is precisely what Olivier Judo director of the company, who confirmed that since 2015 has been auto rotate by 95%.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The works in this company three thousand and 500 workers, and up the value of sales of 270 million euros .. also explained Olivier Judo How is the recycling process, which takes four hours, where the worker unzip car parts made of glass and doors and Motor Alberpreez, and are stored about 100 thousand pieces and these the pieces are sent to other factories or are exported to Africa, and other parts of the tires and battery, car seats are collected 100 cubic meters per year, it is the distribution of fuel and gasoline at the pumps working in the company, while the diesel Vied sell it again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>As the disarmament of tens of meters of electrical behavior, bringing the car to lose 400 kg and is recycled steel, which is 75% of the car, as well as copper cables, made him the car As for electronic equipment is another question.</p>\r\n', null, 'recycles', 'اعاده التدوير', 'recycles', null, '1', '2', '2', '2', '2', '1', '2016-10-19 15:13:00', '2016-11-10 14:29:46', '1', '1', '0', '4', '2016-08-21 14:21:47', '2016-11-10 14:29:46', '4', null, '0');
INSERT INTO `news` VALUES ('12', 'انخفاض صافى أرباح\"القاهرة للإسكان\"لـ58 مليون جنيه خلال 6 شهور', 'البرصة', 'انخفاض-صافى-أرباح-القاهرة-للإسكان-لـ58-مليون-جنيه-خلال-6-شهور', '<p>أعلنت شركة القاهرة للإسكان والتعمير عن النتائج المالية المجمعة للشركة، وبلغت صافى الأرباح 58 مليون جنيه خلال الفترة من يناير وحتى 30 يونيو الماضى، مقابل 91 مليون جنيه خلال نفس الفترة من العام الماضى، وبلغ صافى الربح خلال الفترة من إبريل وحتى يونيو نحو 15 مليون جنيه مقابل 67 مليون جنيه خلال نفس الفترة من العام الماضى.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>وقالت الشركة، فى بيانها للبورصة المصرية اليوم الأحد، إن القوائم المالية المستقلة للشركة بلغ صافى أرباحها 5.8 مليون جنيه خلال الفترة من يناير وحتى نهاية يونيو، مقابل نحو 20.8 مليون جنيه خلال نفس الفترة من العام الماضى.</p>\r\n', null, '', 'Decline in net profit, \"Cairo for housing\" for 58 million pounds in 6 months', 'profit', 'Decline-in-net-profit-Cairo-for-housing-for-58-million-pounds-in-6-months', '<p>Cairo Company for Housing and Construction announced financial results for the combined company, amounting to a net profit of 58 million pounds during the period from January to June 30, compared with 91 million pounds during the same period last year, net profit during the period from April to June of about 15 million pounds compared with 67 million pounds during the same period last year.<br />\r\n&nbsp;<br />\r\nThe company said, in a statement, the Egyptian stock exchange said on Sunday that the separate financial statements of the company&#39;s net profit of 5.8 million pounds during the period from January until the end of June, compared to about 20.8 million pounds during the same period last year.</p>\r\n', null, '', '', '', null, '1', '2', '2', '2', '2', '1', '2016-08-01 14:44:00', '2016-09-01 00:00:00', '1', '1', '0', '4', '2016-08-21 14:45:26', '2016-10-20 17:24:24', '4', null, '0');
INSERT INTO `news` VALUES ('13', 'استقرار سعر النفط الخام', 'النفط', 'استقرار-سعر-النفط-الخام', '<p dir=\"RTL\">بحسب آخر بيانات البورصة العالمية حول أسعار الخام، ينشر &quot;اليوم السابع&quot; أسعار النفط والبترول عالميا فى تعاملات اليوم الأحد 21-8-2016، والتى سجلت استقرارا وثباتا عند ذات الأسعار التى وصلت إليها فى تعاملات الأمس، حيث بلغ نفط برنت 50.80 دولار للبرميل، فيما سجل سعر النفط الأمريكى &quot;غرب تكساس الوسيط&quot; 48.50 دولارا للبرميل.</p>\r\n\r\n<p dir=\"RTL\">ووفقا للبيانات الصادرة عن وزارة البترول، اليوم، استقرت أسعار البنزين المحلى أيضا، مسجلة 160 قرشا للتر بنزين 80، و&quot;260&quot; قرشا للتر بنزين 92، فيما بلغ بنزين 95 &quot;625&quot; قرشا، بينما سجلت المنتجات البترولية المحلية، 180 قرشا لكل لتر سولار، وهو ذات السعر الذى سجله سعر اللتر للكيروسين، ووصل سعر البوتاجاز إلى 8.0 جنيه للأسطوانة.</p>\r\n', null, 'النفط', 'The stability of crude oil to the cylinder price.', 'cylinder', 'The-stability-of-crude-oil-to-the-cylinder-price', '<p>According to the latest global stock market data on crude prices, publishes the &quot;seventh day&quot; of oil and oil prices worldwide in trading today, Sunday 21/08/2016, which recorded a stable and consistent at the same prices that reached in trading yesterday, with Brent oil amounted to $ 50.80 a barrel, with record oil price of US &quot;West Texas intermediate&quot; of $ 48.50 a barrel.</p>\r\n\r\n<p>According to the data released by the Ministry of Petroleum, today, domestic gasoline prices also stabilized, registered 160 piasters per liter of gasoline 80, and &quot;260&quot; piasters per liter petrol 92, while petrol 95 &quot;625&quot; was piasters, while domestic petroleum products recorded a 0.180 pounds per liter Solar, which is the same price at which the record price of a liter of kerosene, and the price of butane gas to 8.0 pounds per disc.</p>\r\n', null, 'cylinder', 'النفط', 'cylinder', null, '1', '2', '2', '2', '2', '1', '2016-07-19 15:00:00', '2016-09-01 00:00:00', '1', '1', '0', '1', '2016-08-21 15:01:55', '2016-09-06 17:37:36', '4', null, '0');
INSERT INTO `news` VALUES ('14', 'انخفاض سعر الجنيه الإسترلينى يعزز قطاع السياحة فى بريطانيا', 'السياحة', 'انخفاض-سعر-الجنيه-الإسترلينى-يعزز-قطاع-السياحة-فى-بريطانيا', '', null, 'السياحة', 'Low Sterling promotes tourism in Britain', 'tourism', 'Low-Sterling-promotes-tourism-in-Britain', '<p>The impact of the decision to get out of the European Union on the British economy is still unclear, but working in tourism in the United Kingdom seem optimistic that attracts low Pound Sterling foreign tourists.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Said Charlotte French student who came from Toulouse to spend a weekend with her friends in London, &quot;The decline in the exchange rate is very good for tourists, because of the high cost of London&#39;s well-known city prices.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>She said while she was standing in line to enter the wax museum &quot;Madame Tussauds&quot; one of the most popular tourist destinations in the British capital &quot;We had bookings for the journey before the results of the referendum&quot; on the exit of the United Kingdom from the European Union, noting that the depreciation of the currency &quot;pleasantly surprised by the form of the price.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The Pound Sterling fell more than 10% against the dollar and against the euro since the referendum held on June 23, and the voice of the British out of the Union.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This step requires the application of some time and require lengthy negotiations with the European Commission. But the financial markets began to respond to this sudden decision to punish the pound, which raises the satisfaction of workers in the tourism sector in the United Kingdom.</p>\r\n', null, 'tourism', 'السياحة', 'tourism', null, '5', '2', '2', '2', '2', '1', '2016-08-12 15:06:00', '2016-09-01 00:00:00', '1', '1', '0', '6', '2016-08-21 15:06:13', '2016-11-09 14:00:39', '4', null, '0');
INSERT INTO `news` VALUES ('15', 'جدل بزراعة البرلمان\" بسبب ترشيح الوزير لأعضاء مجلس إدارة بنك التنمية', 'التنمية', 'جدل-بزراعة-البرلمان-بسبب-ترشيح-الوزير-لأعضاء-مجلس-إدارة-بنك-التنمية', '', null, 'التنمية', 'cale cultivation of parliament, \"the minister because of the nomination of the members of the Board of the Bank of Development Administration', 'Administration', 'cale-cultivation-of-parliament-the-minister-because-of-the-nomination-of-the-members-of-the-Board-of-the-Bank-of-Development-Administration', '<p>Article 5 of the draft special law raised converts Development Bank and Agricultural Credit to the Agricultural Bank is subject to the supervision of the central bank, considerable debate during the discussion today, the meeting of the Committees of Agriculture and Economic Affairs of parliament and article concerning the mechanism to select the members of the Board of Directors by the nomination of the Minister of Agriculture.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>It objected MP Solaf Hussein, that are selected members of the Board of Directors on the nomination of the Minister of Agriculture, saying that this means that we are accustomed to scratch and I&#39;m haveing ​​corrupted files and lists of names of those who took over the presidency of the Bank of courtesy and all those departments were negative and did not add a new Bank &quot; .</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mahmoud prestige, a member of the House of Representatives also objected, saying: &quot;We have to assume goodwill in the next cabinet and put in our perception that everybody is corrupt.&quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>He objected Sayed Hassan, also a member of the Agriculture Committee, to be Secretary of Agriculture is entrusted with the nomination of board members must be assigned to choose members of the central bank especially that dependency will have a fortiori be the owner of the right to choose the members of the Board of Directors, and that all Previous administrations have not lived up to provide what benefits the farmer or as assigned to them, and that the Minister of Agriculture when he wants to live up one of his co-workers entrusted with the task of managing development Bank.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The MP Jewels Sherbini, it must assign the selection of members to the Board of Directors of the Central Bank and the specialization of the door so that it becomes the focus of responsibility.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>He said the Meselhy, chairman of parliament&#39;s economic commission, should not bear in mind the previous negative experiences and corrupt ministers, and that the next best, and when we make new legislation taking into account the good choice.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>In spite of this debate was to vote on the text of the article came as most of the members and approved as presented text of the draft law, which allows the Secretary of Agriculture to nominate members of the Agricultural Bank of Egypt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Article 5: &quot;holds the bank&#39;s Board of Directors formed of chairman and two deputies, and the representative of the Ministry of Finance and a representative of the Ministry of Supply and Trade and a representative of the Ministry of Agriculture and Land Reclamation and six specialists in matters&quot; banking, monetary and economic and agricultural development, legal and financial. &quot;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>And it shall be appointed and his deputies and members of the Board President&#39;s decision by the Prime Minister on the nomination of the Minister of Agriculture and Land Reclamation and the approval of the Governor of the Central Bank for a period of three years, subject to renewal.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>The issue of Bank management decisions by majority. In case of equal number of votes is likely, the Chairman.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>And determine the salaries, allowances and bonuses chairman and his two deputies and specialists bonuses Members of the non-working bank, a decision of the Prime&nbsp;</p>\r\n', null, 'Administration', 'التنمية', '', null, '2', '2', '2', '2', '2', '1', '2016-08-08 15:09:00', '2016-09-07 18:27:44', '1', '1', '3', '5', '2016-08-21 15:11:22', '2016-10-19 12:08:57', '4', null, '0');
INSERT INTO `news` VALUES ('17', 'احسان عبد القدوس', '', 'احسان-عبد-القدوس', '<p><br></p><div class=\"text-center\"><iframe src=\"https://www.facebook.com/plugins/post.php?href=https://www.facebook.com/melkady/posts/10154778137886282?comment_id=10154778925351282&amp;reply_comment_id=10154779897806282&amp;notif_t=feed_comment&amp;notif_id=1477478831980307&amp;width=500&amp;show_text=true&amp;appId=1620723841472882\" style=\"border:none;overflow:hidden\" scrolling=\"no\" allowtransparency=\"true\" width=\"500\" height=\"300\" frameborder=\"0\"></iframe></div><p><br></p><div class=\"text-center\"></div><p><br></p>', null, '', 'Ehsan Abd Elkodous', '', 'Ehsan-Abd-Elkodous', '<p>Ehsan Abd Elkodous</p><p><br></p><div class=\"text-center\"><iframe src=\"https://www.facebook.com/plugins/post.php?href=https://www.facebook.com/melkady/posts/10154778137886282?comment_id=10154778925351282&amp;reply_comment_id=10154779897806282&amp;notif_t=feed_comment&amp;notif_id=1477478831980307&amp;width=500&amp;show_text=true&amp;appId=1620723841472882\" style=\"border:none;overflow:hidden\" scrolling=\"no\" allowtransparency=\"true\" width=\"500\" height=\"300\" frameborder=\"0\"></iframe></div><p><br></p>\r\n', null, '', '', '', null, '7', '2', '2', '2', '2', '0', '2016-09-27 16:13:00', '2016-10-26 13:34:50', '1', '0', '0', '8', '2016-09-27 16:10:01', '2016-10-27 14:01:26', '2', null, '0');
INSERT INTO `news` VALUES ('18', 'احسان عبد القدوس', 'القوى العاملة', null, '<p>القوى العاملة<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '3', null, null, '0', null, '2016-10-26 18:05:29', '0', '0', '0', '0', '2016-10-23 16:08:10', '2016-10-27 12:20:08', '3', null, '0');
INSERT INTO `news` VALUES ('19', 'خبر جديد بواسطة المحرر', '', null, '<p>خبر جديد بواسطة المحرر<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '3', null, null, '0', null, '2016-10-27 10:45:50', '0', '0', '0', '0', '2016-10-26 14:22:02', '2016-10-27 12:20:03', '3', null, '0');
INSERT INTO `news` VALUES ('20', 'NEWW', '', null, '<p>NEWW<br></p>', null, '', '', '', null, '<p><br></p>', null, '', 'القاهرة', '', null, '1', '2', '2', null, null, '0', null, '2016-10-30 15:06:48', '0', '0', '0', '0', '2016-10-27 14:28:11', '2016-10-30 15:06:49', '4', null, '0');
INSERT INTO `news` VALUES ('21', 'خطاب السيسى', 'القوى العاملة', null, '<p>خطاب السيسى<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '3', null, null, '0', null, '2016-10-30 16:11:15', '0', '1', '0', '0', '2016-10-30 16:10:47', '2016-11-03 15:07:12', '2', null, '0');
INSERT INTO `news` VALUES ('22', 'test the send to reviewers', '', null, '<p>test the send to reviewers<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '3', null, '2', '1', '2016-11-03 16:20:23', '2016-11-03 16:20:23', '0', '0', '0', '2', '2016-11-03 16:00:50', '2016-11-09 11:31:29', '2', '2016-11-03 15:12:22', '0');
INSERT INTO `news` VALUES ('23', 'يا رب ', 'يا رب ', null, '<p>يا رب&nbsp;<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '2', null, null, '0', '2016-11-03 15:37:00', null, '0', '0', '0', '0', '2016-11-03 16:15:28', '2016-11-03 17:28:49', '3', '2016-11-03 17:28:49', '0');
INSERT INTO `news` VALUES ('24', 'ندى sddd', 'ندى sdddd', null, '<p>ندى sddd<br></p>', null, '', '', '', null, '<p><br></p>', null, '', '', '', null, '1', '2', '3', null, null, '1', null, '2016-11-09 17:35:50', '0', '0', '0', '0', '2016-11-03 17:26:32', '2016-11-09 17:35:50', '3', '2016-11-03 17:28:29', '0');

-- ----------------------------
-- Table structure for `news_images`
-- ----------------------------
DROP TABLE IF EXISTS `news_images`;
CREATE TABLE `news_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0->image , 1-> Video',
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_images
-- ----------------------------
INSERT INTO `news_images` VALUES ('5', null, null, '0', '57b973ade348c_egypt.jpg', null, '2', '2016-07-11 16:44:36');
INSERT INTO `news_images` VALUES ('6', null, null, '0', '57b9757cdba14_good-new.jpg', null, '1', '2016-07-21 14:53:34');
INSERT INTO `news_images` VALUES ('7', null, null, '0', '5790c5ce51146_dd.jpg', null, '1', '2016-07-21 14:53:34');
INSERT INTO `news_images` VALUES ('10', null, null, '0', '57b97024694a0_ronaldo.jpg', null, '4', '2016-07-24 12:12:48');
INSERT INTO `news_images` VALUES ('11', null, null, '0', '57b970f8df850_borsa.jpg', null, '3', '2016-08-16 16:47:25');
INSERT INTO `news_images` VALUES ('12', null, null, '0', '57b44e3378f8b_201512094.jpg', null, '5', '2016-08-17 13:44:51');
INSERT INTO `news_images` VALUES ('13', null, null, '0', '57b9772d786c4_3lom.jpg', null, '6', '2016-08-21 11:41:01');
INSERT INTO `news_images` VALUES ('14', null, null, '0', '57b979220d961_kwa.jpg', null, '7', '2016-08-21 11:49:22');
INSERT INTO `news_images` VALUES ('16', null, null, '0', '57b99a6528fa4_sokar.jpg', null, '9', '2016-08-21 14:11:17');
INSERT INTO `news_images` VALUES ('17', null, null, '0', '57b99b2721b9f_3loo.jpg', null, '10', '2016-08-21 14:14:31');
INSERT INTO `news_images` VALUES ('18', 'صورة 1', 'صورة 2', '0', '57b99cdb4c71d_car.jpg', null, '11', '2016-08-21 14:21:47');
INSERT INTO `news_images` VALUES ('19', null, null, '0', '57b9a266eee14_borsss.jpg', null, '12', '2016-08-21 14:45:26');
INSERT INTO `news_images` VALUES ('20', null, null, '0', '57b9a64340f2b_borsa.jpg', null, '13', '2016-08-21 15:01:55');
INSERT INTO `news_images` VALUES ('21', null, null, '0', '57b9a7454159f_tour.jpg', null, '14', '2016-08-21 15:06:13');
INSERT INTO `news_images` VALUES ('22', null, null, '0', '57d1a728e2a8f_4.jpg', null, '16', '2016-09-08 20:00:08');
INSERT INTO `news_images` VALUES ('23', null, null, '0', '57ea7db960d35_dd.jpg', null, '17', '2016-09-27 16:10:01');
INSERT INTO `news_images` VALUES ('24', null, null, '0', '580cc44b00531_760.gif', null, '18', '2016-10-23 16:08:10');
INSERT INTO `news_images` VALUES ('25', null, null, '0', '58109fea29d65_add-new-news.png', null, '19', '2016-10-26 14:22:02');
INSERT INTO `news_images` VALUES ('26', null, null, '0', '5810a0db70b1d_add-new-news.png', null, '20', '2016-10-26 14:26:03');
INSERT INTO `news_images` VALUES ('27', null, null, '0', '5811f2db1d597_home-bug.jpg', null, '20', '2016-10-27 14:28:11');
INSERT INTO `news_images` VALUES ('28', null, null, '0', '5815ff671680e_dd.jpg', null, '21', '2016-10-30 16:10:47');
INSERT INTO `news_images` VALUES ('29', null, null, '0', '5819e21a4206f_1280x720-0ob.jpg', null, '22', '2016-11-02 14:54:50');
INSERT INTO `news_images` VALUES ('30', null, null, '0', '581b3d98f2bad_dd.jpg', null, '23', '2016-11-03 15:37:28');
INSERT INTO `news_images` VALUES ('31', '', '', '0', '581b5728bc581_1280x720-0ob.jpg', 'https://www.youtube.com/watch?v=xIuG6qlDUJo', '24', '2016-11-03 17:26:32');
INSERT INTO `news_images` VALUES ('33', 'فيديو 1', 'فيديو 2', '1', null, 'https://www.youtube.com/watch?v=hYn4bq3gvMs', '11', '2016-11-10 11:42:32');

-- ----------------------------
-- Table structure for `news_notes`
-- ----------------------------
DROP TABLE IF EXISTS `news_notes`;
CREATE TABLE `news_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` text NOT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_notes
-- ----------------------------
INSERT INTO `news_notes` VALUES ('2', 'يارب ', '20', '3', '2016-10-30 14:52:04');
INSERT INTO `news_notes` VALUES ('4', 'يا رب ', '20', '2', '2016-10-30 15:06:49');
INSERT INTO `news_notes` VALUES ('5', 'يارب', '21', '2', '2016-10-30 16:11:15');

-- ----------------------------
-- Table structure for `news_videos`
-- ----------------------------
DROP TABLE IF EXISTS `news_videos`;
CREATE TABLE `news_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `youtube_id` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news_videos
-- ----------------------------
INSERT INTO `news_videos` VALUES ('6', '4', 'NdUIzOWAOqc', '2016-07-26 13:08:59', null);
INSERT INTO `news_videos` VALUES ('7', '4', 'lixPaFt3VV0', '2016-07-26 16:25:21', null);
INSERT INTO `news_videos` VALUES ('8', '2', 'lixPaFt3VV0', '2016-08-16 17:23:04', null);
INSERT INTO `news_videos` VALUES ('9', '2', 'NdUIzOWAOqc', '2016-08-16 17:23:04', null);
INSERT INTO `news_videos` VALUES ('10', '20', 'lixPaFt3VV0', '2016-10-30 12:54:07', null);
INSERT INTO `news_videos` VALUES ('11', '20', 'zAVxjj_xzcM', '2016-10-30 12:54:07', null);

-- ----------------------------
-- Table structure for `newsletters`
-- ----------------------------
DROP TABLE IF EXISTS `newsletters`;
CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `address` tinytext,
  `email` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newsletters
-- ----------------------------
INSERT INTO `newsletters` VALUES ('4', 'developer', '04100000000', 'Al Mahallah Al Kubra, Al Mahalah Al Kubra (Part 2), Al Mahalah Al Kubra, Gharbia Governorate, Egypt', 'm.elkady365@gmail.com', '2', '2016-08-23 15:35:56', '2016-08-23 15:35:56');
INSERT INTO `newsletters` VALUES ('12', null, null, null, 'm.elkady365@gmail.com', '1', '2016-08-23 18:00:19', '2016-08-23 18:00:19');
INSERT INTO `newsletters` VALUES ('13', null, null, null, 'el_kady365@yahoo.com', '1', '2016-08-23 18:03:07', '2016-08-23 18:03:07');
INSERT INTO `newsletters` VALUES ('14', null, null, null, 'el_kady365d@yahoo.com', '1', '2016-08-23 18:03:29', '2016-08-23 18:03:29');
INSERT INTO `newsletters` VALUES ('15', null, null, null, 'el_kady365ss@yahoo.com', '1', '2016-08-23 18:05:04', '2016-08-23 18:05:04');
INSERT INTO `newsletters` VALUES ('16', null, null, null, 'm.elkasdy365@gmail.com', '1', '2016-08-23 18:07:56', '2016-08-23 18:07:56');
INSERT INTO `newsletters` VALUES ('17', null, null, null, 'm.el111kady365@gmail.com', '1', '2016-08-25 10:08:00', '2016-08-25 10:08:00');
INSERT INTO `newsletters` VALUES ('18', null, null, null, 'm.ssel111kady365@gmail.com', '1', '2016-08-25 10:21:27', '2016-08-25 10:21:27');
INSERT INTO `newsletters` VALUES ('19', null, null, null, 'm.ssel111skady365@gmail.com', '1', '2016-08-25 10:21:31', '2016-08-25 10:21:31');
INSERT INTO `newsletters` VALUES ('20', null, null, null, 'm.elkadyssss365@gmail.com', '1', '2016-09-27 16:51:44', '2016-09-27 16:51:44');
INSERT INTO `newsletters` VALUES ('21', null, null, null, 'm.elkadysssssss365@gmail.com', '1', '2016-09-27 16:51:56', '2016-09-27 16:51:56');
INSERT INTO `newsletters` VALUES ('22', null, null, null, 'm.elkassssdy365@gmail.com', '1', '2016-09-27 16:52:53', '2016-09-27 16:52:53');
INSERT INTO `newsletters` VALUES ('23', null, null, null, 'm.elkssssasdady365@gmail.com', '1', '2016-09-27 16:54:19', '2016-09-27 16:54:19');
INSERT INTO `newsletters` VALUES ('24', 'developer', '04100000000', 'address', 'el_kady365@yahoo.com', '2', '2016-09-27 19:48:41', '2016-09-27 19:48:41');

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `ar_content` text,
  `en_content` text,
  `add_to_mainmenu` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('18', 'من نحن', 'About us', '<p>تعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافي تعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافي.</p>\r\n\r\n<p>تمزج بين الأخبار العاجلة والتقارير والتحليلات السياسية ومقالات كبار الكتاب، جنبا إلي جنب مع الخدمات والترفيه، حرصا علي إرضاء جمهورها بتنوع اهتماماته في مصر والوطن العربي، ومتحدثي العربية حول العالم تمزج بين الأخبار العاجلة والتقارير والتحليلات السياسية ومقالات كبار الكتاب، جنبا إلي جنب مع الخدمات والترفيه، حرصا علي إرضاء جمهورها بتنوع اهتماماته في مصر والوطن العربي، ومتحدثي العربية حول العالم تمزج بين الأخبار العاجلة والتقارير والتحليلات السياسية ومقالات كبار الكتاب، جنبا إلي جنب مع الخدمات والترفيه، حرصا علي إرضاء جمهورها بتنوع اهتماماته في مصر والوطن العربي، ومتحدثي العربية حول العالم تمزج بين الأخبار العاجلة والتقارير والتحليلات السياسية ومقالات كبار الكتاب، جنبا إلي جنب مع الخدمات والترفيه، حرصا علي إرضاء جمهورها بتنوع اهتماماته في مصر والوطن العربي، ومتحدثي العربية حول العالم.</p>\r\n\r\n<p>تعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافي تعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافيتعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافي تعتمد على مجموعة من صحافييها الشباب لانتاج جريدة يومية عصرية تكسر النهج التقليدي في العمل الصحافي.</p>\r\n', '<p>Rely on a group of young people its journalists to produce a modern daily newspaper breaking the traditional approach in journalism relies on a group of young people its journalists to produce a modern daily newspaper breaking the traditional approach in journalism. Combines breaking news and reports and political analyzes and articles by leading writers, along with services and entertainment, in order to satisfy its audience the diversity of interests in Egypt and the Arab world, the Arab-speaking world that combines breaking news and reports and political analyzes and articles by leading writers, along with services and entertainment , in order to satisfy its audience the diversity of interests in Egypt and the Arab world, the Arab-speaking world that combines breaking news and reports and political analyzes and articles by leading writers, along with services and entertainment, in order to satisfy its audience the diversity of interests in Egypt and the Arab world, the Arab-speaking world blends between breaking news and reports and political analyzes and articles by leading writers, along with services and entertainment, in order to satisfy the diversity of its audience interests in Egypt and the Arab world, the Arab-speaking world. Rely on a group of its journalists youth to produce a daily newspaper modern break the traditional approach in journalism relies on a group of its journalists youth to produce a daily newspaper modern break the traditional approach in Asahafatatmd work on a group of its journalists youth to produce a daily newspaper modern break the traditional approach in journalism depends on the group the youth of its journalists to produce a modern daily newspaper breaks the traditional approach in journalism.</p>\r\n', '1', '0', '5', '4', '57d18ba762b14_4.jpg', '1');
INSERT INTO `pages` VALUES ('19', 'asdasd', 'adasdasd', '<p><img src=\"/img/uploads/e222582fe82708b32c0971cbd4abc050.jpeg\" style=\"width: 962px;\"><img src=\"/img/uploads/cdb73818542cc840c1cdf17f62910fc4.png\" style=\"width: 16px;\"></p><p><img src=\"/img/uploads/ef9effa1dc8645cdf2c8ed8f33c90716.png\" style=\"width: 16px;\"><br></p><p><br></p>', '<p><img src=\"/img/uploads/ef9effa1dc8645cdf2c8ed8f33c90716.png\" style=\"width: 16px;\"><br></p>', null, null, '5', '6', null, null);

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `perm_key` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('3', 'Publish News', 'news,publish', '1');
INSERT INTO `permissions` VALUES ('5', 'Manage Pages', 'pages,manage', '1');
INSERT INTO `permissions` VALUES ('6', 'Manage Contactus', 'contacts,manage', '1');
INSERT INTO `permissions` VALUES ('7', 'Manage Categories', 'categories,manage', '0');
INSERT INTO `permissions` VALUES ('16', 'Manage Ads', 'ads,manage', '0');
INSERT INTO `permissions` VALUES ('17', 'Manage FAQs', 'faqs,manage', '0');
INSERT INTO `permissions` VALUES ('18', 'Manage Polls', 'polls,manage', '0');
INSERT INTO `permissions` VALUES ('19', 'Manage Polls Questions', 'poll-questions,manage', '0');
INSERT INTO `permissions` VALUES ('20', 'Manage Poll Answers', 'poll-answers,manage', '0');
INSERT INTO `permissions` VALUES ('21', 'Manage Writers', 'writers,manage', '0');
INSERT INTO `permissions` VALUES ('23', 'Manage Images', 'images,manage', '0');
INSERT INTO `permissions` VALUES ('24', 'Manage Users', 'users,manage', '1');
INSERT INTO `permissions` VALUES ('25', 'Manage Settings', 'settings,edit-configs', '1');
INSERT INTO `permissions` VALUES ('26', 'dashboard', 'pages,dashboard', '0');
INSERT INTO `permissions` VALUES ('27', 'permissions', 'permissions,manage', '0');
INSERT INTO `permissions` VALUES ('28', 'groups', 'groups,manage', '0');
INSERT INTO `permissions` VALUES ('29', 'Review Articles', 'articles,review', '0');
INSERT INTO `permissions` VALUES ('30', 'Publish Articles', 'articles,publish', '0');
INSERT INTO `permissions` VALUES ('31', 'Manage Videos', 'videos,manage', '0');
INSERT INTO `permissions` VALUES ('32', 'Manage newsletters', 'newsletters,manage', '1');
INSERT INTO `permissions` VALUES ('33', 'Add News', 'news,add', '0');
INSERT INTO `permissions` VALUES ('34', 'Delete News', 'news,delete', '0');
INSERT INTO `permissions` VALUES ('35', 'List News', 'news,index', '1');
INSERT INTO `permissions` VALUES ('36', 'Edit News', 'news,edit', '0');
INSERT INTO `permissions` VALUES ('37', 'Send News to Reviewers', 'news,send-news-to-reviewers', '0');
INSERT INTO `permissions` VALUES ('38', 'send news to desks', 'news,send-news-to-desks', '0');
INSERT INTO `permissions` VALUES ('39', 'Add Writer', 'writers,add', '0');
INSERT INTO `permissions` VALUES ('40', 'return news to eritors', 'news,return-news-to-editors', '0');
INSERT INTO `permissions` VALUES ('41', 'return news to reviewers', 'news,return-news-to-reviewers', '0');
INSERT INTO `permissions` VALUES ('42', 'list writers', 'writers,index', '0');
INSERT INTO `permissions` VALUES ('43', 'Un publish news', 'news,unpublish', '0');
INSERT INTO `permissions` VALUES ('44', 'linguistic reviewing', 'news,linguistic-review', '0');
INSERT INTO `permissions` VALUES ('45', 'Add news notes', 'news,add-notes', '0');
INSERT INTO `permissions` VALUES ('46', 'List Articles', 'articles,index', '0');
INSERT INTO `permissions` VALUES ('47', 'add Articles', 'articles,add', '0');
INSERT INTO `permissions` VALUES ('48', 'Edit Articles', 'articles,edit', '0');
INSERT INTO `permissions` VALUES ('49', 'Delete Articles', 'articles,delete', '0');

-- ----------------------------
-- Table structure for `poll_answers`
-- ----------------------------
DROP TABLE IF EXISTS `poll_answers`;
CREATE TABLE `poll_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_question_id` int(11) DEFAULT NULL,
  `poll_question_option_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of poll_answers
-- ----------------------------
INSERT INTO `poll_answers` VALUES ('1', '2', '3', '2016-11-02 17:37:09');

-- ----------------------------
-- Table structure for `poll_question_options`
-- ----------------------------
DROP TABLE IF EXISTS `poll_question_options`;
CREATE TABLE `poll_question_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_question_id` int(11) NOT NULL,
  `ar_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of poll_question_options
-- ----------------------------
INSERT INTO `poll_question_options` VALUES ('1', '1', '22', '111', null);
INSERT INTO `poll_question_options` VALUES ('2', '1', '21', '787', '2');
INSERT INTO `poll_question_options` VALUES ('3', '2', 'نعم', 'YES', '1');
INSERT INTO `poll_question_options` VALUES ('4', '3', 'سسسس', 'sss', '222');
INSERT INTO `poll_question_options` VALUES ('5', '2', 'لا', 'No', '2');
INSERT INTO `poll_question_options` VALUES ('6', '3', 'asdasd', 'asdasd', null);
INSERT INTO `poll_question_options` VALUES ('7', '4', 'إجابة واحد', 'answer one', '1');
INSERT INTO `poll_question_options` VALUES ('8', '4', 'إجابة اتنين', 'answer two', null);

-- ----------------------------
-- Table structure for `poll_questions`
-- ----------------------------
DROP TABLE IF EXISTS `poll_questions`;
CREATE TABLE `poll_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `poll_id` int(11) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of poll_questions
-- ----------------------------
INSERT INTO `poll_questions` VALUES ('2', 'هل ستتأثر انجلترا بعد خروجها من الاتحاد الأوروبى؟', 'It is affected England after graduating from the EU?', '2', '1', '1', '1', '2016-07-13 13:20:25', null);
INSERT INTO `poll_questions` VALUES ('3', 'احسان عبد القدوس', 'Ehsan Abd Elkodous', '2', null, '1', '0', '2016-09-15 11:56:33', null);

-- ----------------------------
-- Table structure for `polls`
-- ----------------------------
DROP TABLE IF EXISTS `polls`;
CREATE TABLE `polls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of polls
-- ----------------------------
INSERT INTO `polls` VALUES ('1', 'Poll 1', 'Poll 1', '1', '2016-07-13 10:39:29', '2016-07-13 11:34:32');

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(255) DEFAULT NULL,
  `setting_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'site_name_ar', 'اقتصاد مصر');
INSERT INTO `settings` VALUES ('2', 'site_name_en', 'Masr Economy');
INSERT INTO `settings` VALUES ('30', 'mobile', '01096587423\r\n01820475698');
INSERT INTO `settings` VALUES ('31', 'telephone', '001203368954');
INSERT INTO `settings` VALUES ('32', 'facebook', 'http://sdasdasd.com');
INSERT INTO `settings` VALUES ('33', 'facebook', 'https://www.facebook.com/fikracommunity/');
INSERT INTO `settings` VALUES ('34', 'twitter', 'http://youtube.com');
INSERT INTO `settings` VALUES ('35', 'linkedin', 'http://youtube.com');
INSERT INTO `settings` VALUES ('36', 'youtube', 'http://youtube.com');
INSERT INTO `settings` VALUES ('38', 'google_plus', 'https://plus.google.com/');
INSERT INTO `settings` VALUES ('40', 'printed_version', '57eaaf7910b3d_Mohammed-Elkady.pdf');
INSERT INTO `settings` VALUES ('42', 'email', 'advertising@gmail.com');
INSERT INTO `settings` VALUES ('44', 'hijri_correction', '0');
INSERT INTO `settings` VALUES ('45', 'post_office', '256902');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` char(100) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `writer_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'admin', '$2y$10$FpG8.MjCMG7gQ1/wbblf1.wiGFDpaZjgN3dKL7NybxMfjtnvioifK', '1', null, '2016-06-12 11:02:13', null);
INSERT INTO `users` VALUES ('3', 'editor', '$2y$10$7jJxmX7BZ5cnuY2HPme.5euJJy9CCYxy8SlAjgF/oihqZjKPSZgnC', '4', null, '2016-07-17 10:19:39', null);
INSERT INTO `users` VALUES ('4', 'reviewer', '$2y$10$2.2RH2T/1oFqAaw.e7IBkugTocgFY4h5XGywxqDUQrMr1SNyCK8sa', '3', null, '2016-07-21 13:38:29', null);
INSERT INTO `users` VALUES ('5', 'publisher', '$2y$10$Hwgg/JyUkD/3Suh4TA515.LOSLGWggw8CHJlLuA9.9mzVTj2ofzsy', '2', null, '2016-07-21 13:38:45', null);

-- ----------------------------
-- Table structure for `videos`
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of videos
-- ----------------------------
INSERT INTO `videos` VALUES ('1', 'asdasdasd', 'asdasdasd', 'https://vimeo.com/129315122', '1', '1', '2016-07-28 12:21:49', null);
INSERT INTO `videos` VALUES ('2', ' خطاب السيسى خطاب السيسى خطاب السيسى', 'El sisi Speech', 'https://www.youtube.com/embed/NclZ776Hv4k', '1', '2', '2016-08-17 14:16:54', null);
INSERT INTO `videos` VALUES ('3', 'خطاب السيسى', 'خطاب السيسى', 'https://www.youtube.com/embed/NclZ776Hv4k', '1', '3', null, null);

-- ----------------------------
-- Table structure for `writers`
-- ----------------------------
DROP TABLE IF EXISTS `writers`;
CREATE TABLE `writers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `ar_about` text,
  `en_about` text,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of writers
-- ----------------------------
INSERT INTO `writers` VALUES ('2', 'محمد عاطف', 'Ehsan Abd Elkodous', '57d140de79565_114-rp-id-1-labrador-ral.jpg', '<p>يشسيشسي</p><p><img src=\"/img/uploads/ef9effa1dc8645cdf2c8ed8f33c90716.png\" data-filename=\"arabic-art.png\" style=\"width: 16px;\"></p><p><img src=\"/img/uploads/cdb73818542cc840c1cdf17f62910fc4.png\" data-filename=\"islamic-art.png\" style=\"width: 16px;\"><br></p>', '<p>شسيشسيشسي</p>', '1', '2016-07-19 11:33:27');
