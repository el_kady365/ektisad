
jQuery(document).ready(function () {


    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    $('.tootlip').tooltip();
    

});


//<![CDATA[
jQuery(window).load(function () { // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(700).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(700).css({'overflow': 'visible'});
})
//]]>
